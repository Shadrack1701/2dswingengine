### What is this repository for? ###
* This was made as a learning project for a 2D tile based game using Swing/AWT components.
* 0.01

### How do I get set up? ###
Clone the project using source tree or download directly and use a Java IDE, I recomment Eclipse or IntelliJ
Use Java 1.8 JDK and JRE
This project is fully Mavenized, import as Maven project and dependencies should be managed for you.

### Disclaimer ###
Not a working game, this was designed as an engine demo and learning.  Not all functionality is fully working.