package trx.puzzlerpg.main.level;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import trx.puzzlerpg.main.entities.Entity;
import trx.puzzlerpg.main.level.tiles.LoadTile;

public class Area {

	private byte[] tiles;
	private BufferedImage image;
	private int width;
	private int height; 
	private int areaId;
	private String areaName;
	private String areaPath;
	private List<LoadTile> loadTiles = new ArrayList<>();
	private List<Entity> areaEntities = new ArrayList<>();
    private List<Entity> areaRemoveEntities = new ArrayList<>();
	private ArrayList<int[]> heightMap = null;

/*******************************************GETTERS AND SETTERS***********************************************/
	public int getAreaId() {
		return areaId;
	}
	public void setAreaId(int areaId) {
	this.areaId = areaId;
}
	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	public byte[] getTiles() {
		return tiles;
	}
	public void setTiles(byte[] tiles) {
		this.tiles = tiles;
	}
	public void setTile(byte tile, int location) {
		this.tiles[location] = tile;
	}
	public BufferedImage getImage() {
		return image;
	}
	public void setImage(BufferedImage image) {
		this.image = image;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public List<LoadTile> getLoadTiles() {
		return loadTiles;
	}
	public void setLoadTiles(List<LoadTile> loadTiles) {
		this.loadTiles = loadTiles;
	}
	public List<Entity> getAreaEntities() {
		return areaEntities;
	}
	public void setAreaEntities(List<Entity> entities) {
		this.areaEntities = entities;
	}
	public String getAreaPath() {
		return areaPath;
	}
	public void setAreaPath(String areaPath) {
		this.areaPath = areaPath;
	}
    public List<Entity> getAreaRemoveEntities() {
        return areaRemoveEntities;
    }
    public void setAreaRemoveEntities(List<Entity> areaRemoveEntities) {
        this.areaRemoveEntities = areaRemoveEntities;
    }
    public ArrayList<int[]> getHeightMap() {
		return heightMap;
	}
	public void setHeightMap(ArrayList<int[]> heightMap2) {
		this.heightMap = heightMap2;
	}
}

