package trx.puzzlerpg.main.level;

import java.awt.Graphics;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.imageio.ImageIO;

import trx.puzzlerpg.main.entities.Entity;
import trx.puzzlerpg.main.entities.Mob;
import trx.puzzlerpg.main.gfx.Screen;
import trx.puzzlerpg.main.level.tiles.LoadTile;
import trx.puzzlerpg.main.level.tiles.Tile;
import trx.puzzlerpg.main.util.EntityComparator;
import trx.puzzlerpg.main.util.Vector2D;

public class Level {

	private int levelId;
	private String name;
	private String displayName;
	private int currentArea;
	private boolean wipeScreen;

    public List<Entity> levelEntities = new ArrayList<>();
	public List<Tile> levelTiles = new ArrayList<>();
	public List<Area> areas = new ArrayList<>();
    public List<Entity> levelRemoveEntities = new ArrayList<Entity>();
	public int levelSpriteSheetSize;
	public int levelSpriteSheetBinary;
	
	public void init() {
		for (Entity entity : levelEntities) {
			entity.setLevel(this);
			entity.init();
		}
		
		int tempCurrentArea = currentArea;
		for (Area area: areas) {
			currentArea = area.getAreaId();
			for (Entity entity : area.getAreaEntities()) {
				entity.setLevel(this);
				entity.init();
			}
		}
		currentArea = tempCurrentArea;
	}
	
	public void loadLevelFromFile() {
		for (Area a : areas) {
			try {
				a.setImage(ImageIO.read(Level.class.getResource(a.getAreaPath())));
				a.setWidth(a.getImage().getWidth());
				a.setHeight(a.getImage().getHeight());
				a.setTiles(new byte[a.getWidth() * a.getHeight()]);
				this.loadTiles(a);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void loadTiles(Area a) {
		int[] tileColors = a.getImage().getRGB(0, 0, a.getWidth(), a.getHeight(), null, 0, a.getWidth());
		for (int y = 0; y < a.getHeight(); y++) {
			for (int x = 0; x < a.getWidth(); x++) {
				tileCheck: for (Tile t : levelTiles) {
					if (t == null)
						break tileCheck;
					long tempLong = Long.decode(t.getLevelColor());
					int tempInt = (int) tempLong;
					if (t != null && tempInt == tileColors[x + y * a.getWidth()]) {
						a.setTile(t.getId(),x + y * a.getWidth());
						break tileCheck;
					}
				}
			}
		}
	}

	public Tile getTile(int x, int y) {
		if (0 > x || x >= areas.get(currentArea).getWidth() || 0 > y || y >= areas.get(currentArea).getHeight())
			return levelTiles.get(0);
		for (Tile t : levelTiles) {
			if (t == null)
				break;
			if (areas.get(currentArea).getTiles()[x + y * areas.get(currentArea).getWidth()] == t.getId())
				return t;
		}
		return levelTiles.get(0);
	}

	public Tile getTile(int tileId) {
		for (Tile t : levelTiles) {
			if (t == null) break;
			if (t.getId() == tileId) return t;
		}
		return levelTiles.get(0);
	}	
	
	public void tick() {
		for (Entity e : levelEntities) {
			e.tick();
		}
        levelEntities.removeAll(levelRemoveEntities);
		levelRemoveEntities.clear();

		for (Entity e : areas.get(currentArea).getAreaEntities()) {
			e.tick();
		}
        areas.get(currentArea).getAreaEntities().removeAll(areas.get(currentArea).getAreaRemoveEntities());
        areas.get(currentArea).getAreaRemoveEntities().clear();

		for (Tile t : levelTiles) {
			if (t == null) break;
			t.tick();
		}
	}

	public Vector2D getNewAreaOnLoadTile(int x, int y, int movingDir) {
		for (LoadTile lt : areas.get(currentArea).getLoadTiles()) {
			if (lt.getOriginVector().x == x && lt.getOriginVector().y == y && movingDir == lt.getTriggerMovingDir()) {
				this.setCurrentArea(lt.getDestinationArea());
				return lt.getDestinationVector();
			}
		}
		return null;
	}
	
	public boolean collideWithLoadTile(int x, int y, int movingDir) {
		for (LoadTile lt : areas.get(currentArea).getLoadTiles()) {
			if (lt.getOriginVector().x == x && lt.getOriginVector().y == y && movingDir == lt.getTriggerMovingDir()) {
				return true;
			}
		}
		return false;
	}

	public void renderTiles(Screen screen, int xOffset, int yOffset) {
		if (xOffset < 0)
			xOffset = 0;
		if (xOffset > ((areas.get(currentArea).getWidth() * this.levelSpriteSheetSize) - screen.width))
			xOffset = ((areas.get(currentArea).getWidth() * this.levelSpriteSheetSize) - screen.width);
		if (yOffset < 0)
			yOffset = 0;
		if (yOffset > ((areas.get(currentArea).getHeight() * this.levelSpriteSheetSize) - screen.height))
			yOffset = ((areas.get(currentArea).getHeight() * this.levelSpriteSheetSize) - screen.height);

		screen.setOffset(xOffset, yOffset);

		if (this.isWipeScreen()) {
			screen.screenWipe();
			this.setWipeScreen(false);
		}		
		
		for (int y = (yOffset >> this.levelSpriteSheetBinary); y < (yOffset + screen.height >> this.levelSpriteSheetBinary) + 1; y++) {
			for (int x = (xOffset >> this.levelSpriteSheetBinary); x < (xOffset + screen.width >> this.levelSpriteSheetBinary) + 1; x++) {
				getTile(x, y).render(screen, this, x * this.levelSpriteSheetSize, y * this.levelSpriteSheetSize);
			}
		}
	}
	
	public void renderEntities(Screen screen) {
		List<Entity> renderList = getAllEntities();
		Collections.sort(renderList, EntityComparator.getComparator(EntityComparator.VECTOR_Y));
		for (Entity e : renderList) {
			e.render(screen);
		}
	}
	
	public void renderEntitiesText(Screen screen, Graphics g) {
		List<Entity> renderList = getAllEntities();
		Collections.sort(renderList, EntityComparator.getComparator(EntityComparator.VECTOR_Y));
		for (Entity e : renderList) {
			e.renderText(screen, g);
		}
	}
	
	public void addEntity(Entity entity) {
		this.levelEntities.add(entity);
	}

	public ArrayList<Entity> getAllEntities() {
		return new ArrayList<Entity>(Stream.concat(areas.get(currentArea).getAreaEntities().stream(), levelEntities.stream()).collect(Collectors.toList()));
	}
	
	public int[] getFaceByName(String speakerName) {
		List<Entity> entityList = getAllEntities();
		for (Entity e : entityList) {
			if (e.isInteractable()) {
				if (e.getName().equals(speakerName)) {
					if (e instanceof Mob) {
						return ((Mob) e).getSpriteSet().getPassiveSprites().get(1);
					}
				}
			}
		}
		return null;
	}
	
	/*******************************************GETTERS AND SETTERS***********************************************/	
	public int getCurrentArea() {
		return currentArea;
	}
	public void setCurrentArea(int currentArea) {
		this.currentArea = currentArea;
	}
	public String getName() {
		return name;
	}
	public String getDisplayName() {
		return displayName;
	}
	public boolean isWipeScreen() {
		return wipeScreen;
	}
	public void setWipeScreen(boolean wipeScreen) {
		this.wipeScreen = wipeScreen;
	}
	public List<Entity> getLevelEntities() {
		return levelEntities;
	}
	public void setLevelEntities(List<Entity> levelEntities) {
		this.levelEntities = levelEntities;
	}
	public int getLevelId() {
		return levelId;
	}
	public void setLevelId(int levelId) {
		this.levelId = levelId;
	}
	public List<Area> getAreas() {
		return areas;
	}
	public void setAreas(List<Area> areas) {
		this.areas = areas;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
}
