package trx.puzzlerpg.main.level.tiles;

public enum Collision {
	Full,
	North,
	East,
	South,
	West,
	NorthEast,
	SouthEast,
	SouthWest,
	NorthWest,
	NorthEastDiagonal, 
	SouthEastDiagonal, 
	SouthWestDiagonal, 
	NorthWestDiagonal,
	ForwardDiagonalTop, 
	BackwardDiagonalTop, 
	ForwardDiagonalBottom, 
	BackwardDiagonalBottom;
}
