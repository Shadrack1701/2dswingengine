package trx.puzzlerpg.main.level.tiles;

import trx.puzzlerpg.main.util.Vector2D;

public class LoadTile {

	private Vector2D originVector;
	private Vector2D destinationVector;
	private int originArea;
	private int destinationArea;
	private int triggerMovingDir;

	public Vector2D getOriginVector() {
		return originVector;
	}
	public void setOriginVector(Vector2D originVector) {
		this.originVector = originVector;
	}
	public Vector2D getDestinationVector() {
		return destinationVector;
	}
	public void setDestinationVector(Vector2D destinationVector) {
		this.destinationVector = destinationVector;
	}
	public int getOriginArea() {
		return originArea;
	}
	public void setOriginArea(int originArea) {
		this.originArea = originArea;
	}
	public int getDestinationArea() {
		return destinationArea;
	}
	public void setDestinationArea(int destinationArea) {
		this.destinationArea = destinationArea;
	}
	public int getTriggerMovingDir() {
		return triggerMovingDir;
	}
	public void setTriggerMovingDir(int triggerMovingDir) {
		this.triggerMovingDir = triggerMovingDir;
	}
	
}
