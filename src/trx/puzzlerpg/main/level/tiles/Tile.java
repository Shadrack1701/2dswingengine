package trx.puzzlerpg.main.level.tiles;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import trx.puzzlerpg.main.gfx.Screen;
import trx.puzzlerpg.main.level.Level;

public class Tile {

	private byte id;
	private String name;
	private String levelColor;
	private int tileAnimationDelay;
	private boolean animationSwitch;
	private boolean animationRandomize;
	private int currentAnimationIndex;
	private long lastIterationTime;
	private TileParticles tileParticles;
	private Collision collision;
	//colliding tile is always subTiles[0]
	private int[] subTiles;
	
	public EnumSet<TileTypes> properties;
	private List<int[]> tileImages = new ArrayList<int[]>();
	private List<TileTypes> loadProperties = new ArrayList<TileTypes>();

	public void render(Screen screen, Level level, int x, int y) {
		screen.render(x, y, this.tileImages.get(this.currentAnimationIndex), 0x00, 1, level.levelSpriteSheetSize, level.levelSpriteSheetSize);
	}

	public String getLevelColor() {
		return this.levelColor;
	}

	public void tick() {
		if (animationSwitch) {
			if ((System.currentTimeMillis() - lastIterationTime >= tileAnimationDelay)) {
				lastIterationTime = System.currentTimeMillis();
				currentAnimationIndex = (currentAnimationIndex + 1) % tileImages.size();
			}
		}
	}

/*******************************************GETTERS AND SETTERS***********************************************/	
	public byte getId() {
		return id;
	}
	public void setTileProperties(EnumSet<TileTypes> properties) {
		this.properties = properties;
	}
	public TileParticles getTileParticles() {
		return tileParticles;
	}
	public void setTileParticles(TileParticles tileParticles) {
		this.tileParticles = tileParticles;
	}
	public EnumSet<TileTypes> getProperties() {
		return properties;
	}
	public void setProperties(EnumSet<TileTypes> properties) {
		this.properties = properties;
	}
	public List<TileTypes> getLoadProperties() {
		return loadProperties;
	}
	public void setLoadProperties(List<TileTypes> loadProperties) {
		this.loadProperties = loadProperties;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isAnimationSwitch() {
		return animationSwitch;
	}
	public void setAnimationSwitch(boolean animationSwitch) {
		this.animationSwitch = animationSwitch;
	}
	public boolean isAnimationRandomize() {
		return animationRandomize;
	}
	public void setAnimationRandomize(boolean animationRandomize) {
		this.animationRandomize = animationRandomize;
	}
	public List<int[]> getTileImages() {
		return tileImages;
	}
	public void setTileImages(List<int[]> tileImages) {
		this.tileImages = tileImages;
	}
	public int getCurrentAnimationIndex() {
		return currentAnimationIndex;
	}
	public void setCurrentAnimationIndex(int currentAnimationIndex) {
		this.currentAnimationIndex = currentAnimationIndex;
	}
	public long getLastIterationTime() {
		return lastIterationTime;
	}
	public void setLastIterationTime(long lastIterationTime) {
		this.lastIterationTime = lastIterationTime;
	}
	public void setId(byte id) {
		this.id = id;
	}
	public void setLevelColor(String levelColor) {
		this.levelColor = levelColor;
	}
	public void setTileAnimationDelay(int tileAnimationDelay) {
		this.tileAnimationDelay = tileAnimationDelay;
	}

	public int getTileAnimationDelay() {
		return tileAnimationDelay;
	}
	public Collision getCollision() {
		return collision;
	}
	public void setCollision(Collision collision) {
		this.collision = collision;
	}

	/**
	 * @return the subTiles
	 */
	public int[] getSubTiles() {
		return subTiles;
	}

	/**
	 * @param subTiles the subTiles to set
	 */
	public void setSubTiles(int[] subTiles) {
		this.subTiles = subTiles;
	}

}
