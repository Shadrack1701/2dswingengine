package trx.puzzlerpg.main.level.tiles;

import java.util.ArrayList;
import java.util.List;

import trx.puzzlerpg.main.util.Vector2D;

public class TileParticles {
	
	private List<int[]> enterImages = new ArrayList<int[]>();
	private Vector2D enterOffset;
	private int enterRate;
	private boolean enterPreEntity;
	
	private List<int[]> movingUpImages = new ArrayList<int[]>();
	private Vector2D movingUpOffset;
	private List<int[]> movingDownImages = new ArrayList<int[]>();
	private Vector2D movingDownOffset;
	private List<int[]> movingLeftImages = new ArrayList<int[]>();
	private Vector2D movingLeftOffset;
	private List<int[]> movingRightImages = new ArrayList<int[]>();
	private Vector2D movingRightOffset;
	private int movingRate;
	private boolean movingPreEntity;
	
	private List<int[]> exitImages = new ArrayList<int[]>();
	private Vector2D exitOffset;
	private int exitRate;
	private boolean exitPreEntity;	

	private List<int[]> passiveImages = new ArrayList<int[]>();
	private Vector2D passiveOffset;
	private int passiveRate;
	private boolean passivePreEntity;
	
/*******************************************GETTERS AND SETTERS***********************************************/		
	public List<int[]> getEnterImages() {
		return enterImages;
	}
	public void setEnterImages(List<int[]> enterImages) {
		this.enterImages = enterImages;
	}
	public Vector2D getEnterOffset() {
		return enterOffset;
	}
	public void setEnterOffset(Vector2D enterOffset) {
		this.enterOffset = enterOffset;
	}
	public int getEnterRate() {
		return enterRate;
	}
	public void setEnterRate(int enterRate) {
		this.enterRate = enterRate;
	}
	public boolean isEnterPreEntity() {
		return enterPreEntity;
	}
	public void setEnterPreEntity(boolean enterPreEntity) {
		this.enterPreEntity = enterPreEntity;
	}
	public List<int[]> getMovingUpImages() {
		return movingUpImages;
	}
	public void setMovingUpImages(List<int[]> movingUpImages) {
		this.movingUpImages = movingUpImages;
	}
	public Vector2D getMovingUpOffset() {
		return movingUpOffset;
	}
	public void setMovingUpOffset(Vector2D movingUpOffset) {
		this.movingUpOffset = movingUpOffset;
	}
	public List<int[]> getMovingDownImages() {
		return movingDownImages;
	}
	public void setMovingDownImages(List<int[]> movingDownImages) {
		this.movingDownImages = movingDownImages;
	}
	public Vector2D getMovingDownOffset() {
		return movingDownOffset;
	}
	public void setMovingDownOffset(Vector2D movingDownOffset) {
		this.movingDownOffset = movingDownOffset;
	}
	public List<int[]> getMovingLeftImages() {
		return movingLeftImages;
	}
	public void setMovingLeftImages(List<int[]> movingLeftImages) {
		this.movingLeftImages = movingLeftImages;
	}
	public Vector2D getMovingLeftOffset() {
		return movingLeftOffset;
	}
	public void setMovingLeftOffset(Vector2D movingLeftOffset) {
		this.movingLeftOffset = movingLeftOffset;
	}
	public List<int[]> getMovingRightImages() {
		return movingRightImages;
	}
	public void setMovingRightImages(List<int[]> movingRightImages) {
		this.movingRightImages = movingRightImages;
	}
	public Vector2D getMovingRightOffset() {
		return movingRightOffset;
	}
	public void setMovingRightOffset(Vector2D movingRightOffset) {
		this.movingRightOffset = movingRightOffset;
	}
	public int getMovingRate() {
		return movingRate;
	}
	public void setMovingRate(int movingRate) {
		this.movingRate = movingRate;
	}
	public boolean isMovingPreEntity() {
		return movingPreEntity;
	}
	public void setMovingPreEntity(boolean movingPreEntity) {
		this.movingPreEntity = movingPreEntity;
	}
	public List<int[]> getExitImages() {
		return exitImages;
	}
	public void setExitImages(List<int[]> exitImages) {
		this.exitImages = exitImages;
	}
	public Vector2D getExitOffset() {
		return exitOffset;
	}
	public void setExitOffset(Vector2D exitOffset) {
		this.exitOffset = exitOffset;
	}
	public int getExitRate() {
		return exitRate;
	}
	public void setExitRate(int exitRate) {
		this.exitRate = exitRate;
	}
	public boolean isExitPreEntity() {
		return exitPreEntity;
	}
	public void setExitPreEntity(boolean exitPreEntity) {
		this.exitPreEntity = exitPreEntity;
	}
	public List<int[]> getPassiveImages() {
		return passiveImages;
	}
	public void setPassiveImages(List<int[]> passiveImages) {
		this.passiveImages = passiveImages;
	}
	public Vector2D getPassiveOffset() {
		return passiveOffset;
	}
	public void setPassiveOffset(Vector2D passiveOffset) {
		this.passiveOffset = passiveOffset;
	}
	public int getPassiveRate() {
		return passiveRate;
	}
	public void setPassiveRate(int passiveRate) {
		this.passiveRate = passiveRate;
	}
	public boolean isPassivePreEntity() {
		return passivePreEntity;
	}
	public void setPassivePreEntity(boolean passivePreEntity) {
		this.passivePreEntity = passivePreEntity;
	}	
}
	