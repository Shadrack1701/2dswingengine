package trx.puzzlerpg.main.level.tiles;

public enum TileTypes {
	    Basic,
		Void,
		Solid,
		Liquid,
		Load,
		Hot,
		Cold,
		Slippery;
}
