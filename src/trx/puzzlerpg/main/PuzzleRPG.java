package trx.puzzlerpg.main;

import java.awt.*;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import javax.swing.*;

import org.imgscalr.Scalr;
import trx.puzzlerpg.main.filemanagement.FileManager;
import trx.puzzlerpg.main.filemanagement.GameInfo;
import trx.puzzlerpg.main.gfx.Screen;
import trx.puzzlerpg.main.state.DialogueState;
import trx.puzzlerpg.main.state.GameState;
import trx.puzzlerpg.main.state.InfoState;
import trx.puzzlerpg.main.state.MenuState;
import trx.puzzlerpg.main.state.PauseState;
import trx.puzzlerpg.main.state.StateManager;

public class PuzzleRPG extends Canvas implements Runnable {	
//standard turn based battles, heath is sanity, any annoying or aggravating real life situation turns into a battle
//'in game' combat is free roam?
//TODO:  Movement doesn't timescale
//TODO:  Text Position and Size doesn't Size Scale...switch to sprites?????
//TODO:  Can we stretch an image to a canvas?  Performance is garbage with larger scale.

	
	private static final long serialVersionUID = 1L;
	private BufferedImage image;
	private int[] pixels;
	private Screen screen;
	private JFrame frame;
	private static GameInfo gameInfo;
    private static boolean running = false;

	public static StateManager stateManager = new StateManager();
	public static Handler handler = new Handler();

	public PuzzleRPG() {
		gameInfo = FileManager.getGameInfo();
		
		image = new BufferedImage(gameInfo.getWidth(), gameInfo.getHeight(), BufferedImage.TYPE_INT_RGB);
		pixels = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();
	
		setMinimumSize(new Dimension(gameInfo.getWidth() * gameInfo.getScale(), gameInfo.getHeight() * gameInfo.getScale()));
		setMaximumSize(new Dimension(gameInfo.getWidth() * gameInfo.getScale(), gameInfo.getHeight() * gameInfo.getScale()));
		setPreferredSize(new Dimension(gameInfo.getWidth() * gameInfo.getScale(), gameInfo.getHeight() * gameInfo.getScale()));

        frame = new JFrame(gameInfo.getDisplayName());
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.setLayout(new BorderLayout());
		frame.add(this, BorderLayout.CENTER);
        frame.pack();
		frame.setResizable(true);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	public void init() {
		screen = new Screen(gameInfo.getWidth(), gameInfo.getHeight());
		InputHandler input = new InputHandler(this);
		handler.setScreen(screen);
		handler.setInputHandler(input);
		PauseState.setPauseMenu(gameInfo.getPauseMenu());
		MenuState.setMenu(gameInfo.getMainMenu());
		InfoState.setInfoBox(gameInfo.getInfoBox());
		DialogueState.setInfoBox(gameInfo.getInfoBox());
		GameState.setHud(gameInfo.getHud());
        //gameInfo.getProjectileLibrary();
		
		stateManager.states.add(new MenuState());	
	}

	public synchronized void start() {
		if (running) return;
		running = true;
		Thread thread = new Thread(this);
		thread.start();
	}

	public static synchronized void stop() {
		if (!running) return;
		running = false;
	}

	public void run() {
		
		init();
		long lastTime = System.nanoTime();
		long lastTimer = System.currentTimeMillis();
		double nsPerTick = 1000000000D / gameInfo.getTickRate();
        double nsPerRender = 1000000000D / gameInfo.getRenderRate();
		double tDelta = 0, rDelta = 0;
		int ticks = 0;
		int frames = 0;
		this.requestFocus();
		
		while (running) {
			long now = System.nanoTime();
			tDelta += (now - lastTime) / nsPerTick;
            rDelta += (now - lastTime) / nsPerRender;
			lastTime = now;
			boolean shouldRender = false;

			while (tDelta >= 1) {
				ticks++;
				tick();
				tDelta -= 1;
				shouldRender = true;
			}
			
			if (rDelta >= 1) {
				frames++;
				render();
                rDelta -= 1;
			}

			if (System.currentTimeMillis() - lastTimer >= 1000) {
				lastTimer += 1000;
				frame.setTitle(ticks + " ticks , " + frames + " frames per second");
				frames = 0;
				ticks = 0;
			}
		}
		frame.dispose();
	}

	public void tick() {
		stateManager.tick(handler);
	}

	public void render() {
		BufferStrategy bs = getBufferStrategy();
		if (bs == null) {
			createBufferStrategy(2);
			return;
		}
		stateManager.render(handler);

        System.arraycopy(screen.pixels, 0, pixels, 0, screen.pixels.length);

		Graphics g = bs.getDrawGraphics();
		g.drawImage(image, 0, 0, getWidth(), getHeight(), null);
		stateManager.renderText(handler, g);

		g.dispose();
		bs.show();
	}

	public static void main(String[] args) {
		new PuzzleRPG().start();
	}

}
