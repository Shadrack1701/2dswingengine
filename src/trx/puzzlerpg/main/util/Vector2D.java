package trx.puzzlerpg.main.util;

public class Vector2D {

	public float x;
	public float y;
	
	public Vector2D() {
		this.x = 0;
		this.y = 0;
	}	
	
	public Vector2D(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public Vector2D(float x, float y) {
		this.x = x;
		this.y = y;
	}

	public int getIntX() {
		return (int) this.x;
	}
	
	public int getIntY() {
		return (int) this.y;
	}
}
