package trx.puzzlerpg.main.util;

import java.util.Comparator;
import trx.puzzlerpg.main.entities.Entity;

public enum EntityComparator implements Comparator<Entity> {
	VECTOR_Y {
		public int compare(Entity e1, Entity e2) {
			return Integer.valueOf(e1.vector.getIntY()).compareTo(e2.vector.getIntY());
		}
	},
	VECTOR_X {
		public int compare(Entity e1, Entity e2) {
			return Integer.valueOf(e1.vector.getIntX()).compareTo(e2.vector.getIntX());
		}
	};

	public static Comparator<Entity> decending(final Comparator<Entity> other) {
		return new Comparator<Entity>() {
			public int compare(Entity e1, Entity e2) {
				return -1 * other.compare(e1, e2);
			}
		};
	}

	public static Comparator<Entity> getComparator(final EntityComparator... multipleOptions) {
		return new Comparator<Entity>() {
			public int compare(Entity e1, Entity e2) {
				for (EntityComparator option : multipleOptions) {
					int result = option.compare(e1, e2);
					if (result != 0) {
						return result;
					}
				}
				return 0;
			}
		};
	}

}
