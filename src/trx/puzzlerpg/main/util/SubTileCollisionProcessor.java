package trx.puzzlerpg.main.util;

import java.awt.Rectangle;
import java.awt.geom.Line2D;
import trx.puzzlerpg.main.entities.Mob;
import trx.puzzlerpg.main.level.tiles.Collision;

public class SubTileCollisionProcessor {

	private static Rectangle collisionRectangle;
	private static Rectangle mobRectangle;
	
	public static boolean processSubTileCollision(Mob mob, Collision collision, int x, int xa, int y, int ya, int tempMovingDir) {
		collisionRectangle = getCollisionRectangle(mob, x, xa, y, ya);
		mobRectangle = new Rectangle(mob.vector.getIntX() + xa + mob.getCollisionCoords().get(0).getIntX(), mob.vector.getIntY() + ya + mob.getCollisionCoords().get(0).getIntY(), mob.getCollision().width, mob.getCollision().height);
		switch (collision) {
			case North: return processNorthCollision();
			case East:	return processEastCollision();
			case South:	return processSouthCollision();
			case West:	return processWestCollision();
			case NorthEast:	return processNorthEastCollision();
			case SouthEast:	return processSouthEastCollision();
			case SouthWest:	return processSouthWestCollision();
			case NorthWest:	return processNorthWestCollision();
			case NorthEastDiagonal:	return processNorthEastDiagonalCollision(tempMovingDir);
			case SouthEastDiagonal:	return processSouthEastDiagonalCollision(tempMovingDir);
			case SouthWestDiagonal:	return processSouthWestDiagonalCollision(tempMovingDir);
			case NorthWestDiagonal:	return processNorthWestDiagonalCollision(tempMovingDir);
			case ForwardDiagonalTop:  return processForwardDiagonalTopCollision(tempMovingDir);
			case BackwardDiagonalTop: return processBackwardDiagonalTopCollision(tempMovingDir);
			case ForwardDiagonalBottom:  return processForwardDiagonalBottomCollision(tempMovingDir);
			case BackwardDiagonalBottom: return processBackwardDiagonalBottomCollision(tempMovingDir);
			case Full: return true;
		}
		
		return false;
	}

	public static boolean processSubTileCollision(Mob mob, Collision collision, int xa, int ya, int tempMovingDir, Rectangle collisionRectangle) {
		SubTileCollisionProcessor.collisionRectangle = collisionRectangle;
		mobRectangle = new Rectangle(mob.vector.getIntX() + xa + mob.getCollisionCoords().get(0).getIntX(), mob.vector.getIntY() + ya + mob.getCollisionCoords().get(0).getIntY(), mob.getCollision().width, mob.getCollision().height);
		switch (collision) {
			case North: return processNorthCollision();
			case East:	return processEastCollision();
			case South:	return processSouthCollision();
			case West:	return processWestCollision();
			case NorthEast:	return processNorthEastCollision();
			case SouthEast:	return processSouthEastCollision();
			case SouthWest:	return processSouthWestCollision();
			case NorthWest:	return processNorthWestCollision();
			case NorthEastDiagonal:	return processFullNorthEastDiagonalCollision();
			case SouthEastDiagonal:	return processFullSouthEastDiagonalCollision();
			case SouthWestDiagonal:	return processFullSouthWestDiagonalCollision();
			case NorthWestDiagonal:	return processFullNorthWestDiagonalCollision();
			case ForwardDiagonalTop:  return processFullForwardDiagonalTopCollision();
			case BackwardDiagonalTop: return processFullBackwardDiagonalTopCollision();
			case ForwardDiagonalBottom:  return processFullForwardDiagonalBottomCollision();
			case BackwardDiagonalBottom: return processFullBackwardDiagonalBottomCollision();
			case Full: return true;
		}
		
		return false;
	}
	
	public static boolean processSubTileCollision(Collision collision, Rectangle collisionRectangle, Rectangle mobRectangle) {
		SubTileCollisionProcessor.collisionRectangle = collisionRectangle;
		SubTileCollisionProcessor.mobRectangle = mobRectangle;
		switch (collision) {
			case North: return processNorthCollision();
			case East:	return processEastCollision();
			case South:	return processSouthCollision();
			case West:	return processWestCollision();
			case NorthEast:	return processNorthEastCollision();
			case SouthEast:	return processSouthEastCollision();
			case SouthWest:	return processSouthWestCollision();
			case NorthWest:	return processNorthWestCollision();
			case NorthEastDiagonal:	return processFullNorthEastDiagonalCollision();
			case SouthEastDiagonal:	return processFullSouthEastDiagonalCollision();
			case SouthWestDiagonal:	return processFullSouthWestDiagonalCollision();
			case NorthWestDiagonal:	return processFullNorthWestDiagonalCollision();
			case ForwardDiagonalTop:  return processFullForwardDiagonalTopCollision();
			case BackwardDiagonalTop: return processFullBackwardDiagonalTopCollision();
			case ForwardDiagonalBottom:  return processFullForwardDiagonalBottomCollision();
			case BackwardDiagonalBottom: return processFullBackwardDiagonalBottomCollision();
			case Full: return true;
		}
		
		return false;
	}
	
	private static boolean processNorthCollision() {
		Rectangle northRectangle = new Rectangle(collisionRectangle.x, collisionRectangle.y, collisionRectangle.width, collisionRectangle.height / 2);
		if (northRectangle.intersects(mobRectangle)) return true;
		return false;
	}

	private static boolean processEastCollision() {
		Rectangle eastRectangle = new Rectangle(collisionRectangle.x + collisionRectangle.width / 2, collisionRectangle.y, collisionRectangle.width / 2, collisionRectangle.height);
		if (eastRectangle.intersects(mobRectangle)) return true;
		return false;
	}

	private static boolean processSouthCollision() {
		Rectangle southRectangle = new Rectangle(collisionRectangle.x, collisionRectangle.y + collisionRectangle.height / 2,  collisionRectangle.width, collisionRectangle.height / 2);
		if (southRectangle.intersects(mobRectangle)) return true;
		return false;
	}

	private static boolean processWestCollision() {
		Rectangle westRectangle = new Rectangle(collisionRectangle.x, collisionRectangle.y, collisionRectangle.width / 2, collisionRectangle.height);		
		if (westRectangle.intersects(mobRectangle)) return true;
		return false;
	}

	private static boolean processNorthEastCollision() {
		Rectangle northEastRectangle = new Rectangle(collisionRectangle.x + collisionRectangle.width / 2, collisionRectangle.y, collisionRectangle.width / 2, collisionRectangle.height / 2);
		if (northEastRectangle.intersects(mobRectangle)) return true;
		return false;
	}

	private static boolean processSouthEastCollision() {
		Rectangle southEastRectangle = new Rectangle(collisionRectangle.x + collisionRectangle.width / 2, collisionRectangle.y + collisionRectangle.height / 2, collisionRectangle.width / 2, collisionRectangle.height / 2);		
		if (southEastRectangle.intersects(mobRectangle)) return true;
		return false;
	}

	private static boolean processSouthWestCollision() {
		Rectangle southWestRectangle = new Rectangle(collisionRectangle.x, collisionRectangle.y + collisionRectangle.height / 2, collisionRectangle.width / 2, collisionRectangle.height / 2);		
		if (southWestRectangle.intersects(mobRectangle)) return true;
		return false;
	}

	private static boolean processNorthWestCollision() {
		Rectangle westRectangle = new Rectangle(collisionRectangle.x, collisionRectangle.y, collisionRectangle.width / 2, collisionRectangle.height / 2);		
		if (westRectangle.intersects(mobRectangle)) return true;
		return false;
	}

	private static boolean processNorthWestDiagonalCollision(int tempMovingDir) {		
		if (tempMovingDir == 0) {
			Line2D hypotenuse = new Line2D.Double();
			hypotenuse.setLine(collisionRectangle.x + collisionRectangle.width / 2, collisionRectangle.y, collisionRectangle.x, collisionRectangle.y + collisionRectangle.height / 2);
			if (hypotenuse.intersects(mobRectangle)) return true;
		} else if (tempMovingDir == 1) {
			Line2D northLine    = new Line2D.Double();
			northLine.setLine(collisionRectangle.x, collisionRectangle.y, collisionRectangle.x + collisionRectangle.width / 2, collisionRectangle.y);
			if (northLine.intersects(mobRectangle)) return true; 
		} else if (tempMovingDir == 2) {
			Line2D hypotenuse = new Line2D.Double();
			hypotenuse.setLine(collisionRectangle.x + collisionRectangle.width / 2, collisionRectangle.y, collisionRectangle.x, collisionRectangle.y + collisionRectangle.height / 2);
			if (hypotenuse.intersects(mobRectangle)) return true;
		} else if (tempMovingDir == 3) {
			Line2D westLine  = new Line2D.Double();
			westLine.setLine(collisionRectangle.x, collisionRectangle.y, collisionRectangle.x, collisionRectangle.y + collisionRectangle.height / 2);
			if (westLine.intersects(mobRectangle)) return true;
		}
		return false;
	}

	private static boolean processSouthWestDiagonalCollision(int tempMovingDir) {		
		if (tempMovingDir == 0) {
			Line2D southLine    = new Line2D.Double();
			southLine.setLine(collisionRectangle.x, collisionRectangle.y + collisionRectangle.height, collisionRectangle.x + collisionRectangle.width / 2, collisionRectangle.y + collisionRectangle.height);
			if (southLine.intersects(mobRectangle)) return true; 
		} else if (tempMovingDir == 1) {
			Line2D hypotenuse = new Line2D.Double();
			hypotenuse.setLine(collisionRectangle.x, collisionRectangle.y + collisionRectangle.height / 2, collisionRectangle.x + collisionRectangle.width / 2, collisionRectangle.y + collisionRectangle.height);
			if (hypotenuse.intersects(mobRectangle)) return true;
		} else if (tempMovingDir == 2) {
			Line2D hypotenuse = new Line2D.Double();
			hypotenuse.setLine(collisionRectangle.x, collisionRectangle.y + collisionRectangle.height / 2, collisionRectangle.x + collisionRectangle.width / 2, collisionRectangle.y + collisionRectangle.height);
			if (hypotenuse.intersects(mobRectangle)) return true;
		} else if (tempMovingDir == 3) {
			Line2D westLine  = new Line2D.Double();
			westLine.setLine(collisionRectangle.x, collisionRectangle.y + collisionRectangle.height / 2, collisionRectangle.x, collisionRectangle.y + collisionRectangle.height);
			if (westLine.intersects(mobRectangle)) return true;
		}
		return false;
	}

	private static boolean processSouthEastDiagonalCollision(int tempMovingDir) {		
		if (tempMovingDir == 0) {
			Line2D southLine    = new Line2D.Double();
			southLine.setLine(collisionRectangle.x + collisionRectangle.width / 2, collisionRectangle.y + collisionRectangle.height, collisionRectangle.x + collisionRectangle.width, collisionRectangle.y + collisionRectangle.height);
			if (southLine.intersects(mobRectangle)) return true; 
		} else if (tempMovingDir == 1) {
			Line2D hypotenuse = new Line2D.Double();
			hypotenuse.setLine(collisionRectangle.x + collisionRectangle.width, collisionRectangle.y + collisionRectangle.height / 2, collisionRectangle.x + collisionRectangle.width / 2, collisionRectangle.y + collisionRectangle.height);
			if (hypotenuse.intersects(mobRectangle)) return true;
		} else if (tempMovingDir == 2) {
			Line2D eastLine  = new Line2D.Double();
			eastLine.setLine(collisionRectangle.x + collisionRectangle.width, collisionRectangle.y + collisionRectangle.height / 2, collisionRectangle.x + collisionRectangle.width, collisionRectangle.y + collisionRectangle.height);
			if (eastLine.intersects(mobRectangle)) return true;
		} else if (tempMovingDir == 3) {
			Line2D hypotenuse = new Line2D.Double();
			hypotenuse.setLine(collisionRectangle.x + collisionRectangle.width, collisionRectangle.y + collisionRectangle.height / 2, collisionRectangle.x + collisionRectangle.width / 2, collisionRectangle.y + collisionRectangle.height);
			if (hypotenuse.intersects(mobRectangle)) return true;
		}
		return false;
	}

	private static boolean processNorthEastDiagonalCollision(int tempMovingDir) {		
		if (tempMovingDir == 0) {
			Line2D hypotenuse = new Line2D.Double();
			hypotenuse.setLine(collisionRectangle.x + collisionRectangle.width / 2, collisionRectangle.y, collisionRectangle.x + collisionRectangle.width, collisionRectangle.y + collisionRectangle.height / 2);
			if (hypotenuse.intersects(mobRectangle)) return true;
		} else if (tempMovingDir == 1) {
			Line2D northLine    = new Line2D.Double();
			northLine.setLine(collisionRectangle.x + collisionRectangle.width / 2, collisionRectangle.y, collisionRectangle.x + collisionRectangle.width, collisionRectangle.y);
			if (northLine.intersects(mobRectangle)) return true; 
		} else if (tempMovingDir == 2) {
			Line2D eastLine  = new Line2D.Double();
			eastLine.setLine(collisionRectangle.x + collisionRectangle.width, collisionRectangle.y, collisionRectangle.x + collisionRectangle.width, collisionRectangle.y + collisionRectangle.height / 2);
			if (eastLine.intersects(mobRectangle)) return true;
		} else if (tempMovingDir == 3) {
			Line2D hypotenuse = new Line2D.Double();
			hypotenuse.setLine(collisionRectangle.x + collisionRectangle.width / 2, collisionRectangle.y, collisionRectangle.x + collisionRectangle.width, collisionRectangle.y + collisionRectangle.height / 2);
			if (hypotenuse.intersects(mobRectangle)) return true;
		}
		return false;
	}
	
	private static boolean processForwardDiagonalTopCollision(int tempMovingDir) {		
		if (tempMovingDir == 0) {
			Line2D hypotenuse = new Line2D.Double();
			hypotenuse.setLine(collisionRectangle.x, collisionRectangle.y, collisionRectangle.x + collisionRectangle.width, collisionRectangle.y + collisionRectangle.height);
			if (hypotenuse.intersects(mobRectangle)) return true;
		} else if (tempMovingDir == 1) {
			Line2D northLine    = new Line2D.Double();
			northLine.setLine(collisionRectangle.x, collisionRectangle.y, collisionRectangle.x + collisionRectangle.width, collisionRectangle.y);
			if (northLine.intersects(mobRectangle)) return true; 
		} else if (tempMovingDir == 2) {
			Line2D eastLine  = new Line2D.Double();
			eastLine.setLine(collisionRectangle.x + collisionRectangle.width, collisionRectangle.y, collisionRectangle.x + collisionRectangle.width, collisionRectangle.y + collisionRectangle.height);
			if (eastLine.intersects(mobRectangle)) return true;
		} else if (tempMovingDir == 3) {
			Line2D hypotenuse = new Line2D.Double();
			hypotenuse.setLine(collisionRectangle.x, collisionRectangle.y, collisionRectangle.x + collisionRectangle.width, collisionRectangle.y + collisionRectangle.height);
			if (hypotenuse.intersects(mobRectangle)) return true;
		}
		return false;
	}
	
	private static boolean processForwardDiagonalBottomCollision(int tempMovingDir) {		
		if (tempMovingDir == 0) {
			Line2D southLine    = new Line2D.Double();
			southLine.setLine(collisionRectangle.x, collisionRectangle.y + collisionRectangle.height, collisionRectangle.x + collisionRectangle.width, collisionRectangle.y + collisionRectangle.height);
			if (mobRectangle.intersectsLine(southLine)) return true; 
		} else if (tempMovingDir == 1) {
			Line2D hypotenuse = new Line2D.Double();
			hypotenuse.setLine(collisionRectangle.x, collisionRectangle.y, collisionRectangle.x + collisionRectangle.width, collisionRectangle.y + collisionRectangle.height);
			if (hypotenuse.intersects(mobRectangle)) return true;
		} else if (tempMovingDir == 2) {
			Line2D hypotenuse = new Line2D.Double();
			hypotenuse.setLine(collisionRectangle.x, collisionRectangle.y, collisionRectangle.x + collisionRectangle.width, collisionRectangle.y + collisionRectangle.height);
			if (hypotenuse.intersects(mobRectangle)) return true;
		} else if (tempMovingDir == 3) {
			Line2D westLine  = new Line2D.Double();
			westLine.setLine(collisionRectangle.x, collisionRectangle.y, collisionRectangle.x, collisionRectangle.y + collisionRectangle.height);
			if (mobRectangle.intersectsLine(westLine)) return true;
		}
		return false;

	}

	private static boolean processBackwardDiagonalTopCollision(int tempMovingDir) {		
		if (tempMovingDir == 0) {
			Line2D hypotenuse = new Line2D.Double();
			hypotenuse.setLine(collisionRectangle.x + collisionRectangle.width, collisionRectangle.y, collisionRectangle.x, collisionRectangle.y + collisionRectangle.height);
			if (hypotenuse.intersects(mobRectangle)) return true;
		} else if (tempMovingDir == 1) {		
			Line2D northLine    = new Line2D.Double();
			northLine.setLine(collisionRectangle.x, collisionRectangle.y, collisionRectangle.x + collisionRectangle.width, collisionRectangle.y);
			if (mobRectangle.intersectsLine(northLine)) return true; 	
			Line2D hypotenuse = new Line2D.Double();
			hypotenuse.setLine(collisionRectangle.x + collisionRectangle.width, collisionRectangle.y, collisionRectangle.x, collisionRectangle.y + collisionRectangle.height);
			if (hypotenuse.intersects(mobRectangle)) return true;
		} else if (tempMovingDir == 2) {
			Line2D hypotenuse = new Line2D.Double();
			hypotenuse.setLine(collisionRectangle.x + collisionRectangle.width, collisionRectangle.y, collisionRectangle.x, collisionRectangle.y + collisionRectangle.height);
			if (hypotenuse.intersects(mobRectangle)) return true;
		} else if (tempMovingDir == 3) {
			Line2D westLine  = new Line2D.Double();
			westLine.setLine(collisionRectangle.x, collisionRectangle.y, collisionRectangle.x, collisionRectangle.y + collisionRectangle.height);
			if (mobRectangle.intersectsLine(westLine)) return true;
		}
		return false;
	}
	
	private static boolean processBackwardDiagonalBottomCollision(int tempMovingDir) {		
		if (tempMovingDir == 0) {
			Line2D southLine    = new Line2D.Double();
			southLine.setLine(collisionRectangle.x, collisionRectangle.y + collisionRectangle.height, collisionRectangle.x + collisionRectangle.width, collisionRectangle.y + collisionRectangle.height);
			if (mobRectangle.intersectsLine(southLine)) return true; 
		} else if (tempMovingDir == 1) {
			Line2D hypotenuse = new Line2D.Double();
			hypotenuse.setLine(collisionRectangle.x + collisionRectangle.width, collisionRectangle.y, collisionRectangle.x, collisionRectangle.y + collisionRectangle.height);
			if (hypotenuse.intersects(mobRectangle)) return true;
		} else if (tempMovingDir == 2) {
			Line2D eastLine  = new Line2D.Double();
			eastLine.setLine(collisionRectangle.x + collisionRectangle.width, collisionRectangle.y, collisionRectangle.x + collisionRectangle.width, collisionRectangle.y + collisionRectangle.height);
			if (eastLine.intersects(mobRectangle)) return true;
		} else if (tempMovingDir == 3) {
			Line2D hypotenuse = new Line2D.Double();
			hypotenuse.setLine(collisionRectangle.x + collisionRectangle.width, collisionRectangle.y, collisionRectangle.x, collisionRectangle.y + collisionRectangle.height);
			if (hypotenuse.intersects(mobRectangle)) return true;
		}
		return false;
	}

	private static boolean processFullNorthWestDiagonalCollision() {		
		Line2D hypotenuse = new Line2D.Double();
		hypotenuse.setLine(collisionRectangle.x + collisionRectangle.width / 2, collisionRectangle.y, collisionRectangle.x, collisionRectangle.y + collisionRectangle.height / 2);;
		if (hypotenuse.intersects(mobRectangle)) return true;
		
		Line2D northLine    = new Line2D.Double();
		northLine.setLine(collisionRectangle.x, collisionRectangle.y, collisionRectangle.x + collisionRectangle.width / 2, collisionRectangle.y);
		if (northLine.intersects(mobRectangle)) return true; 	
		
		Line2D westLine  = new Line2D.Double();
		westLine.setLine(collisionRectangle.x, collisionRectangle.y, collisionRectangle.x, collisionRectangle.y + collisionRectangle.height / 2);
		if (westLine.intersects(mobRectangle)) return true;

		return false;
	}

	private static boolean processFullSouthWestDiagonalCollision() {		
		Line2D hypotenuse = new Line2D.Double();
		hypotenuse.setLine(collisionRectangle.x, collisionRectangle.y + collisionRectangle.height / 2, collisionRectangle.x + collisionRectangle.width / 2, collisionRectangle.y + collisionRectangle.height);
		if (hypotenuse.intersects(mobRectangle)) return true;		
		
		Line2D southLine    = new Line2D.Double();
		southLine.setLine(collisionRectangle.x, collisionRectangle.y + collisionRectangle.height, collisionRectangle.x + collisionRectangle.width / 2, collisionRectangle.y + collisionRectangle.height);
		if (southLine.intersects(mobRectangle)) return true; 
		
		Line2D westLine  = new Line2D.Double();
		westLine.setLine(collisionRectangle.x, collisionRectangle.y + collisionRectangle.height / 2, collisionRectangle.x, collisionRectangle.y + collisionRectangle.height);
		if (westLine.intersects(mobRectangle)) return true;

		return false;
	}

	private static boolean processFullSouthEastDiagonalCollision() {		
		Line2D hypotenuse = new Line2D.Double();
		hypotenuse.setLine(collisionRectangle.x + collisionRectangle.width, collisionRectangle.y + collisionRectangle.height / 2, collisionRectangle.x + collisionRectangle.width / 2, collisionRectangle.y + collisionRectangle.height);
		if (hypotenuse.intersects(mobRectangle)) return true;	
	
		Line2D southLine    = new Line2D.Double();
		southLine.setLine(collisionRectangle.x + collisionRectangle.width / 2, collisionRectangle.y + collisionRectangle.height, collisionRectangle.x + collisionRectangle.width, collisionRectangle.y + collisionRectangle.height);
		if (southLine.intersects(mobRectangle)) return true; 
		
		Line2D eastLine  = new Line2D.Double();
		eastLine.setLine(collisionRectangle.x + collisionRectangle.width, collisionRectangle.y + collisionRectangle.height / 2, collisionRectangle.x + collisionRectangle.width, collisionRectangle.y + collisionRectangle.height);
		if (eastLine.intersects(mobRectangle)) return true;
		
		return false;
	}

	private static boolean processFullNorthEastDiagonalCollision() {		
		Line2D hypotenuse = new Line2D.Double();
		hypotenuse.setLine(collisionRectangle.x + collisionRectangle.width / 2, collisionRectangle.y, collisionRectangle.x + collisionRectangle.width, collisionRectangle.y + collisionRectangle.height / 2);
		if (hypotenuse.intersects(mobRectangle)) return true;		
		
		Line2D northLine    = new Line2D.Double();
		northLine.setLine(collisionRectangle.x + collisionRectangle.width / 2, collisionRectangle.y, collisionRectangle.x + collisionRectangle.width, collisionRectangle.y);
		if (northLine.intersects(mobRectangle)) return true; 
	
		Line2D eastLine  = new Line2D.Double();
		eastLine.setLine(collisionRectangle.x + collisionRectangle.width, collisionRectangle.y, collisionRectangle.x + collisionRectangle.width, collisionRectangle.y + collisionRectangle.height / 2);
		if (eastLine.intersects(mobRectangle)) return true;
		
		return false;
	}
	
	private static boolean processFullForwardDiagonalTopCollision() {		
		Line2D hypotenuse = new Line2D.Double();
		hypotenuse.setLine(collisionRectangle.x, collisionRectangle.y, collisionRectangle.x + collisionRectangle.width, collisionRectangle.y + collisionRectangle.height);
		if (hypotenuse.intersects(mobRectangle)) return true;
		
		Line2D northLine    = new Line2D.Double();
		northLine.setLine(collisionRectangle.x, collisionRectangle.y, collisionRectangle.x + collisionRectangle.width, collisionRectangle.y);
		if (northLine.intersects(mobRectangle)) return true; 
		
		Line2D eastLine  = new Line2D.Double();
		eastLine.setLine(collisionRectangle.x + collisionRectangle.width, collisionRectangle.y, collisionRectangle.x + collisionRectangle.width, collisionRectangle.y + collisionRectangle.height);
		if (eastLine.intersects(mobRectangle)) return true;
		
		return false;
	}
	
	private static boolean processFullForwardDiagonalBottomCollision() {
		Line2D hypotenuse = new Line2D.Double();
		hypotenuse.setLine(collisionRectangle.x, collisionRectangle.y, collisionRectangle.x + collisionRectangle.width, collisionRectangle.y + collisionRectangle.height);
		if (hypotenuse.intersects(mobRectangle)) return true;	
		
		Line2D southLine    = new Line2D.Double();
		southLine.setLine(collisionRectangle.x, collisionRectangle.y + collisionRectangle.height, collisionRectangle.x + collisionRectangle.width, collisionRectangle.y + collisionRectangle.height);
		if (mobRectangle.intersectsLine(southLine)) return true; 
		
		Line2D westLine  = new Line2D.Double();
		westLine.setLine(collisionRectangle.x, collisionRectangle.y, collisionRectangle.x, collisionRectangle.y + collisionRectangle.height);
		if (mobRectangle.intersectsLine(westLine)) return true;
		
		return false;

	}

	private static boolean processFullBackwardDiagonalTopCollision() {
		Line2D hypotenuse = new Line2D.Double();
		hypotenuse.setLine(collisionRectangle.x + collisionRectangle.width, collisionRectangle.y, collisionRectangle.x, collisionRectangle.y + collisionRectangle.height);
		if (hypotenuse.intersects(mobRectangle)) return true;		
		
		Line2D northLine    = new Line2D.Double();
		northLine.setLine(collisionRectangle.x, collisionRectangle.y, collisionRectangle.x + collisionRectangle.width, collisionRectangle.y);
		if (mobRectangle.intersectsLine(northLine)) return true; 	
		
		Line2D westLine  = new Line2D.Double();
		westLine.setLine(collisionRectangle.x, collisionRectangle.y, collisionRectangle.x, collisionRectangle.y + collisionRectangle.height);
		if (mobRectangle.intersectsLine(westLine)) return true;

		return false;
	}
		
	private static boolean processFullBackwardDiagonalBottomCollision() {
		Line2D hypotenuse = new Line2D.Double();
		hypotenuse.setLine(collisionRectangle.x + collisionRectangle.width, collisionRectangle.y, collisionRectangle.x, collisionRectangle.y + collisionRectangle.height);
		if (hypotenuse.intersects(mobRectangle)) return true;
		
		Line2D southLine    = new Line2D.Double();
		southLine.setLine(collisionRectangle.x, collisionRectangle.y + collisionRectangle.height, collisionRectangle.x + collisionRectangle.width, collisionRectangle.y + collisionRectangle.height);			
		if (mobRectangle.intersectsLine(southLine)) return true;
		
		Line2D eastLine  = new Line2D.Double();
		eastLine.setLine(collisionRectangle.x + collisionRectangle.width, collisionRectangle.y, collisionRectangle.x + collisionRectangle.width, collisionRectangle.y + collisionRectangle.height); 
		if (eastLine.intersects(mobRectangle)) return true;
		
		return false;
	}

	private static Rectangle getCollisionRectangle(Mob mob, int x, int xa, int y ,int ya) {
		int minX = (mob.vector.getIntX() + xa + x >> mob.getLevel().levelSpriteSheetBinary) * mob.getLevel().levelSpriteSheetSize;
		int minY = (mob.vector.getIntY() + ya + y >> mob.getLevel().levelSpriteSheetBinary) * mob.getLevel().levelSpriteSheetSize;

		return new Rectangle(minX, minY, mob.getLevel().levelSpriteSheetSize, mob.getLevel().levelSpriteSheetSize);
	}
	
}
