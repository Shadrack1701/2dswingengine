package trx.puzzlerpg.main;

import trx.puzzlerpg.main.entities.subtypes.Player;
import trx.puzzlerpg.main.gfx.Screen;
import trx.puzzlerpg.main.level.Level;

public class Handler {
	
	private Level level;
	private Player player;
	private Screen screen;
	private InputHandler inputHandler;

/*******************************************GETTERS AND SETTERS***********************************************/	
	public Level getLevel() {
		return level;
	}
	public void setLevel(Level level) {
		this.level = level;
	}
	public Player getPlayer() {
		return player;
	}
	public void setPlayer(Player player) {
		this.player = player;
	}
	public Screen getScreen() {
		return screen;
	}
	public void setScreen(Screen screen) {
		this.screen = screen;
	}
	public InputHandler getInputHandler() {
		return inputHandler;
	}
	public void setInputHandler(InputHandler inputHandler) {
		this.inputHandler = inputHandler;
	}
}
