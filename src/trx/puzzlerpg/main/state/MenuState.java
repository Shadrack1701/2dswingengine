package trx.puzzlerpg.main.state;

import java.awt.Graphics;

import trx.puzzlerpg.main.Handler;
import trx.puzzlerpg.main.PuzzleRPG;
import trx.puzzlerpg.main.gfx.gui.MainMenu;

public class MenuState extends State {

	private static MainMenu mainMenu;
	private static long lastUpInput = 0;
	private static long lastDownInput = 0;
	private static long lastEnterInput = 0;

	@Override
	public void tick(Handler handler) {
		if (handler.getInputHandler().e.isPressed() || handler.getInputHandler().enter.isPressed()) {
			long temp = System.currentTimeMillis() - lastEnterInput;
			if (temp > 1000) {
				if (mainMenu.getSelectedOption() == 0) {
					PuzzleRPG.stateManager.states.add(new NewGameState(handler));
				} else if (mainMenu.getSelectedOption() == 1) {
					//PuzzleRPG.stateManager.states.add(new LoadGameState());
				} else if (mainMenu.getSelectedOption() == 2) {
					//PuzzleRPG.stateManager.states.add(new SettingsState());
				} else {
					PuzzleRPG.stop();
				}
			} 
		} else lastEnterInput = 0;
		if (handler.getInputHandler().up.isPressed()) {
			if (System.currentTimeMillis() - lastUpInput > 500) {
				if (mainMenu.getSelectedOption() > 0) mainMenu.setSelectedOption(mainMenu.getSelectedOption() - 1);
				lastUpInput = System.currentTimeMillis();
			}
		} else lastUpInput = 0;
			
		if (handler.getInputHandler().down.isPressed()) {
			if (System.currentTimeMillis() - lastDownInput > 500) {	
				if (mainMenu.getSelectedOption() < 3) mainMenu.setSelectedOption(mainMenu.getSelectedOption() + 1);
				lastDownInput = System.currentTimeMillis();
			}
		} else lastDownInput = 0;

	}

	@Override
	public void render(Handler handler) {
		handler.getScreen().screenWipe();
		handler.getScreen().render(mainMenu.getTitleLoc().getIntX() + handler.getScreen().xOffset, mainMenu.getTitleLoc().getIntY() + handler.getScreen().yOffset, mainMenu.getTitleImage(), 0x00, 1, mainMenu.getTitleWidth(), mainMenu.getTitleHeight());
		if (mainMenu.getSelectedOption() == 0) {
			handler.getScreen().render(mainMenu.getNewGameLoc().getIntX() + handler.getScreen().xOffset, mainMenu.getNewGameLoc().getIntY() + handler.getScreen().yOffset, mainMenu.getNewGameSelImage(), 0x00, 1, mainMenu.getNewGameSelWidth(), mainMenu.getNewGameSelHeight());
		} else { 			
			handler.getScreen().render(mainMenu.getNewGameLoc().getIntX() + handler.getScreen().xOffset, mainMenu.getNewGameLoc().getIntY() + handler.getScreen().yOffset, mainMenu.getNewGameImage(), 0x00, 1, mainMenu.getNewGameWidth(), mainMenu.getNewGameHeight());
		}
		
		if (mainMenu.getSelectedOption() == 1) {
			handler.getScreen().render(mainMenu.getContinueLoc().getIntX() + handler.getScreen().xOffset, mainMenu.getContinueLoc().getIntY() + handler.getScreen().yOffset, mainMenu.getContinueSelImage(), 0x00, 1, mainMenu.getContinueSelWidth(), mainMenu.getContinueSelHeight());
		} else { 
			handler.getScreen().render(mainMenu.getContinueLoc().getIntX() + handler.getScreen().xOffset, mainMenu.getContinueLoc().getIntY() + handler.getScreen().yOffset, mainMenu.getContinueImage(), 0x00, 1, mainMenu.getContinueWidth(), mainMenu.getContinueHeight());
		}
		
		if (mainMenu.getSelectedOption() == 2) {
			handler.getScreen().render(mainMenu.getSettingsLoc().getIntX() + handler.getScreen().xOffset, mainMenu.getSettingsLoc().getIntY() + handler.getScreen().yOffset, mainMenu.getSettingsSelImage(), 0x00, 1, mainMenu.getSettingsSelWidth(), mainMenu.getSettingsSelHeight());
		} else {
			handler.getScreen().render(mainMenu.getSettingsLoc().getIntX() + handler.getScreen().xOffset, mainMenu.getSettingsLoc().getIntY() + handler.getScreen().yOffset, mainMenu.getSettingsImage(), 0x00, 1, mainMenu.getSettingsWidth(), mainMenu.getSettingsHeight());
		}
		
		if (mainMenu.getSelectedOption() == 3) {
			handler.getScreen().render(mainMenu.getQuitDesktopLoc().getIntX() + handler.getScreen().xOffset, mainMenu.getQuitDesktopLoc().getIntY() + handler.getScreen().yOffset, mainMenu.getQuitDesktopSelImage(), 0x00, 1, mainMenu.getQuitDesktopSelWidth(), mainMenu.getQuitDesktopSelHeight());
		} else {
			handler.getScreen().render(mainMenu.getQuitDesktopLoc().getIntX() + handler.getScreen().xOffset, mainMenu.getQuitDesktopLoc().getIntY() + handler.getScreen().yOffset, mainMenu.getQuitDesktopImage(), 0x00, 1, mainMenu.getQuitDesktopWidth(), mainMenu.getQuitDesktopHeight());
		}
		handler.getScreen().render(mainMenu.getCursorLoc().getIntX() + handler.getScreen().xOffset, mainMenu.getCursorLoc().getIntY() + 55 * mainMenu.getSelectedOption() + handler.getScreen().yOffset, mainMenu.getCursorImage(), 0x00, 1, mainMenu.getCursorWidth(), mainMenu.getCursorHeight());

	}

	/******************************************** GETTERS AND SETTERS***********************************************/
	public MainMenu getMenu() {
		return mainMenu;
	}
	public static void setMenu(MainMenu menu) {
		MenuState.mainMenu = menu;
	}
	public static MainMenu getMainMenu() {
		return mainMenu;
	}
	public static void setLastEnterInput(long lastEnterInput) {
		MenuState.lastEnterInput = lastEnterInput;
	}

	@Override
	public void renderText(Handler handler, Graphics g) {
		// TODO Auto-generated method stub
		
	}

}
