package trx.puzzlerpg.main.state;

import java.awt.Graphics;
import java.util.Stack;

import trx.puzzlerpg.main.Handler;

public class StateManager {

	public Stack<State> states = new Stack<State>();
	
	public void tick(Handler handler) {
		states.peek().tick(handler);
	}
	
	public void render(Handler handler) {
		states.peek().render(handler);
	}
	
	public void renderText(Handler handler, Graphics g) {
		states.peek().renderText(handler, g);
	}
}
