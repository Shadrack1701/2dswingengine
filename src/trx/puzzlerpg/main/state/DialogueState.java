package trx.puzzlerpg.main.state;

import java.awt.Graphics;
import trx.puzzlerpg.main.Handler;
import trx.puzzlerpg.main.PuzzleRPG;
import trx.puzzlerpg.main.gfx.gui.Conversation;
import trx.puzzlerpg.main.gfx.gui.InfoBox;

public class DialogueState extends State {

	private static InfoBox infoBox;
	private static long lastEnterInput = 0;
	private static Conversation conversation;
	private static int currentDialogue;
	private static boolean fullMessage;
	private static long msgTimer;
	private static long autoTimer;
	private static int msgCount;
	
	public DialogueState(Conversation conversation) {
		autoTimer = 0;
		currentDialogue = 0;
		msgCount = 1;
		msgTimer = System.currentTimeMillis();
		DialogueState.conversation = conversation;
		conversation.getDialogues().get(0).setDisplayedMessage(conversation.getDialogues().get(0).getMessage().substring(0, msgCount));
		infoBox.setInfoDialogue(conversation.getDialogues().get(0));
	}

	@Override
	public void tick(Handler handler) {
		if (handler.getInputHandler().e.isPressed() || handler.getInputHandler().enter.isPressed() || handler.getInputHandler().esc.isPressed() || handler.getInputHandler().space.isPressed()) {
			long temp = System.currentTimeMillis() - lastEnterInput;
			if (temp > 1000 || lastEnterInput == 0) {
				processDialogue(handler);
			} else {
				processScrollDialogue(handler);
			}
		} else { 
			lastEnterInput = 0;
			processScrollDialogue(handler);
		}
	}

	public void processDialogue(Handler handler) {
		if (conversation.getDialogues().get(currentDialogue).getDisplayedMessage().equals(conversation.getDialogues().get(currentDialogue).getMessage())) {
			currentDialogue++;
			msgCount = 0;
			lastEnterInput = System.currentTimeMillis();
		} else {
			conversation.getDialogues().get(currentDialogue).setDisplayedMessage(conversation.getDialogues().get(currentDialogue).getMessage());
			infoBox.setMsgText(conversation.getDialogues().get(currentDialogue).getDisplayedMessage());
			lastEnterInput = System.currentTimeMillis();
			msgCount = conversation.getDialogues().get(currentDialogue).getDisplayedMessage().length() - 1;
			return;
		}
		if (currentDialogue >= conversation.getDialogues().size()) {
			PuzzleRPG.stateManager.states.pop();
			handler.getPlayer().setLastInteract(System.currentTimeMillis());
		} else {
			handler.getLevel().renderTiles(handler.getScreen(), handler.getScreen().xOffset, handler.getScreen().yOffset);
			handler.getLevel().renderEntities(handler.getScreen());
			GameState.getHud().render(handler);
			conversation.getDialogues().get(currentDialogue).setDisplayedMessage(conversation.getDialogues().get(currentDialogue).getMessage().substring(0, msgCount));
			infoBox.setInfoDialogue(conversation.getDialogues().get(currentDialogue));
			lastEnterInput = System.currentTimeMillis();
		}
	}
	
	public void processScrollDialogue(Handler handler) {
		long temp = System.currentTimeMillis() - msgTimer;
		if (temp >= conversation.getDialogues().get(currentDialogue).getDisplaySpeed() && msgCount < conversation.getDialogues().get(currentDialogue).getMessage().length()) {
			msgCount++;
			conversation.getDialogues().get(currentDialogue).setDisplayedMessage(conversation.getDialogues().get(currentDialogue).getMessage().substring(0, msgCount));
			infoBox.setMsgText(conversation.getDialogues().get(currentDialogue).getDisplayedMessage());
			msgTimer = System.currentTimeMillis();
		} else if (msgCount >= conversation.getDialogues().get(currentDialogue).getMessage().length() && conversation.getDialogues().get(currentDialogue).isAutoRead()) {
			if (autoTimer == 0) autoTimer = System.currentTimeMillis(); 
			long autoTemp = System.currentTimeMillis() - autoTimer;
			if (autoTemp >= 1000) {
				processDialogue(handler);
			}
		} 
	}
	
	@Override
	public void render(Handler handler) {
		infoBox.render(handler);
	}
	
	@Override
	public void renderText(Handler handler, Graphics g) {
		infoBox.renderText(handler, g);
		
	}

/*******************************************GETTERS AND SETTERS***********************************************/		
	public InfoBox getInfoBox() {
		return infoBox;
	}
	public static void setInfoBox(InfoBox infoBox) {
		DialogueState.infoBox = infoBox;
	}
	public static long getLastEnterInput() {
		return lastEnterInput;
	}
	public static void setLastEnterInput(long lastEnterInput) {
		DialogueState.lastEnterInput = lastEnterInput;
	}
	public static Conversation getConversation() {
		return conversation;
	}
	public static void setConversation(Conversation conversation) {
		DialogueState.conversation = conversation;
	}
	public static int getCurrentDialogue() {
		return currentDialogue;
	}
	public static void setCurrentDialogue(int currentDialogue) {
		DialogueState.currentDialogue = currentDialogue;
	}
	public boolean isFullMessage() {
		return fullMessage;
	}
	public void setFullMessage(boolean fullMessage) {
		DialogueState.fullMessage = fullMessage;
	}
}
