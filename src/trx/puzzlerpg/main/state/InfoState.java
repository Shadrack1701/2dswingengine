package trx.puzzlerpg.main.state;

import java.awt.Graphics;
import java.util.List;
import trx.puzzlerpg.main.Handler;
import trx.puzzlerpg.main.PuzzleRPG;
import trx.puzzlerpg.main.entities.item.Item;
import trx.puzzlerpg.main.gfx.gui.InfoBox;

public class InfoState extends State {

	private static InfoBox infoBox;
	private static long lastEnterInput = 0;
	private static List<Item> displayItems;
	private static int currentItem;
	
	public InfoState(List<Item> items) {
		currentItem = 0;
		displayItems = items;
		infoBox.setInfoItem(displayItems.get(currentItem));
	}

	@Override
	public void tick(Handler handler) {
		if (handler.getInputHandler().e.isPressed() || handler.getInputHandler().enter.isPressed() || handler.getInputHandler().esc.isPressed() || handler.getInputHandler().space.isPressed()) {
			long temp = System.currentTimeMillis() - lastEnterInput;
			if (temp > 1000) {
				currentItem++;
				if (currentItem >= displayItems.size()) {
					PuzzleRPG.stateManager.states.pop();	
				} else {
					handler.getLevel().renderTiles(handler.getScreen(), handler.getScreen().xOffset, handler.getScreen().yOffset);
					handler.getLevel().renderEntities(handler.getScreen());
                    GameState.getHud().render(handler);
					infoBox.setInfoItem(displayItems.get(currentItem));
					lastEnterInput = System.currentTimeMillis();
				}
			} 
		} else lastEnterInput = 0;
	}

	@Override
	public void render(Handler handler) {
		infoBox.render(handler);
	}
	
	@Override
	public void renderText(Handler handler, Graphics g) {
		infoBox.renderText(handler, g);
		
	}

/*******************************************GETTERS AND SETTERS***********************************************/		
	public InfoBox getInfoBox() {
		return infoBox;
	}
	public static void setInfoBox(InfoBox infoBox) {
		InfoState.infoBox = infoBox;
	}
	public static long getLastEnterInput() {
		return lastEnterInput;
	}
	public static void setLastEnterInput(long lastEnterInput) {
		InfoState.lastEnterInput = lastEnterInput;
	}

}
