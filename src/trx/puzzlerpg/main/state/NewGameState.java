package trx.puzzlerpg.main.state;

import java.awt.Graphics;
import trx.puzzlerpg.main.Handler;
import trx.puzzlerpg.main.PuzzleRPG;
import trx.puzzlerpg.main.entities.Entity;
import trx.puzzlerpg.main.entities.subtypes.Player;
import trx.puzzlerpg.main.filemanagement.FileManager;
import trx.puzzlerpg.main.filemanagement.LoadLevel;
import trx.puzzlerpg.main.level.Level;

public class NewGameState extends State {
	
	public NewGameState(Handler handler) {
		LoadLevel loadLevel;
		loadLevel = FileManager.getLevel("level1.json");
		Level level = loadLevel.getGameLevel();
		for (Entity entity: level.getLevelEntities()) {
			if (entity.getClass() == Player.class) {
				Player player = (Player) entity;
				handler.setPlayer(player);
				player.setInputHandler(handler.getInputHandler());
			}
		}
		level.init();
		handler.setLevel(level);
	}
	
	@Override
	public void tick(Handler handler) {
		PuzzleRPG.stateManager.states.pop();
		PuzzleRPG.stateManager.states.add(new GameState());
	}

	@Override
	public void render(Handler handler) {}

	@Override
	public void renderText(Handler handler, Graphics g) {
		// TODO Auto-generated method stub
		
	}

}
