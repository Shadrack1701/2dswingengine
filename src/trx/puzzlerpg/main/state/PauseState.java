package trx.puzzlerpg.main.state;

import java.awt.Graphics;

import trx.puzzlerpg.main.Handler;
import trx.puzzlerpg.main.PuzzleRPG;
import trx.puzzlerpg.main.gfx.gui.PauseMenu;

public class PauseState extends State {

	private static PauseMenu pauseMenu;

	private long lastUpInput = 0;
	private long lastDownInput = 0;

	@Override
	public void tick(Handler handler) {
		if (handler.getInputHandler().e.isPressed() || handler.getInputHandler().enter.isPressed()) {
			if (pauseMenu.getSelectedOption() == 0) {
				PuzzleRPG.stateManager.states.pop();
			} else if (pauseMenu.getSelectedOption() == 1) {
				// handler.getLevel().saveGame();
			} else if (pauseMenu.getSelectedOption() == 2) {
				//PuzzleRPG.stateManager.states.add(new LoadGameState());
			} else if (pauseMenu.getSelectedOption() == 3) {
				//PuzzleRPG.stateManager.states.add(new SettingsState());
			} else if (pauseMenu.getSelectedOption() == 4) {
				PuzzleRPG.stateManager.states.pop();
				PuzzleRPG.stateManager.states.pop();
				MenuState.setLastEnterInput(System.currentTimeMillis());
			} else {
				PuzzleRPG.stop();
			}
		}
		if (handler.getInputHandler().up.isPressed()) {
			if (System.currentTimeMillis() - lastUpInput > 500) {
				if (pauseMenu.getSelectedOption() > 0) pauseMenu.setSelectedOption(pauseMenu.getSelectedOption() - 1);
				lastUpInput = System.currentTimeMillis();
			}
		} else lastUpInput = 0;
			
		if (handler.getInputHandler().down.isPressed()) {
			if (System.currentTimeMillis() - lastDownInput > 500) {	
				if (pauseMenu.getSelectedOption() < 5) pauseMenu.setSelectedOption(pauseMenu.getSelectedOption() + 1);
				lastDownInput = System.currentTimeMillis();
			}
		} else lastDownInput = 0;

	}

	@Override
	public void render(Handler handler) {
		handler.getScreen().screenWipe();
		handler.getScreen().render(pauseMenu.getTitleLoc().getIntX() + handler.getScreen().xOffset, pauseMenu.getTitleLoc().getIntY() + handler.getScreen().yOffset, pauseMenu.getTitleImage(), 0x00, 1, pauseMenu.getTitleWidth(), pauseMenu.getTitleHeight());
		if (pauseMenu.getSelectedOption() == 0) {
			handler.getScreen().render(pauseMenu.getReturnLoc().getIntX() + handler.getScreen().xOffset, pauseMenu.getReturnLoc().getIntY() + handler.getScreen().yOffset, pauseMenu.getReturnSelImage(), 0x00, 1, pauseMenu.getReturnSelWidth(), pauseMenu.getReturnSelHeight());
		} else { 			
			handler.getScreen().render(pauseMenu.getReturnLoc().getIntX() + handler.getScreen().xOffset, pauseMenu.getReturnLoc().getIntY() + handler.getScreen().yOffset, pauseMenu.getReturnImage(), 0x00, 1, pauseMenu.getReturnWidth(), pauseMenu.getReturnHeight());
		}
		
		if (pauseMenu.getSelectedOption() == 1) {
			handler.getScreen().render(pauseMenu.getSaveGameLoc().getIntX() + handler.getScreen().xOffset, pauseMenu.getSaveGameLoc().getIntY() + handler.getScreen().yOffset, pauseMenu.getSaveGameSelImage(), 0x00, 1, pauseMenu.getSaveGameSelWidth(), pauseMenu.getSaveGameSelHeight());
		} else { 
			handler.getScreen().render(pauseMenu.getSaveGameLoc().getIntX() + handler.getScreen().xOffset, pauseMenu.getSaveGameLoc().getIntY() + handler.getScreen().yOffset, pauseMenu.getSaveGameImage(), 0x00, 1, pauseMenu.getSaveGameWidth(), pauseMenu.getSaveGameHeight());
		}
		
		if (pauseMenu.getSelectedOption() == 2) {
			handler.getScreen().render(pauseMenu.getLoadGameLoc().getIntX() + handler.getScreen().xOffset, pauseMenu.getLoadGameLoc().getIntY() + handler.getScreen().yOffset, pauseMenu.getLoadGameSelImage(), 0x00, 1, pauseMenu.getLoadGameSelWidth(), pauseMenu.getLoadGameSelHeight());
		} else {
			handler.getScreen().render(pauseMenu.getLoadGameLoc().getIntX() + handler.getScreen().xOffset, pauseMenu.getLoadGameLoc().getIntY() + handler.getScreen().yOffset, pauseMenu.getLoadGameImage(), 0x00, 1, pauseMenu.getLoadGameWidth(), pauseMenu.getLoadGameHeight());
		}
		
		if (pauseMenu.getSelectedOption() == 3) {
			handler.getScreen().render(pauseMenu.getSettingsLoc().getIntX() + handler.getScreen().xOffset, pauseMenu.getSettingsLoc().getIntY() + handler.getScreen().yOffset, pauseMenu.getSettingsSelImage(), 0x00, 1, pauseMenu.getSettingsSelWidth(), pauseMenu.getSettingsSelHeight());
		} else {
			handler.getScreen().render(pauseMenu.getSettingsLoc().getIntX() + handler.getScreen().xOffset, pauseMenu.getSettingsLoc().getIntY() + handler.getScreen().yOffset, pauseMenu.getSettingsImage(), 0x00, 1, pauseMenu.getSettingsWidth(), pauseMenu.getSettingsHeight());
		}		
		
		if (pauseMenu.getSelectedOption() == 4) {
			handler.getScreen().render(pauseMenu.getQuitTitleLoc().getIntX() + handler.getScreen().xOffset, pauseMenu.getQuitTitleLoc().getIntY() + handler.getScreen().yOffset, pauseMenu.getQuitTitleSelImage(), 0x00, 1, pauseMenu.getQuitTitleSelWidth(), pauseMenu.getQuitTitleSelHeight());
		} else {
			handler.getScreen().render(pauseMenu.getQuitTitleLoc().getIntX() + handler.getScreen().xOffset, pauseMenu.getQuitTitleLoc().getIntY() + handler.getScreen().yOffset, pauseMenu.getQuitTitleImage(), 0x00, 1, pauseMenu.getQuitTitleWidth(), pauseMenu.getQuitTitleHeight());
		}
		
		if (pauseMenu.getSelectedOption() == 5) {
			handler.getScreen().render(pauseMenu.getQuitDesktopLoc().getIntX() + handler.getScreen().xOffset, pauseMenu.getQuitDesktopLoc().getIntY() + handler.getScreen().yOffset, pauseMenu.getQuitDesktopSelImage(), 0x00, 1, pauseMenu.getQuitDesktopSelWidth(), pauseMenu.getQuitDesktopSelHeight());
		} else {
			handler.getScreen().render(pauseMenu.getQuitDesktopLoc().getIntX() + handler.getScreen().xOffset, pauseMenu.getQuitDesktopLoc().getIntY() + handler.getScreen().yOffset, pauseMenu.getQuitDesktopImage(), 0x00, 1, pauseMenu.getQuitDesktopWidth(), pauseMenu.getQuitDesktopHeight());
		}
		handler.getScreen().render(pauseMenu.getCursorLoc().getIntX() + handler.getScreen().xOffset, pauseMenu.getCursorLoc().getIntY() + 25 * pauseMenu.getSelectedOption() + handler.getScreen().yOffset, pauseMenu.getCursorImage(), 0x00, 1, pauseMenu.getCursorWidth(), pauseMenu.getCursorHeight());

	}

	/******************************************** GETTERS AND SETTERS***********************************************/
	public static PauseMenu getPauseMenu() {
		return pauseMenu;
	}

	public static void setPauseMenu(PauseMenu pauseMenu) {
		PauseState.pauseMenu = pauseMenu;
	}

	@Override
	public void renderText(Handler handler, Graphics g) {
		// TODO Auto-generated method stub
		
	}
}
