package trx.puzzlerpg.main.state;

import java.awt.Graphics;

import trx.puzzlerpg.main.Handler;

public abstract class State {
	
	public abstract void tick(Handler handler);
	public abstract void render(Handler handler);
	public abstract void renderText(Handler handler, Graphics g);

}
