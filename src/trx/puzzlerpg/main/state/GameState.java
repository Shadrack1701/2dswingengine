package trx.puzzlerpg.main.state;

import java.awt.Graphics;

import trx.puzzlerpg.main.Handler;
import trx.puzzlerpg.main.PuzzleRPG;
import trx.puzzlerpg.main.gfx.gui.Hud;

public class GameState extends State {
	private static Hud hud;

	@Override
	public void tick(Handler handler) {
		if (handler.getInputHandler().esc.isPressed() || handler.getInputHandler().p.isPressed()) {
			PuzzleRPG.stateManager.states.add(new PauseState());
		}
		handler.getLevel().tick();
		hud.tick(handler);
		
	}

	@Override
	public void render(Handler handler) {

		int xOffset = handler.getPlayer().vector.getIntX() - (handler.getScreen().width / 2);
		int yOffset = handler.getPlayer().vector.getIntY() - (handler.getScreen().height / 2);

		handler.getLevel().renderTiles(handler.getScreen(), xOffset, yOffset);
		handler.getLevel().renderEntities(handler.getScreen());
		
		hud.render(handler);
	}

	@Override
	public void renderText(Handler handler, Graphics g) {
		handler.getLevel().renderEntitiesText(handler.getScreen(), g);
		
	}

	public static Hud getHud() {
		return hud;
	}

	public static void setHud(Hud hud) {
		GameState.hud = hud;
	}

}
