package trx.puzzlerpg.main.gfx;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.Random;

import trx.puzzlerpg.main.util.Vector2D;

public class DamageText {
    private Integer damageAmount;
	private float heightDrop;
	private float xIncrement;
	private float yIncrement;
	private boolean stationary = false;
	private long displayTime; 
	private boolean stopDisplaying = false;
	private float xAdjust = 0, yAdjust = 0;
	private boolean down = false;

    public DamageText(int damage) {
        boolean movingLeft = true;
		this.damageAmount = damage;
		Random rand = new Random();
        int temp = rand.nextInt(3);
		if (rand.nextInt(100) >= 50) movingLeft = false;
		xIncrement = temp + 2;
		if (movingLeft) xIncrement = xIncrement * -1;
		yIncrement = 8 - temp;
		heightDrop = yIncrement * -6;
	}
	
	public void tick() {
		if (!stationary) {
			xAdjust = xAdjust + xIncrement;
			if (yAdjust < heightDrop) down = true;
			if (down) {
				yAdjust = yAdjust + yIncrement;
			} else {
				yAdjust = yAdjust - yIncrement;
			}
			if (yAdjust >= 1) {
				stationary = true;
				displayTime = System.currentTimeMillis();
			}
		} else {
			if (System.currentTimeMillis() - displayTime >= 500) {
				setStopDisplaying(true);
			}
		}
	}
	
	public void render(Screen screen, Graphics g, Vector2D mobVector) {
		int displayX = (mobVector.getIntX() - screen.xOffset + (int) xAdjust) * 2;
		int displayY = (mobVector.getIntY() - screen.yOffset + (int) yAdjust) * 2;
		g.setFont(GameFonts.damageFont);
		g.getFont().deriveFont(Font.BOLD);
		g.setColor(Color.BLACK);
		g.drawString(damageAmount.toString(), displayX, displayY);
	}


	public boolean isStopDisplaying() {
		return stopDisplaying;
	}
	public void setStopDisplaying(boolean stopDisplaying) {
		this.stopDisplaying = stopDisplaying;
	}
}
