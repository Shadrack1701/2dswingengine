package trx.puzzlerpg.main.gfx.gui;

import trx.puzzlerpg.main.util.Vector2D;

public class MainMenu {

    private int[] testRender;
	private Vector2D titleLoc;
	private int[] titleImage;
	private int titleWidth, titleHeight;
	private Vector2D newGameLoc;
	private int[] newGameImage;
	private int newGameWidth, newGameHeight;
	private Vector2D continueLoc;
	private int[] continueImage;
	private int continueWidth, continueHeight;
	private Vector2D settingsLoc;
	private int[] settingsImage;
	private int settingsWidth, settingsHeight;
	private Vector2D quitDesktopLoc;
	private int[] quitDesktopImage;
	private int quitDesktopWidth, quitDesktopHeight;
	private int[] newGameSelImage;
	private int newGameSelWidth, newGameSelHeight;
	private int[] continueSelImage;
	private int continueSelWidth, continueSelHeight;
	private int[] settingsSelImage;
	private int settingsSelWidth, settingsSelHeight;
	private int[] quitDesktopSelImage;
	private int quitDesktopSelWidth, quitDesktopSelHeight;
	private Vector2D cursorLoc;
	private int[] cursorImage;
	private int cursorWidth, cursorHeight;
	private int selectedOption;

    public int[] getTestRender() {
        return testRender;
    }

    public void setTestRender(int[] testRender) {
        this.testRender = testRender;
    }

    /*******************************************GETTERS AND SETTERS***********************************************/
	public Vector2D getTitleLoc() {
		return titleLoc;
	}
	public void setTitleLoc(Vector2D titleLoc) {
		this.titleLoc = titleLoc;
	}
	public int[] getTitleImage() {
		return titleImage;
	}
	public void setTitleImage(int[] titleImage) {
		this.titleImage = titleImage;
	}
	public int getTitleWidth() {
		return titleWidth;
	}
	public void setTitleWidth(int titleWidth) {
		this.titleWidth = titleWidth;
	}
	public int getTitleHeight() {
		return titleHeight;
	}
	public void setTitleHeight(int titleHeight) {
		this.titleHeight = titleHeight;
	}
	public Vector2D getNewGameLoc() {
		return newGameLoc;
	}
	public void setNewGameLoc(Vector2D newGameLoc) {
		this.newGameLoc = newGameLoc;
	}
	public int[] getNewGameImage() {
		return newGameImage;
	}
	public void setNewGameImage(int[] newGameImage) {
		this.newGameImage = newGameImage;
	}
	public int getNewGameWidth() {
		return newGameWidth;
	}
	public void setNewGameWidth(int newGameWidth) {
		this.newGameWidth = newGameWidth;
	}
	public int getNewGameHeight() {
		return newGameHeight;
	}
	public void setNewGameHeight(int newGameHeight) {
		this.newGameHeight = newGameHeight;
	}
	public Vector2D getContinueLoc() {
		return continueLoc;
	}
	public void setContinueLoc(Vector2D continueLoc) {
		this.continueLoc = continueLoc;
	}
	public int[] getContinueImage() {
		return continueImage;
	}
	public void setContinueImage(int[] continueImage) {
		this.continueImage = continueImage;
	}
	public int getContinueWidth() {
		return continueWidth;
	}
	public void setContinueWidth(int continueWidth) {
		this.continueWidth = continueWidth;
	}
	public int getContinueHeight() {
		return continueHeight;
	}
	public void setContinueHeight(int continueHeight) {
		this.continueHeight = continueHeight;
	}
	public Vector2D getSettingsLoc() {
		return settingsLoc;
	}
	public void setSettingsLoc(Vector2D settingsLoc) {
		this.settingsLoc = settingsLoc;
	}
	public int[] getSettingsImage() {
		return settingsImage;
	}
	public void setSettingsImage(int[] settingsImage) {
		this.settingsImage = settingsImage;
	}
	public int getSettingsWidth() {
		return settingsWidth;
	}
	public void setSettingsWidth(int settingsWidth) {
		this.settingsWidth = settingsWidth;
	}
	public int getSettingsHeight() {
		return settingsHeight;
	}
	public void setSettingsHeight(int settingsHeight) {
		this.settingsHeight = settingsHeight;
	}
	public Vector2D getQuitDesktopLoc() {
		return quitDesktopLoc;
	}
	public void setQuitDesktopLoc(Vector2D quitDesktopLoc) {
		this.quitDesktopLoc = quitDesktopLoc;
	}
	public int[] getQuitDesktopImage() {
		return quitDesktopImage;
	}
	public void setQuitDesktopImage(int[] quitDesktopImage) {
		this.quitDesktopImage = quitDesktopImage;
	}
	public int getQuitDesktopWidth() {
		return quitDesktopWidth;
	}
	public void setQuitDesktopWidth(int quitDesktopWidth) {
		this.quitDesktopWidth = quitDesktopWidth;
	}
	public int getQuitDesktopHeight() {
		return quitDesktopHeight;
	}
	public void setQuitDesktopHeight(int quitDesktopHeight) {
		this.quitDesktopHeight = quitDesktopHeight;
	}
	public int[] getNewGameSelImage() {
		return newGameSelImage;
	}
	public void setNewGameSelImage(int[] newGameSelImage) {
		this.newGameSelImage = newGameSelImage;
	}
	public int getNewGameSelWidth() {
		return newGameSelWidth;
	}
	public void setNewGameSelWidth(int newGameSelWidth) {
		this.newGameSelWidth = newGameSelWidth;
	}
	public int getNewGameSelHeight() {
		return newGameSelHeight;
	}
	public void setNewGameSelHeight(int newGameSelHeight) {
		this.newGameSelHeight = newGameSelHeight;
	}
	public int[] getContinueSelImage() {
		return continueSelImage;
	}
	public void setContinueSelImage(int[] continueSelImage) {
		this.continueSelImage = continueSelImage;
	}
	public int getContinueSelWidth() {
		return continueSelWidth;
	}
	public void setContinueSelWidth(int continueSelWidth) {
		this.continueSelWidth = continueSelWidth;
	}
	public int getContinueSelHeight() {
		return continueSelHeight;
	}
	public void setContinueSelHeight(int continueSelHeight) {
		this.continueSelHeight = continueSelHeight;
	}
	public int[] getSettingsSelImage() {
		return settingsSelImage;
	}
	public void setSettingsSelImage(int[] settingsSelImage) {
		this.settingsSelImage = settingsSelImage;
	}
	public int getSettingsSelWidth() {
		return settingsSelWidth;
	}
	public void setSettingsSelWidth(int settingsSelWidth) {
		this.settingsSelWidth = settingsSelWidth;
	}
	public int getSettingsSelHeight() {
		return settingsSelHeight;
	}
	public void setSettingsSelHeight(int settingsSelHeight) {
		this.settingsSelHeight = settingsSelHeight;
	}
	public int[] getQuitDesktopSelImage() {
		return quitDesktopSelImage;
	}
	public void setQuitDesktopSelImage(int[] quitDesktopSelImage) {
		this.quitDesktopSelImage = quitDesktopSelImage;
	}
	public int getQuitDesktopSelWidth() {
		return quitDesktopSelWidth;
	}
	public void setQuitDesktopSelWidth(int quitDesktopSelWidth) {
		this.quitDesktopSelWidth = quitDesktopSelWidth;
	}
	public int getQuitDesktopSelHeight() {
		return quitDesktopSelHeight;
	}
	public void setQuitDesktopSelHeight(int quitDesktopSelHeight) {
		this.quitDesktopSelHeight = quitDesktopSelHeight;
	}
	public Vector2D getCursorLoc() {
		return cursorLoc;
	}
	public void setCursorLoc(Vector2D cursorLoc) {
		this.cursorLoc = cursorLoc;
	}
	public int[] getCursorImage() {
		return cursorImage;
	}
	public void setCursorImage(int[] cursorImage) {
		this.cursorImage = cursorImage;
	}
	public int getCursorWidth() {
		return cursorWidth;
	}
	public void setCursorWidth(int cursorWidth) {
		this.cursorWidth = cursorWidth;
	}
	public int getCursorHeight() {
		return cursorHeight;
	}
	public void setCursorHeight(int cursorHeight) {
		this.cursorHeight = cursorHeight;
	}
	public int getSelectedOption() {
		return selectedOption;
	}
	public void setSelectedOption(int selectedOption) {
		this.selectedOption = selectedOption;
	}
}
