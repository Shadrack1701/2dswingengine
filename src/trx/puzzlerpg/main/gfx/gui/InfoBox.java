package trx.puzzlerpg.main.gfx.gui;

import java.awt.Color;
import java.awt.Graphics;
import trx.puzzlerpg.main.Handler;
import trx.puzzlerpg.main.PuzzleRPG;
import trx.puzzlerpg.main.entities.item.Item;
import trx.puzzlerpg.main.gfx.GameFonts;

public class InfoBox {

	private int infoWidth = 32;
	private int infoHeight = 16;
	private int[] topLeft;
	private int[] topMiddle;
	private int[] topRight;
	private int[] middleLeft;
	private int[] middleMiddle;
	private int[] middleRight;
	private int[] bottomLeft;
	private int[] bottomMiddle;
	private int[] bottomRight;
	private int[] image;
	private int textWidth, textHeight;
	private String msgText;
	private StringBuilder formattedText;
	private int horizontalBoxes, verticalBoxes;
	private Item infoItem;
	private Dialogue infoDialogue;
	private int imageSize;
	
	public InfoBox() {}
	
	public void setInfoItem(Item infoItem) {
		resetInfoBox();
		imageSize = 16;
		this.infoItem = infoItem;
		this.image = infoItem.getIcon();
		if (infoItem.isPluralName()) {
			setFormattedMsgText("You found: " + infoItem.getName());
		} else {
			setFormattedMsgText("You found a: " + infoItem.getName());
		}
	}
	
	public void setInfoDialogue(Dialogue infoDialogue) {
		resetInfoBox();
		imageSize = 32;
		this.infoDialogue = infoDialogue;
		this.image = PuzzleRPG.handler.getLevel().getFaceByName(infoDialogue.getSpeakerName());
		setFormattedMsgText(infoDialogue.getMessage());
		infoDialogue.setMessage(msgText);
		msgText = infoDialogue.getDisplayedMessage();
	}

	public void setFormattedMsgText(String text) {
		formattedText = new StringBuilder();
		setLineBreaks(text);
		msgText = formattedText.toString();
		setTextMetrics(msgText);
	}
	
	public void setMsgText(String text) {
		msgText = text;
	}

	private void setLineBreaks(String text) {
		if (text.length() > 40) {
			StringBuilder textBuilder = new StringBuilder(text);
			int x;
			for (x = 40; x >= 20; x--) {
				if (textBuilder.charAt(x) == ' ') {
					textBuilder.deleteCharAt(x);
					textBuilder.insert(x, "\n");
					formattedText.append(textBuilder.substring(0, x + 1));
					if (textBuilder.length() - x > 40) { 
						setLineBreaks(textBuilder.substring(x + 1, textBuilder.length()));
					} else {
						formattedText.append(textBuilder.substring(x + 1, textBuilder.length()));
					}
					return;
				}
			} 
			textBuilder.insert(39, "-\n");
			if (textBuilder.length() - 39 > 39) {
				formattedText.append(textBuilder.substring(0, 39));
				setLineBreaks(textBuilder.substring(39, textBuilder.length()));
			} else {
				formattedText.append(textBuilder.toString());
			}
		} else {
			formattedText.append(text);
		}
		
	}
	
	private void setTextMetrics(String text) {
		String[] lines = text.split("\n");
		for (String line : lines) {
			if ((line.length()) * 7 > textWidth) textWidth = (line.length()) * 7 ;
			textHeight = textHeight + 8;
		}
		int boxWidth = textWidth / 32;
		int remainder = textWidth % 32;
		if (remainder > 0) boxWidth++;
		horizontalBoxes = boxWidth - 2;
		int boxHeight = textHeight / 16 + 1;
		remainder = textHeight % 16;
		if (remainder > 0) boxHeight++;
		verticalBoxes = boxHeight - 2;
	}

	public void tick(Handler handler) {}
	
	public void render(Handler handler) {
		int overallWidth = (horizontalBoxes + 2) * infoWidth;
		int mainX = handler.getScreen().xOffset + handler.getScreen().width / 2 - overallWidth / 2;
		int mainY = handler.getScreen().yOffset + 10;
		handler.getScreen().render(mainX, mainY, topLeft, 0x00, 1, infoWidth, infoHeight);
		for (int x = 1; x <= horizontalBoxes; x++) {
			handler.getScreen().render(mainX + x * infoWidth, mainY, topMiddle, 0x00, 1, infoWidth, infoHeight);
		}
		handler.getScreen().render(mainX + (horizontalBoxes + 1) * infoWidth, mainY, topRight, 0x00, 1, infoWidth, infoHeight);
		if (verticalBoxes > 2) {
			for (int y = 1; y < verticalBoxes; y++) {
				handler.getScreen().render(mainX, mainY + y * infoWidth, middleLeft, 0x00, 1, infoWidth, infoHeight);
				for (int x = 1; x <= horizontalBoxes;  x++ ) {
					handler.getScreen().render(mainX + x * infoWidth, mainY + y * infoHeight, middleMiddle, 0x00, 1, infoWidth, infoHeight);
				}
				handler.getScreen().render(mainX + (horizontalBoxes + 1) * infoWidth, mainY + y * infoHeight, middleRight, 0x00, 1, infoWidth, infoHeight);
			}
			handler.getScreen().render(mainX, mainY + infoHeight * verticalBoxes, bottomLeft, 0x00, 1, infoWidth, infoHeight);
			for (int x = 1; x <= horizontalBoxes; x++) {
				handler.getScreen().render(mainX + x * infoWidth, mainY + infoHeight * verticalBoxes, bottomMiddle, 0x00, 1, infoWidth, infoHeight);
			}		
			handler.getScreen().render(mainX + (horizontalBoxes + 1) * infoWidth, mainY + infoHeight * verticalBoxes, bottomRight, 0x00, 1, infoWidth, infoHeight);
		} else {
			handler.getScreen().render(mainX, mainY + infoHeight, bottomLeft, 0x00, 1, infoWidth, infoHeight);
			for (int x = 1; x <= horizontalBoxes; x++) {
				handler.getScreen().render(mainX + x * infoWidth, mainY + infoHeight, bottomMiddle, 0x00, 1, infoWidth, infoHeight);
			}		
			handler.getScreen().render(mainX + (horizontalBoxes + 1) * infoWidth, mainY + infoHeight, bottomRight, 0x00, 1, infoWidth, infoHeight);
		}
	
		if (!(image == null)) {
			handler.getScreen().render(mainX - 2 * infoWidth / 2, mainY , topLeft, 0, 0, 16, 16, 0x00, 1, 32);
			handler.getScreen().render(mainX - 1 * infoWidth, mainY, topRight, 16, 0, 32, 16, 0x00, 1, 32);
			handler.getScreen().render(mainX - 2 * infoWidth / 2, mainY + infoHeight, bottomLeft, 0, 0, 16, 16, 0x00, 1, 32);
			handler.getScreen().render(mainX - 1 * infoWidth, mainY + infoHeight, bottomRight, 16, 0, 32, 16, 0x00, 1, 32);
			
			if (imageSize == 16) {
				handler.getScreen().render(mainX - 32 + 8, mainY + imageSize / 2, image, 0,0, imageSize, imageSize, 0x00, 1, imageSize);
			} else {
				handler.getScreen().render(mainX - 32, mainY + 6, image, 2,0, imageSize, 20, 0x00, 1, imageSize);
			}
		}
	}
	
	public void renderText(Handler handler, Graphics g) {
		int overallWidth = (horizontalBoxes + 2) * infoWidth;
		int overallHeight = (verticalBoxes + 2) * infoHeight;
		int mainX = handler.getScreen().xOffset + handler.getScreen().width / 2 - overallWidth / 2;
		int mainY = handler.getScreen().yOffset + 10;
		int x =  mainX + overallWidth / 2 - textWidth / 2 + 6;
		int y =  mainY + overallHeight / 2 - textHeight + 5;
		x = (x - handler.getScreen().xOffset) * 2;
		y = (y - handler.getScreen().yOffset) * 2;
		g.setFont(GameFonts.infoFont);
		g.setColor(Color.WHITE);
		drawString(g, msgText, x, y);
	}	
	
	 private void drawString(Graphics g, String text, int x, int y) {
         for (String line : text.split("\n"))
             g.drawString(line, x, y += g.getFontMetrics().getHeight());
     }
	
	private void resetInfoBox() {
		verticalBoxes = 0;
		horizontalBoxes = 0;
		textWidth = 0;
		textHeight = 0;
	}
	 
/*******************************************GETTERS AND SETTERS***********************************************/	
	public int[] getTopLeft() {
		return topLeft;
	}
	public void setTopLeft(int[] topLeft) {
		this.topLeft = topLeft;
	}
	public int[] getIcon() {
		return image;
	}
	public void setIcon(int[] icon) {
		this.image = icon;
	}
	public int getTextWidth() {
		return textWidth;
	}
	public void setTextWidth(int textWidth) {
		this.textWidth = textWidth;
	}
	public int getTextHeight() {
		return textHeight;
	}
	public void setTextHeight(int textHeight) {
		this.textHeight = textHeight;
	}
	public int getHorizontalBoxes() {
		return horizontalBoxes;
	}
	public void setHorizontalBoxes(int horizontalBoxes) {
		this.horizontalBoxes = horizontalBoxes;
	}
	public int getVerticalBoxes() {
		return verticalBoxes;
	}
	public void setVerticalBoxes(int verticalBoxes) {
		this.verticalBoxes = verticalBoxes;
	}
	public String getMsgText() {
		return msgText;
	}
	public int getInfoWidth() {
		return infoWidth;
	}
	public void setInfoWidth(int infoWidth) {
		this.infoWidth = infoWidth;
	}
	public int getInfoHeight() {
		return infoHeight;
	}
	public void setInfoHeight(int infoHeight) {
		this.infoHeight = infoHeight;
	}
	public int[] getTopMiddle() {
		return topMiddle;
	}
	public void setTopMiddle(int[] topMiddle) {
		this.topMiddle = topMiddle;
	}
	public int[] getTopRight() {
		return topRight;
	}
	public void setTopRight(int[] topRight) {
		this.topRight = topRight;
	}
	public int[] getMiddleLeft() {
		return middleLeft;
	}
	public void setMiddleLeft(int[] middleLeft) {
		this.middleLeft = middleLeft;
	}
	public int[] getMiddleMiddle() {
		return middleMiddle;
	}
	public void setMiddleMiddle(int[] middleMiddle) {
		this.middleMiddle = middleMiddle;
	}
	public int[] getMiddleRight() {
		return middleRight;
	}
	public void setMiddleRight(int[] middleRight) {
		this.middleRight = middleRight;
	}
	public int[] getBottomLeft() {
		return bottomLeft;
	}
	public void setBottomLeft(int[] bottomLeft) {
		this.bottomLeft = bottomLeft;
	}
	public int[] getBottomMiddle() {
		return bottomMiddle;
	}
	public void setBottomMiddle(int[] bottomMiddle) {
		this.bottomMiddle = bottomMiddle;
	}
	public int[] getBottomRight() {
		return bottomRight;
	}
	public void setBottomRight(int[] bottomRight) {
		this.bottomRight = bottomRight;
	}
	public int[] getImage() {
		return image;
	}
	public void setImage(int[] image) {
		this.image = image;
	}
	public Item getInfoItem() {
		return infoItem;
	}
	public StringBuilder getFormattedText() {
		return formattedText;
	}
	public void setFormattedText(StringBuilder formattedText) {
		this.formattedText = formattedText;
	}
	public Dialogue getInfoDialogue() {
		return infoDialogue;
	}
}
