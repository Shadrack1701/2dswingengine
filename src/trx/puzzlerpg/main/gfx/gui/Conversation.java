package trx.puzzlerpg.main.gfx.gui;

import java.util.ArrayList;
import java.util.List;

public class Conversation {
	private int currentDialogue;
	private List<Dialogue> dialogues = new ArrayList<>();
	private String displayedMessage;
	private boolean read;
	private boolean repeatable;
	
/*******************************************GETTERS AND SETTERS***********************************************/		
	public int getCurrentDialogue() {
		return currentDialogue;
	}
	public void setCurrentDialogue(int currentDialogue) {
		this.currentDialogue = currentDialogue;
	}
	public List<Dialogue> getDialogues() {
		return dialogues;
	}
	public void setDialogues(List<Dialogue> dialogues) {
		this.dialogues = dialogues;
	}
	public boolean isRead() {
		return read;
	}
	public void setRead(boolean read) {
		this.read = read;
	}
	public boolean isRepeatable() {
		return repeatable;
	}
	public void setRepeatable(boolean repeatable) {
		this.repeatable = repeatable;
	}
	public String getDisplayedMessage() {
		return displayedMessage;
	}
	public void setDisplayedMessage(String displayedMessage) {
		this.displayedMessage = displayedMessage;
	} 
	
}
