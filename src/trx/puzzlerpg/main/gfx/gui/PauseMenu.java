package trx.puzzlerpg.main.gfx.gui;

import trx.puzzlerpg.main.util.Vector2D;

public class PauseMenu {

	private Vector2D titleLoc;
	private int[] titleImage;
	private int titleWidth, titleHeight;
	private Vector2D returnLoc;
	private int[] returnImage;
	private int returnWidth, returnHeight;
	private Vector2D saveGameLoc;
	private int[] saveGameImage;
	private int saveGameWidth, saveGameHeight;
	private Vector2D loadGameLoc;
	private int[] loadGameImage;
	private int loadGameWidth, loadGameHeight;
	private Vector2D settingsLoc;
	private int[] settingsImage;
	private int settingsWidth, settingsHeight;	
	private Vector2D quitTitleLoc;
	private int[] quitTitleImage;
	private int quitTitleWidth, quitTitleHeight;	
	private Vector2D quitDesktopLoc;
	private int[] quitDesktopImage;
	private int quitDesktopWidth, quitDesktopHeight;
	private int[] returnSelImage;
	private int returnSelWidth, returnSelHeight;
	private int[] saveGameSelImage;
	private int saveGameSelWidth, saveGameSelHeight;
	private int[] loadGameSelImage;
	private int loadGameSelWidth, loadGameSelHeight;
	private int[] settingsSelImage;
	private int settingsSelWidth, settingsSelHeight;
	private int[] quitTitleSelImage;
	private int quitTitleSelWidth, quitTitleSelHeight;
	private int[] quitDesktopSelImage;
	private int quitDesktopSelWidth, quitDesktopSelHeight;
	private Vector2D cursorLoc;
	private int[] cursorImage;
	private int cursorWidth, cursorHeight;
	private int selectedOption;
	
/*******************************************GETTERS AND SETTERS***********************************************/	
	public Vector2D getTitleLoc() {
		return titleLoc;
	}
	public void setTitleLoc(Vector2D titleLoc) {
		this.titleLoc = titleLoc;
	}
	public int[] getTitleImage() {
		return titleImage;
	}
	public void setTitleImage(int[] titleImage) {
		this.titleImage = titleImage;
	}
	public int getTitleWidth() {
		return titleWidth;
	}
	public void setTitleWidth(int titleWidth) {
		this.titleWidth = titleWidth;
	}
	public int getTitleHeight() {
		return titleHeight;
	}
	public void setTitleHeight(int titleHeight) {
		this.titleHeight = titleHeight;
	}
	public Vector2D getReturnLoc() {
		return returnLoc;
	}
	public void setReturnLoc(Vector2D returnLoc) {
		this.returnLoc = returnLoc;
	}
	public int[] getReturnImage() {
		return returnImage;
	}
	public void setReturnImage(int[] returnImage) {
		this.returnImage = returnImage;
	}
	public int getReturnWidth() {
		return returnWidth;
	}
	public void setReturnWidth(int returnWidth) {
		this.returnWidth = returnWidth;
	}
	public int getReturnHeight() {
		return returnHeight;
	}
	public void setReturnHeight(int returnHeight) {
		this.returnHeight = returnHeight;
	}
	public Vector2D getSaveGameLoc() {
		return saveGameLoc;
	}
	public void setSaveGameLoc(Vector2D saveGameLoc) {
		this.saveGameLoc = saveGameLoc;
	}
	public int[] getSaveGameImage() {
		return saveGameImage;
	}
	public void setSaveGameImage(int[] saveGameImage) {
		this.saveGameImage = saveGameImage;
	}
	public int getSaveGameWidth() {
		return saveGameWidth;
	}
	public void setSaveGameWidth(int saveGameWidth) {
		this.saveGameWidth = saveGameWidth;
	}
	public int getSaveGameHeight() {
		return saveGameHeight;
	}
	public void setSaveGameHeight(int saveGameHeight) {
		this.saveGameHeight = saveGameHeight;
	}
	public Vector2D getLoadGameLoc() {
		return loadGameLoc;
	}
	public void setLoadGameLoc(Vector2D loadGameLoc) {
		this.loadGameLoc = loadGameLoc;
	}
	public int[] getLoadGameImage() {
		return loadGameImage;
	}
	public void setLoadGameImage(int[] loadGameImage) {
		this.loadGameImage = loadGameImage;
	}
	public int getLoadGameWidth() {
		return loadGameWidth;
	}
	public void setLoadGameWidth(int loadGameWidth) {
		this.loadGameWidth = loadGameWidth;
	}
	public int getLoadGameHeight() {
		return loadGameHeight;
	}
	public Vector2D getSettingsLoc() {
		return settingsLoc;
	}
	public void setSettingsLoc(Vector2D settingsLoc) {
		this.settingsLoc = settingsLoc;
	}
	public int[] getSettingsImage() {
		return settingsImage;
	}
	public void setSettingsImage(int[] settingsImage) {
		this.settingsImage = settingsImage;
	}
	public int getSettingsWidth() {
		return settingsWidth;
	}
	public void setSettingsWidth(int settingsWidth) {
		this.settingsWidth = settingsWidth;
	}
	public int getSettingsHeight() {
		return settingsHeight;
	}
	public void setSettingsHeight(int settingsHeight) {
		this.settingsHeight = settingsHeight;
	}
	public void setLoadGameHeight(int loadGameHeight) {
		this.loadGameHeight = loadGameHeight;
	}
	public Vector2D getQuitTitleLoc() {
		return quitTitleLoc;
	}
	public void setQuitTitleLoc(Vector2D quitTitleLoc) {
		this.quitTitleLoc = quitTitleLoc;
	}
	public int[] getQuitTitleImage() {
		return quitTitleImage;
	}
	public void setQuitTitleImage(int[] quitTitleImage) {
		this.quitTitleImage = quitTitleImage;
	}
	public int getQuitTitleWidth() {
		return quitTitleWidth;
	}
	public void setQuitTitleWidth(int quitTitleWidth) {
		this.quitTitleWidth = quitTitleWidth;
	}
	public int getQuitTitleHeight() {
		return quitTitleHeight;
	}
	public void setQuitTitleHeight(int quitTitleHeight) {
		this.quitTitleHeight = quitTitleHeight;
	}
	public Vector2D getQuitDesktopLoc() {
		return quitDesktopLoc;
	}
	public void setQuitDesktopLoc(Vector2D quitDesktopLoc) {
		this.quitDesktopLoc = quitDesktopLoc;
	}
	public int[] getQuitDesktopImage() {
		return quitDesktopImage;
	}
	public void setQuitDesktopImage(int[] quitDesktopImage) {
		this.quitDesktopImage = quitDesktopImage;
	}
	public int getQuitDesktopWidth() {
		return quitDesktopWidth;
	}
	public void setQuitDesktopWidth(int quitDesktopWidth) {
		this.quitDesktopWidth = quitDesktopWidth;
	}
	public int getQuitDesktopHeight() {
		return quitDesktopHeight;
	}
	public void setQuitDesktopHeight(int quitDesktopHeight) {
		this.quitDesktopHeight = quitDesktopHeight;
	}
	public int[] getReturnSelImage() {
		return returnSelImage;
	}
	public void setReturnSelImage(int[] returnSelImage) {
		this.returnSelImage = returnSelImage;
	}
	public int getReturnSelWidth() {
		return returnSelWidth;
	}
	public void setReturnSelWidth(int returnSelWidth) {
		this.returnSelWidth = returnSelWidth;
	}
	public int getReturnSelHeight() {
		return returnSelHeight;
	}
	public void setReturnSelHeight(int returnSelHeight) {
		this.returnSelHeight = returnSelHeight;
	}
	public int[] getSaveGameSelImage() {
		return saveGameSelImage;
	}
	public void setSaveGameSelImage(int[] saveGameSelImage) {
		this.saveGameSelImage = saveGameSelImage;
	}
	public int getSaveGameSelWidth() {
		return saveGameSelWidth;
	}
	public void setSaveGameSelWidth(int saveGameSelWidth) {
		this.saveGameSelWidth = saveGameSelWidth;
	}
	public int getSaveGameSelHeight() {
		return saveGameSelHeight;
	}
	public void setSaveGameSelHeight(int saveGameSelHeight) {
		this.saveGameSelHeight = saveGameSelHeight;
	}
	public int[] getLoadGameSelImage() {
		return loadGameSelImage;
	}
	public void setLoadGameSelImage(int[] loadGameSelImage) {
		this.loadGameSelImage = loadGameSelImage;
	}
	public int getLoadGameSelWidth() {
		return loadGameSelWidth;
	}
	public void setLoadGameSelWidth(int loadGameSelWidth) {
		this.loadGameSelWidth = loadGameSelWidth;
	}
	public int getLoadGameSelHeight() {
		return loadGameSelHeight;
	}
	public void setLoadGameSelHeight(int loadGameSelHeight) {
		this.loadGameSelHeight = loadGameSelHeight;
	}
	public int[] getSettingsSelImage() {
		return settingsSelImage;
	}
	public void setSettingsSelImage(int[] settingsSelImage) {
		this.settingsSelImage = settingsSelImage;
	}
	public int getSettingsSelWidth() {
		return settingsSelWidth;
	}
	public void setSettingsSelWidth(int settingsSelWidth) {
		this.settingsSelWidth = settingsSelWidth;
	}
	public int getSettingsSelHeight() {
		return settingsSelHeight;
	}
	public void setSettingsSelHeight(int settingsSelHeight) {
		this.settingsSelHeight = settingsSelHeight;
	}
	public int[] getQuitTitleSelImage() {
		return quitTitleSelImage;
	}
	public void setQuitTitleSelImage(int[] quitTitleSelImage) {
		this.quitTitleSelImage = quitTitleSelImage;
	}
	public int getQuitTitleSelWidth() {
		return quitTitleSelWidth;
	}
	public void setQuitTitleSelWidth(int quitTitleSelWidth) {
		this.quitTitleSelWidth = quitTitleSelWidth;
	}
	public int getQuitTitleSelHeight() {
		return quitTitleSelHeight;
	}
	public void setQuitTitleSelHeight(int quitTitleSelHeight) {
		this.quitTitleSelHeight = quitTitleSelHeight;
	}
	public int[] getQuitDesktopSelImage() {
		return quitDesktopSelImage;
	}
	public void setQuitDesktopSelImage(int[] quitDesktopSelImage) {
		this.quitDesktopSelImage = quitDesktopSelImage;
	}
	public int getQuitDesktopSelWidth() {
		return quitDesktopSelWidth;
	}
	public void setQuitDesktopSelWidth(int quitDesktopSelWidth) {
		this.quitDesktopSelWidth = quitDesktopSelWidth;
	}
	public int getQuitDesktopSelHeight() {
		return quitDesktopSelHeight;
	}
	public void setQuitDesktopSelHeight(int quitDesktopSelHeight) {
		this.quitDesktopSelHeight = quitDesktopSelHeight;
	}
	public Vector2D getCursorLoc() {
		return cursorLoc;
	}
	public void setCursorLoc(Vector2D cursorLoc) {
		this.cursorLoc = cursorLoc;
	}
	public int[] getCursorImage() {
		return cursorImage;
	}
	public void setCursorImage(int[] cursorImage) {
		this.cursorImage = cursorImage;
	}
	public int getCursorWidth() {
		return cursorWidth;
	}
	public void setCursorWidth(int cursorWidth) {
		this.cursorWidth = cursorWidth;
	}
	public int getCursorHeight() {
		return cursorHeight;
	}
	public void setCursorHeight(int cursorHeight) {
		this.cursorHeight = cursorHeight;
	}
	public int getSelectedOption() {
		return selectedOption;
	}
	public void setSelectedOption(int selectedOption) {
		this.selectedOption = selectedOption;
	}	
}
