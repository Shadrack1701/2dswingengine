package trx.puzzlerpg.main.gfx.gui;

public class Dialogue {
	
	private String speakerName;
	private int displaySpeed;
	private String message;
	private String displayedMessage;
	private boolean autoRead;

/*******************************************GETTERS AND SETTERS***********************************************/			
	public String getSpeakerName() {
		return speakerName;
	}
	public void setSpeakerName(String speakerName) {
		this.speakerName = speakerName;
	}
	public int getDisplaySpeed() {
		return displaySpeed;
	}
	public void setDisplaySpeed(int displaySpeed) {
		this.displaySpeed = displaySpeed;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getDisplayedMessage() {
		return displayedMessage;
	}
	public void setDisplayedMessage(String displayedMessage) {
		this.displayedMessage = displayedMessage;
	}
	/**
	 * @return the autoRead
	 */
	public boolean isAutoRead() {
		return autoRead;
	}
	/**
	 * @param autoRead the autoRead to set
	 */
	public void setAutoRead(boolean autoRead) {
		this.autoRead = autoRead;
	} 	
}
