package trx.puzzlerpg.main.gfx.gui;

import trx.puzzlerpg.main.Handler;

public class Hud {

	private int[] healthBarImage;
	private int healthBarWidth, healthBarHeight;
	private int[] healthImage;
	private int healthWidth, healthHeight;	
	
	public void tick(Handler handler) {
		
	}
	
	public void render(Handler handler) {
		handler.getScreen().render(2 + healthBarHeight * 2 + handler.getScreen().xOffset, handler.getScreen().height - healthBarHeight * 2 + handler.getScreen().yOffset, healthBarImage, 0x00, 2, healthBarWidth, healthBarHeight);
		handler.getScreen().render(20 + healthBarHeight * 2 + handler.getScreen().xOffset, handler.getScreen().height - healthBarHeight * 2 + handler.getScreen().yOffset + 18, healthImage, 0, 0, (int) ((double) handler.getPlayer().getCurrHealth() / (double) handler.getPlayer().getMaxHealth() * (double) healthWidth), healthHeight, 0x00, 2, healthWidth, healthHeight);
	}
	
/*******************************************GETTERS AND SETTERS***********************************************/	
	public int[] getHealthBarImage() {
		return healthBarImage;
	}
	public void setHealthBarImage(int[] healthBarImage) {
		this.healthBarImage = healthBarImage;
	}
	public int getHealthBarWidth() {
		return healthBarWidth;
	}
	public void setHealthBarWidth(int healthBarWidth) {
		this.healthBarWidth = healthBarWidth;
	}
	public int getHealthBarHeight() {
		return healthBarHeight;
	}
	public void setHealthBarHeight(int healthBarHeight) {
		this.healthBarHeight = healthBarHeight;
	}
	public int[] getHealthImage() {
		return healthImage;
	}
	public void setHealthImage(int[] healthImage) {
		this.healthImage = healthImage;
	}
	public int getHealthWidth() {
		return healthWidth;
	}
	public void setHealthWidth(int healthWidth) {
		this.healthWidth = healthWidth;
	}
	public int getHealthHeight() {
		return healthHeight;
	}
	public void setHealthHeight(int healthHeight) {
		this.healthHeight = healthHeight;
	}
	
}
