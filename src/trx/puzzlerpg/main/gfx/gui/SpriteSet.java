package trx.puzzlerpg.main.gfx.gui;

import java.util.ArrayList;
import java.util.List;


public class SpriteSet {

    private int setId;
    private List<int[]> upSprites = new ArrayList<int[]>();
    private List<int[]> downSprites = new ArrayList<int[]>();
    private List<int[]> leftSprites = new ArrayList<int[]>();
    private List<int[]> rightSprites = new ArrayList<int[]>();
    private List<int[]> passiveSprites = new ArrayList<int[]>();

/*******************************************GETTERS AND SETTERS***********************************************/
    public int getSetId() {
        return setId;
    }
    public void setSetId(int setId) {
        this.setId = setId;
    }
    public List<int[]> getUpSprites() {
        return upSprites;
    }
    public void setUpSprites(List<int[]> upSprites) {
        this.upSprites = upSprites;
    }
    public List<int[]> getDownSprites() {
        return downSprites;
    }
    public void setDownSprites(List<int[]> downSprites) {
        this.downSprites = downSprites;
    }
    public List<int[]> getLeftSprites() {
        return leftSprites;
    }
    public void setLeftSprites(List<int[]> leftSprites) {
        this.leftSprites = leftSprites;
    }
    public List<int[]> getRightSprites() {
        return rightSprites;
    }
    public void setRightSprites(List<int[]> rightSprites) {
        this.rightSprites = rightSprites;
    }
    public List<int[]> getPassiveSprites() {
        return passiveSprites;
    }
    public void setPassiveSprites(List<int[]> passiveSprites) {
        this.passiveSprites = passiveSprites;
    }
}
