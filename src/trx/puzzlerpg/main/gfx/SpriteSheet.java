package trx.puzzlerpg.main.gfx;
 
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.imageio.ImageIO;

import trx.puzzlerpg.main.util.Vector2D;
 
public class SpriteSheet {
 
		private BufferedImage image;
        public int width;
        public int height;
        public int size;
        public int shiftSize;
        public int[] pixels;
 
        public SpriteSheet(String path, int size, int shiftSize) {
                try {
                        this.image = ImageIO.read(SpriteSheet.class.getResourceAsStream(path));
                } catch (IOException e) {
                        e.printStackTrace();
                }
                if (this.image == null) {
                        return;
                }
               
                this.width = this.image.getWidth();
                this.height = this.image.getHeight();
                this.size = size;
                this.shiftSize = shiftSize;
               
                pixels = this.image.getRGB(0, 0, width, height, null, 0, width);

        }
        
        public SpriteSheet(String path) {
            try {
                this.image = ImageIO.read(SpriteSheet.class.getResourceAsStream(path));
            } catch (IOException e) {
                    e.printStackTrace();
            }
            if (this.image == null) {
                    return;
            }
            
            this.width = this.image.getWidth();
            this.height = this.image.getHeight();
           
            pixels = image.getRGB(0, 0, width, height, null, 0, width);

    }        
        
        public BufferedImage crop(int x, int y) {
        	return image.getSubimage(x * this.size, y * this.size, this.size, this.size);
        }
        
        public BufferedImage crop(int x, int y, int size) {
        	return image.getSubimage(x * size, y * size, size, size);
        }
        
        public BufferedImage cropVector(Vector2D topLeft, Vector2D bottomRight, int spriteWidth, int spriteHeight) {
        	BufferedImage temp = image.getSubimage(topLeft.getIntX(), topLeft.getIntY(), spriteWidth, spriteHeight);
        	return temp;
        	
        }
}