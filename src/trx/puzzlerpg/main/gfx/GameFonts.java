package trx.puzzlerpg.main.gfx;

import java.awt.Font;
import java.awt.FontFormatException;
import java.io.IOException;
import java.io.InputStream;
import trx.puzzlerpg.main.gfx.gui.InfoBox;

public class GameFonts {
	public static Font infoFont;
	public static Font damageFont;
	
	
	static {
		InputStream is = InfoBox.class.getResourceAsStream("/Gui/synchronizer_nbp.ttf");
		try {
			Font font = Font.createFont(Font.TRUETYPE_FONT, is);
			infoFont = font.deriveFont(16f);
		} catch (FontFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		is = InfoBox.class.getResourceAsStream("/Gui/slkscrb.ttf");
		try {
			Font font = Font.createFont(Font.TRUETYPE_FONT, is);
			damageFont = font.deriveFont(12f);
		} catch (FontFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}	
	}
	
/*******************************************GETTERS AND SETTERS***********************************************/	
	public static Font getInfoFont() {
		return infoFont;
	}
	public static void setInfoFont(Font infoFont) {
		GameFonts.infoFont = infoFont;
	}
	public static Font getDamageFont() {
		return damageFont;
	}
	public static void setDamageFont(Font damageFont) {
		GameFonts.damageFont = damageFont;
	}
}
