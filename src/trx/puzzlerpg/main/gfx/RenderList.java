package trx.puzzlerpg.main.gfx;

public class RenderList {

	public int xOffset;
	public int yOffset;
	public int[] image;
	public byte mirrorImage;
	public int scale;
	public int spriteWidth;
	public int spriteHeight;
	
	public RenderList(int xOffset, int yOffset, int[] image, byte mirrorImage, int scale, int spriteWidth, int spriteHeight) {
		this.xOffset = xOffset;
		this.yOffset = yOffset;
		this.image = image;
		this.mirrorImage = mirrorImage;
		this.scale = scale;
		this.spriteWidth = spriteWidth;
		this.spriteHeight = spriteHeight;
	}
	
	public int getxOffset() {
		return xOffset;
	}
	public void setxOffset(int xOffset) {
		this.xOffset = xOffset;
	}
	public int getyOffset() {
		return yOffset;
	}
	public void setyOffset(int yOffset) {
		this.yOffset = yOffset;
	}
	public int[] getImage() {
		return image;
	}
	public void setImage(int[] image) {
		this.image = image;
	}
	public byte getMirrorImage() {
		return mirrorImage;
	}
	public void setMirrorImage(byte mirrorImage) {
		this.mirrorImage = mirrorImage;
	}
	public int getScale() {
		return scale;
	}
	public void setScale(int scale) {
		this.scale = scale;
	}
	
}
