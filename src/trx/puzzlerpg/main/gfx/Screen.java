package trx.puzzlerpg.main.gfx;

import java.awt.Color;

public class Screen {
	
	public static final byte BIT_MIRROR_X = 0x01;
	public static final byte BIT_MIRROR_Y = 0x02;
	public static final byte BIT_MIRROR_XY = 0x03;
	public static final int TRANSPARENT_PIXEL = new Color(0, 0, 0).getRGB();

	public int[] pixels;

	public int xOffset = 0;
	public int yOffset = 0;

	public int width;
	public int height;

	public Screen(int width, int height) {
		this.width = width;
		this.height = height;

		pixels = new int[width * height];
	}

	public void render(int xPos, int yPos, int[] sprite, int mirrorDir, int scale, int spriteWidth, int spriteHeight) {
		if (mirrorDir == BIT_MIRROR_X && !(mirrorDir == BIT_MIRROR_Y || mirrorDir == BIT_MIRROR_XY)) {
			renderMirrorX(xPos, yPos, sprite, scale, spriteWidth, spriteHeight);
		} else if (mirrorDir == BIT_MIRROR_Y && !(mirrorDir == BIT_MIRROR_X || mirrorDir == BIT_MIRROR_XY)) {
			renderMirrorY(xPos, yPos, sprite, scale, spriteWidth, spriteHeight);
		} else if (mirrorDir == BIT_MIRROR_XY) {
			renderMirrorXY(xPos, yPos, sprite, scale, spriteWidth, spriteHeight);
		} else {
			render(xPos, yPos, sprite, scale, spriteWidth, spriteHeight);
		}
	}
	
	public void render(int xPos, int yPos, int[] sprite, int xTileSubStart, int yTileSubStart, int xTileSubEnd, int yTileSubEnd, int mirrorDir, int scale, int spriteSize) {
		int scaleMap = scale - 1;
		xPos -= xOffset;
		yPos -= yOffset;
		boolean mirrorX = false;
		boolean mirrorY = false;
		
		if (mirrorDir == BIT_MIRROR_X || mirrorDir == BIT_MIRROR_XY) mirrorX = true;
		if (mirrorDir == BIT_MIRROR_Y || mirrorDir == BIT_MIRROR_XY) mirrorY = true;

		for (int y = yTileSubStart; y < yTileSubEnd; y++) {
			int ySheet = y;
			if (mirrorY) ySheet = spriteSize - 1 - y;

			int yPixel = (y + yPos + (y * scaleMap) - ((scaleMap * spriteSize) / 2));

			if (y + yPos < 0 || y + yPos >= height) continue;
			for (int x = xTileSubStart; x < xTileSubEnd; x++) {
				int xSheet = x;
				if (mirrorX) xSheet = spriteSize - 1 - x;
				int xPixel = (x + xPos + (x * scaleMap) - ((scaleMap * spriteSize) / 2));
				for (int yScale = 0; yScale < scale; yScale++) {
					if (yPixel + yScale < 0 || yPixel + yScale >= height) continue;
					for (int xScale = 0; xScale < scale; xScale++) {
						if (xPixel + xScale < 0 || xPixel + xScale >= width) continue;
						if (!(sprite[xSheet + ySheet * spriteSize] == TRANSPARENT_PIXEL || sprite[xSheet + ySheet * spriteSize] == 0)) {
							pixels[(xPixel + xScale) + (yPixel + yScale) * width] = sprite[xSheet + ySheet * spriteSize];
						}
					}
				}
			}
		}
	}
	
	public void render(int xPos, int yPos, int[] sprite, int xTileSubStart, int yTileSubStart, int xTileSubEnd, int yTileSubEnd, int mirrorDir, int scale, int spriteWidth, int spriteHeight) {
		int scaleMap = scale - 1;
		xPos -= xOffset;
		yPos -= yOffset;
		boolean mirrorX = false;
		boolean mirrorY = false;
		
		if (mirrorDir == BIT_MIRROR_X || mirrorDir == BIT_MIRROR_XY) mirrorX = true;
		if (mirrorDir == BIT_MIRROR_Y || mirrorDir == BIT_MIRROR_XY) mirrorY = true;

		for (int y = yTileSubStart; y < yTileSubEnd; y++) {
			int ySheet = y;
			if (mirrorY) ySheet = spriteHeight - 1 - y;

			int yPixel = (y + yPos + (y * scaleMap) - ((scaleMap * spriteHeight) / 2));

			if (y + yPos < 0 || y + yPos >= height) continue;
			for (int x = xTileSubStart; x < xTileSubEnd; x++) {
				int xSheet = x;
				if (mirrorX) xSheet = spriteWidth - 1 - x;
				int xPixel = (x + xPos + (x * scaleMap) - ((scaleMap * spriteWidth) / 2));
				for (int yScale = 0; yScale < scale; yScale++) {
					if (yPixel + yScale < 0 || yPixel + yScale >= height) continue;
					for (int xScale = 0; xScale < scale; xScale++) {
						if (xPixel + xScale < 0 || xPixel + xScale >= width) continue;
						if (!(sprite[xSheet + ySheet * spriteHeight] == TRANSPARENT_PIXEL || sprite[xSheet + ySheet * spriteHeight] == 0)) {
							pixels[(xPixel + xScale) + (yPixel + yScale) * width] = sprite[xSheet + ySheet * spriteHeight];
						}
					}
				}
			}
		}
	}	
	
	public void render(int xPos, int yPos, int[] sprite, int scale, int spriteWidth, int spriteHeight) {
		int scaleMap = scale - 1;
		xPos -= xOffset;
		yPos -= yOffset;

		for (int y = 0; y < spriteHeight; y++) {
			if (y + yPos + spriteHeight < 0 || y + yPos - spriteHeight >= height) continue;
			int yPixel = (y + yPos + (y * scaleMap) - ((scaleMap * spriteHeight) / 2));	
			for (int x = 0; x < spriteWidth; x++) {
				if (x + xPos + spriteWidth < 0 || x + xPos - spriteWidth >= width) continue;
				int xPixel = (x + xPos + (x * scaleMap) - ((scaleMap * spriteWidth) / 2));
				for (int yScale = 0; yScale < scale; yScale++) {
					if (yPixel + yScale < 0 || yPixel + yScale >= height) continue;
					for (int xScale = 0; xScale < scale; xScale++) {
						if (xPixel + xScale < 0 || xPixel + xScale >= width) continue;
						if (!(sprite[x + y * spriteWidth] == TRANSPARENT_PIXEL || sprite[x + y * spriteWidth] == 0)) {
							pixels[(xPixel + xScale) + (yPixel + yScale) * width] = sprite[x + y * spriteWidth];
						}
					}
				}
			}
		}	
	}
	
	public void renderMirrorX(int xPos, int yPos, int[] sprite, int scale, int spriteWidth, int spriteHeight) {
		int scaleMap = scale - 1;
		xPos -= xOffset;
		yPos -= yOffset;

		for (int y = 0; y < spriteHeight; y++) {
			if (y + yPos < 0 || y + yPos >= height) continue;
			int yPixel = (y + yPos + (y * scaleMap) - ((scaleMap * spriteHeight) / 2));	
			for (int x = 0; x < spriteWidth; x++) {	
				int xSheet = spriteWidth - 1 - x;
				if (xSheet + xPos < 0 || xSheet + xPos >= width) continue;
				int xPixel = (x + xPos + (x * scaleMap) - ((scaleMap * spriteWidth) / 2));
				for (int yScale = 0; yScale < scale; yScale++) {
					if (yPixel + yScale < 0 || yPixel + yScale >= height) continue;
					for (int xScale = 0; xScale < scale; xScale++) {
						if (xPixel + xScale < 0 || xPixel + xScale >= width) continue;
						if (!(sprite[xSheet + y * spriteHeight] == TRANSPARENT_PIXEL || sprite[xSheet + y * spriteHeight] == 0)) {
							pixels[(xPixel + xScale) + (yPixel + yScale) * width] = sprite[xSheet + y * spriteWidth];
						}
					}
				}
			}
		}	
	}
	
	public void renderMirrorY(int xPos, int yPos, int[] sprite, int scale, int spriteWidth, int spriteHeight) {
		int scaleMap = scale - 1;
		xPos -= xOffset;
		yPos -= yOffset;

		for (int y = 0; y < spriteHeight; y++) {
			int ySheet = spriteWidth - 1 - y;
			if (ySheet + yPos < 0 || ySheet + yPos >= height) continue;
			int yPixel = (y + yPos + (y * scaleMap) - ((scaleMap * spriteHeight) / 2));	
			for (int x = 0; x < spriteWidth; x++) {
				if (x + xPos < 0 || x + xPos >= width) continue;
				int xPixel = (x + xPos + (x * scaleMap) - ((scaleMap * spriteWidth) / 2));
				for (int yScale = 0; yScale < scale; yScale++) {
					if (yPixel + yScale < 0 || yPixel + yScale >= height) continue;
					for (int xScale = 0; xScale < scale; xScale++) {
						if (xPixel + xScale < 0 || xPixel + xScale >= width) continue;
						if (!(sprite[x + ySheet * spriteHeight] == TRANSPARENT_PIXEL || sprite[x + ySheet * spriteHeight] == 0)) {
							pixels[(xPixel + xScale) + (yPixel + yScale) * width] = sprite[x + ySheet * spriteWidth];
						}
					}
				}
			}
		}	
	}	
	
	public void renderMirrorXY(int xPos, int yPos, int[] sprite, int scale, int spriteWidth, int spriteHeight) {
		int scaleMap = scale - 1;
		xPos -= xOffset;
		yPos -= yOffset;

		for (int y = 0; y < spriteHeight; y++) {
			int ySheet = spriteWidth - 1 - y;
			if (ySheet + yPos < 0 || ySheet + yPos >= height) continue;
			int yPixel = (y + yPos + (y * scaleMap) - ((scaleMap * spriteHeight) / 2));	
			for (int x = 0; x < spriteWidth; x++) {
				int xSheet = spriteWidth - 1 - x;
				if (xSheet + xPos < 0 || xSheet + xPos >= width) continue;
				int xPixel = (x + xPos + (x * scaleMap) - ((scaleMap * spriteWidth) / 2));
				for (int yScale = 0; yScale < scale; yScale++) {
					if (yPixel + yScale < 0 || yPixel + yScale >= height) continue;
					for (int xScale = 0; xScale < scale; xScale++) {
						if (xPixel + xScale < 0 || xPixel + xScale >= width) continue;
						if (!(sprite[xSheet + ySheet * spriteHeight] == TRANSPARENT_PIXEL || sprite[xSheet + ySheet * spriteHeight] == 0)) {
							pixels[(xPixel + xScale) + (yPixel + yScale) * width] = sprite[xSheet + ySheet * spriteWidth];
						}
					}
				}
			}
		}	
	}	
	
	public void setOffset(int x, int y) {
		this.xOffset = x;
		this.yOffset = y;
	}
	
	public void screenWipe() {
		pixels = new int[width * height];
	}
}