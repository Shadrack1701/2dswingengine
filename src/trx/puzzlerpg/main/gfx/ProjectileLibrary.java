package trx.puzzlerpg.main.gfx;

import trx.puzzlerpg.main.gfx.gui.SpriteSet;

import java.util.ArrayList;
import java.util.List;

public class ProjectileLibrary {
    public static List<SpriteSet> projectileList = new ArrayList<>();

    public static SpriteSet getProjectileSet(int projectileSetId) {
        for (SpriteSet projectileSet : projectileList) {
            if (projectileSet.getSetId() == projectileSetId) return projectileSet;
        }
        return null;
    }

    public static List<SpriteSet> getProjectileList() {
        return ProjectileLibrary.projectileList;
    }
    public static void setProjectileList(List<SpriteSet> projectileList) {
        ProjectileLibrary.projectileList = projectileList;
    }
}
