package trx.puzzlerpg.main.filemanagement;

import java.util.ArrayList;
import java.util.List;

import trx.puzzlerpg.main.gfx.SpriteSheet;
import trx.puzzlerpg.main.gfx.gui.PauseMenu;
import trx.puzzlerpg.main.util.Vector2D;

public class LoadPauseMenu {

    private Vector2D titleLoc;
    private List<Vector2D> titleImage = new ArrayList<Vector2D>();
    private Vector2D returnLoc;
    private List<Vector2D> returnImage = new ArrayList<Vector2D>();
    private Vector2D saveGameLoc;
    private List<Vector2D> saveGameImage = new ArrayList<Vector2D>();
    private Vector2D loadGameLoc;
    private List<Vector2D> loadGameImage = new ArrayList<Vector2D>();
    private Vector2D settingsLoc;
    private List<Vector2D> settingsImage = new ArrayList<Vector2D>();
    private Vector2D quitTitleLoc;
    private List<Vector2D> quitTitleImage = new ArrayList<Vector2D>();
    private Vector2D quitDesktopLoc;
    private List<Vector2D> quitDesktopImage = new ArrayList<Vector2D>();
    private List<Vector2D> returnSelImage = new ArrayList<Vector2D>();
    private List<Vector2D> saveGameSelImage = new ArrayList<Vector2D>();
    private List<Vector2D> loadGameSelImage = new ArrayList<Vector2D>();
    private List<Vector2D> settingsSelImage = new ArrayList<Vector2D>();
    private List<Vector2D> quitTitleSelImage = new ArrayList<Vector2D>();
    private List<Vector2D> quitDesktopSelImage = new ArrayList<Vector2D>();
    private Vector2D cursorLoc;
    private List<Vector2D> cursorImage = new ArrayList<Vector2D>();
    private int selectedOption;
    private String sheetPath = "res/Menus/Paused.png";

    public PauseMenu loadPauseMenu() {
        PauseMenu pauseMenu = new PauseMenu();
        SpriteSheet sheet = new SpriteSheet(sheetPath);
        pauseMenu.setTitleLoc(this.titleLoc);
        pauseMenu.setReturnLoc(this.returnLoc);
        pauseMenu.setSaveGameLoc(this.saveGameLoc);
        pauseMenu.setLoadGameLoc(this.loadGameLoc);
        pauseMenu.setSettingsLoc(this.settingsLoc);
        pauseMenu.setQuitTitleLoc(this.quitTitleLoc);
        pauseMenu.setQuitDesktopLoc(this.quitDesktopLoc);
        pauseMenu.setCursorLoc(this.cursorLoc);
        pauseMenu.setSelectedOption(selectedOption);

        pauseMenu.setTitleWidth(titleImage.get(1).getIntX() - titleImage.get(0).getIntX());
        pauseMenu.setTitleHeight(titleImage.get(1).getIntY() - titleImage.get(0).getIntY());
        pauseMenu.setTitleImage(sheet.cropVector(titleImage.get(0), titleImage.get(1), pauseMenu.getTitleWidth(), pauseMenu.getTitleHeight()).getRGB(0, 0, pauseMenu.getTitleWidth(), pauseMenu.getTitleHeight(), null, 0, pauseMenu.getTitleWidth()));

        pauseMenu.setReturnWidth(returnImage.get(1).getIntX() - returnImage.get(0).getIntX());
        pauseMenu.setReturnHeight(returnImage.get(1).getIntY() - returnImage.get(0).getIntY());
        pauseMenu.setReturnImage(sheet.cropVector(returnImage.get(0), returnImage.get(1), pauseMenu.getReturnWidth(), pauseMenu.getReturnHeight()).getRGB(0, 0, returnImage.get(1).getIntX() - returnImage.get(0).getIntX(), returnImage.get(1).getIntY() - returnImage.get(0).getIntY(), null, 0, returnImage.get(1).getIntX() - returnImage.get(0).getIntX()));

        pauseMenu.setSaveGameWidth(saveGameImage.get(1).getIntX() - saveGameImage.get(0).getIntX());
        pauseMenu.setSaveGameHeight(saveGameImage.get(1).getIntY() - saveGameImage.get(0).getIntY());
        pauseMenu.setSaveGameImage(sheet.cropVector(saveGameImage.get(0), saveGameImage.get(1), pauseMenu.getSaveGameWidth(), pauseMenu.getSaveGameHeight()).getRGB(0, 0, saveGameImage.get(1).getIntX() - saveGameImage.get(0).getIntX(), saveGameImage.get(1).getIntY() - saveGameImage.get(0).getIntY(), null, 0, saveGameImage.get(1).getIntX() - saveGameImage.get(0).getIntX()));

        pauseMenu.setLoadGameWidth(loadGameImage.get(1).getIntX() - loadGameImage.get(0).getIntX());
        pauseMenu.setLoadGameHeight(loadGameImage.get(1).getIntY() - loadGameImage.get(0).getIntY());
        pauseMenu.setLoadGameImage(sheet.cropVector(loadGameImage.get(0), loadGameImage.get(1), pauseMenu.getLoadGameWidth(), pauseMenu.getLoadGameHeight()).getRGB(0, 0, loadGameImage.get(1).getIntX() - loadGameImage.get(0).getIntX(), loadGameImage.get(1).getIntY() - loadGameImage.get(0).getIntY(), null, 0, loadGameImage.get(1).getIntX() - loadGameImage.get(0).getIntX()));

        pauseMenu.setSettingsWidth(settingsImage.get(1).getIntX() - settingsImage.get(0).getIntX());
        pauseMenu.setSettingsHeight(settingsImage.get(1).getIntY() - settingsImage.get(0).getIntY());
        pauseMenu.setSettingsImage(sheet.cropVector(settingsImage.get(0), settingsImage.get(1), pauseMenu.getSettingsWidth(), pauseMenu.getSettingsHeight()).getRGB(0, 0, settingsImage.get(1).getIntX() - settingsImage.get(0).getIntX(), settingsImage.get(1).getIntY() - settingsImage.get(0).getIntY(), null, 0, settingsImage.get(1).getIntX() - settingsImage.get(0).getIntX()));

        pauseMenu.setQuitTitleWidth(quitTitleImage.get(1).getIntX() - quitTitleImage.get(0).getIntX());
        pauseMenu.setQuitTitleHeight(quitTitleImage.get(1).getIntY() - quitTitleImage.get(0).getIntY());
        pauseMenu.setQuitTitleImage(sheet.cropVector(quitTitleImage.get(0), quitTitleImage.get(1), pauseMenu.getQuitTitleWidth(), pauseMenu.getQuitTitleHeight()).getRGB(0, 0, quitTitleImage.get(1).getIntX() - quitTitleImage.get(0).getIntX(), quitTitleImage.get(1).getIntY() - quitTitleImage.get(0).getIntY(), null, 0, quitTitleImage.get(1).getIntX() - quitTitleImage.get(0).getIntX()));

        pauseMenu.setQuitDesktopWidth(quitDesktopImage.get(1).getIntX() - quitDesktopImage.get(0).getIntX());
        pauseMenu.setQuitDesktopHeight(quitDesktopImage.get(1).getIntY() - quitDesktopImage.get(0).getIntY());
        pauseMenu.setQuitDesktopImage(sheet.cropVector(quitDesktopImage.get(0), quitDesktopImage.get(1), pauseMenu.getQuitDesktopWidth(), pauseMenu.getQuitDesktopHeight()).getRGB(0, 0, quitDesktopImage.get(1).getIntX() - quitDesktopImage.get(0).getIntX(), quitDesktopImage.get(1).getIntY() - quitDesktopImage.get(0).getIntY(), null, 0, quitDesktopImage.get(1).getIntX() - quitDesktopImage.get(0).getIntX()));

        pauseMenu.setReturnSelWidth(returnSelImage.get(1).getIntX() - returnSelImage.get(0).getIntX());
        pauseMenu.setReturnSelHeight(returnSelImage.get(1).getIntY() - returnSelImage.get(0).getIntY());
        pauseMenu.setReturnSelImage(sheet.cropVector(returnSelImage.get(0), returnSelImage.get(1), pauseMenu.getReturnSelWidth(), pauseMenu.getReturnSelHeight()).getRGB(0, 0, returnSelImage.get(1).getIntX() - returnSelImage.get(0).getIntX(), returnSelImage.get(1).getIntY() - returnSelImage.get(0).getIntY(), null, 0, returnSelImage.get(1).getIntX() - returnSelImage.get(0).getIntX()));

        pauseMenu.setSaveGameSelWidth(saveGameSelImage.get(1).getIntX() - saveGameSelImage.get(0).getIntX());
        pauseMenu.setSaveGameSelHeight(saveGameSelImage.get(1).getIntY() - saveGameSelImage.get(0).getIntY());
        pauseMenu.setSaveGameSelImage(sheet.cropVector(saveGameSelImage.get(0), saveGameSelImage.get(1), pauseMenu.getSaveGameSelWidth(), pauseMenu.getSaveGameSelHeight()).getRGB(0, 0, saveGameSelImage.get(1).getIntX() - saveGameSelImage.get(0).getIntX(), saveGameSelImage.get(1).getIntY() - saveGameSelImage.get(0).getIntY(), null, 0, saveGameSelImage.get(1).getIntX() - saveGameSelImage.get(0).getIntX()));

        pauseMenu.setLoadGameSelWidth(loadGameSelImage.get(1).getIntX() - loadGameSelImage.get(0).getIntX());
        pauseMenu.setLoadGameSelHeight(loadGameSelImage.get(1).getIntY() - loadGameSelImage.get(0).getIntY());
        pauseMenu.setLoadGameSelImage(sheet.cropVector(loadGameSelImage.get(0), loadGameSelImage.get(1), pauseMenu.getLoadGameSelWidth(), pauseMenu.getTitleHeight()).getRGB(0, 0, loadGameSelImage.get(1).getIntX() - loadGameSelImage.get(0).getIntX(), loadGameSelImage.get(1).getIntY() - loadGameSelImage.get(0).getIntY(), null, 0, loadGameSelImage.get(1).getIntX() - loadGameSelImage.get(0).getIntX()));

        pauseMenu.setSettingsSelWidth(settingsSelImage.get(1).getIntX() - settingsSelImage.get(0).getIntX());
        pauseMenu.setSettingsSelHeight(settingsSelImage.get(1).getIntY() - settingsSelImage.get(0).getIntY());
        pauseMenu.setSettingsSelImage(sheet.cropVector(settingsSelImage.get(0), settingsSelImage.get(1), pauseMenu.getSettingsSelWidth(), pauseMenu.getSettingsSelHeight()).getRGB(0, 0, settingsSelImage.get(1).getIntX() - settingsSelImage.get(0).getIntX(), settingsSelImage.get(1).getIntY() - settingsSelImage.get(0).getIntY(), null, 0, settingsSelImage.get(1).getIntX() - settingsSelImage.get(0).getIntX()));

        pauseMenu.setQuitTitleSelWidth(quitTitleSelImage.get(1).getIntX() - quitTitleSelImage.get(0).getIntX());
        pauseMenu.setQuitTitleSelHeight(quitTitleSelImage.get(1).getIntY() - quitTitleSelImage.get(0).getIntY());
        pauseMenu.setQuitTitleSelImage(sheet.cropVector(quitTitleSelImage.get(0), quitTitleSelImage.get(1), pauseMenu.getQuitTitleSelWidth(), pauseMenu.getQuitTitleSelHeight()).getRGB(0, 0, quitTitleSelImage.get(1).getIntX() - quitTitleSelImage.get(0).getIntX(), quitTitleSelImage.get(1).getIntY() - quitTitleSelImage.get(0).getIntY(), null, 0, quitTitleSelImage.get(1).getIntX() - quitTitleSelImage.get(0).getIntX()));

        pauseMenu.setQuitDesktopSelWidth(quitDesktopSelImage.get(1).getIntX() - quitDesktopSelImage.get(0).getIntX());
        pauseMenu.setQuitDesktopSelHeight(quitDesktopSelImage.get(1).getIntY() - quitDesktopSelImage.get(0).getIntY());
        pauseMenu.setQuitDesktopSelImage(sheet.cropVector(quitDesktopSelImage.get(0), quitDesktopSelImage.get(1), pauseMenu.getQuitDesktopSelWidth(), pauseMenu.getQuitDesktopSelHeight()).getRGB(0, 0, quitDesktopSelImage.get(1).getIntX() - quitDesktopSelImage.get(0).getIntX(), quitDesktopSelImage.get(1).getIntY() - quitDesktopSelImage.get(0).getIntY(), null, 0, quitDesktopSelImage.get(1).getIntX() - quitDesktopSelImage.get(0).getIntX()));

        pauseMenu.setCursorWidth(cursorImage.get(1).getIntX() - cursorImage.get(0).getIntX());
        pauseMenu.setCursorHeight(cursorImage.get(1).getIntY() - cursorImage.get(0).getIntY());
        pauseMenu.setCursorImage(sheet.cropVector(cursorImage.get(0), cursorImage.get(1), pauseMenu.getCursorWidth(), pauseMenu.getCursorHeight()).getRGB(0, 0, cursorImage.get(1).getIntX() - cursorImage.get(0).getIntX(), cursorImage.get(1).getIntY() - cursorImage.get(0).getIntY(), null, 0, cursorImage.get(1).getIntX() - cursorImage.get(0).getIntX()));

        return pauseMenu;
    }
}
