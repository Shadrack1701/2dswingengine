package trx.puzzlerpg.main.filemanagement;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import trx.puzzlerpg.main.entities.item.Item;
import trx.puzzlerpg.main.entities.item.ItemProperties;
import trx.puzzlerpg.main.entities.item.ItemType;
import trx.puzzlerpg.main.entities.item.subtypes.Weapon;
import trx.puzzlerpg.main.gfx.SpriteSheet;
import trx.puzzlerpg.main.gfx.gui.SpriteSet;
import trx.puzzlerpg.main.util.Vector2D;

public class LoadItem {
	
	private String name;
    private ItemType type;
	private boolean pluralName;
	private int entityScale;
	private int spriteSize;
	private Vector2D icon = new Vector2D();
	private boolean iconSupplied;
	private List<ItemProperties> itemProperties = new ArrayList<>();
	private int value;
	private String infoText;

    private int projectileSetId;
    private int damage;
    private int attackSpeed;
    private boolean melee;
    private SpriteSet weaponSprite;
    private List<Vector2D> projectileCollision = new ArrayList<>();
    private int projectileLife;
    private int projectileSpeed;
    private int projectileAnimationSpeed;
	
	public Item getItem(SpriteSheet sheet) {
		Item item = new Item();
		item.setName(this.name);
		item.setPluralName(this.pluralName);
		item.setEntityScale(this.entityScale);
		item.setSpriteSize(this.spriteSize);
		item.setItemProperties(EnumSet.copyOf(this.itemProperties));
		item.setValue(this.value);
		item.setInfoText(this.infoText);
		
		if (iconSupplied) {
			item.setIcon(sheet.crop(icon.getIntX(), icon.getIntY(), spriteSize).getRGB(0, 0, spriteSize, spriteSize, null, 0, spriteSize));
		} else {
			item.setIcon(null);
		}
		
		return item;
	}

    public Weapon getWeapon(SpriteSheet sheet) {
        Weapon weapon = new Weapon();
        weapon.setName(this.name);
        weapon.setPluralName(this.pluralName);
        weapon.setEntityScale(this.entityScale);
        weapon.setSpriteSize(this.spriteSize);
        weapon.setItemProperties(EnumSet.copyOf(this.itemProperties));
        weapon.setValue(this.value);
        weapon.setInfoText(this.infoText);
        if (iconSupplied) {
            weapon.setIcon(sheet.crop(icon.getIntX(), icon.getIntY(), spriteSize).getRGB(0, 0, spriteSize, spriteSize, null, 0, spriteSize));
        } else {
            weapon.setIcon(null);
        }

        weapon.setProjectileSetId(this.projectileSetId);
        weapon.setDamage(this.damage);
        weapon.setAttackSpeed(this.attackSpeed);
        weapon.setMelee(this.melee);
        weapon.setWeaponSprite(this.weaponSprite);
        weapon.setProjectileCollision(this.projectileCollision);
        weapon.setProjectileLife(this.projectileLife);
        weapon.setProjectileSpeed(this.projectileSpeed);
        weapon.setProjectileAnimationSpeed(this.projectileAnimationSpeed);

        return weapon;
    }

    public String getName() {
        return name;
    }
    public ItemType getType() {
        return type;
    }
    public void setType(ItemType type) {
        this.type = type;
    }
    public void setName(String name) {
        this.name = name;
    }
    public boolean isPluralName() {
        return pluralName;
    }
    public void setPluralName(boolean pluralName) {
        this.pluralName = pluralName;
    }
    public int getEntityScale() {
        return entityScale;
    }
    public void setEntityScale(int entityScale) {
        this.entityScale = entityScale;
    }
    public int getSpriteSize() {
        return spriteSize;
    }
    public void setSpriteSize(int spriteSize) {
        this.spriteSize = spriteSize;
    }
    public Vector2D getIcon() {
        return icon;
    }
    public void setIcon(Vector2D icon) {
        this.icon = icon;
    }
    public boolean isIconSupplied() {
        return iconSupplied;
    }
    public void setIconSupplied(boolean iconSupplied) {
        this.iconSupplied = iconSupplied;
    }
    public List<ItemProperties> getItemTypes() {
        return itemProperties;
    }
    public void setItemTypes(List<ItemProperties> itemPropertieses) {
        this.itemProperties = itemPropertieses;
    }
    public int getValue() {
        return value;
    }
    public void setValue(int value) {
        this.value = value;
    }
    public String getInfoText() {
        return infoText;
    }
    public void setInfoText(String infoText) {
        this.infoText = infoText;
    }
    public List<ItemProperties> getItemProperties() {
        return itemProperties;
    }
    public void setItemProperties(List<ItemProperties> itemProperties) {
        this.itemProperties = itemProperties;
    }
    public int getProjectileSetId() {
        return projectileSetId;
    }
    public void setProjectileSetId(int projectileSetId) {
        this.projectileSetId = projectileSetId;
    }
    public int getDamage() {
        return damage;
    }
    public void setDamage(int damage) {
        this.damage = damage;
    }
    public int getAttackSpeed() {
        return attackSpeed;
    }
    public void setAttackSpeed(int attackSpeed) {
        this.attackSpeed = attackSpeed;
    }
    public boolean isMelee() {
        return melee;
    }
    public void setMelee(boolean melee) {
        this.melee = melee;
    }
    public SpriteSet getWeaponSprite() {
        return weaponSprite;
    }
    public void setWeaponSprite(SpriteSet weaponSprite) {
        this.weaponSprite = weaponSprite;
    }
    public List<Vector2D> getProjectileCollision() {
        return projectileCollision;
    }
    public void setProjectileCollision(List<Vector2D> projectileCollision) {
        this.projectileCollision = projectileCollision;
    }
    public int getProjectileLife() {
        return projectileLife;
    }
    public void setProjectileLife(int projectileLife) {
        this.projectileLife = projectileLife;
    }
    public int getProjectileSpeed() {
        return projectileSpeed;
    }
    public void setProjectileSpeed(int projectileSpeed) {
        this.projectileSpeed = projectileSpeed;
    }
    public int getProjectileAnimationSpeed() {
        return projectileAnimationSpeed;
    }
    public void setProjectileAnimationSpeed(int projectileAnimationSpeed) {
        this.projectileAnimationSpeed = projectileAnimationSpeed;
    }
}
