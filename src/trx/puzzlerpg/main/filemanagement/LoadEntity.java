package trx.puzzlerpg.main.filemanagement;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import trx.puzzlerpg.main.entities.AiProfiles;
import trx.puzzlerpg.main.entities.Entity;
import trx.puzzlerpg.main.entities.EntityType;
import trx.puzzlerpg.main.entities.item.Equipment;
import trx.puzzlerpg.main.entities.item.Item;
import trx.puzzlerpg.main.entities.item.ItemType;
import trx.puzzlerpg.main.entities.subtypes.EntityLootable;
import trx.puzzlerpg.main.entities.subtypes.HostileMob;
import trx.puzzlerpg.main.entities.subtypes.NPC;
import trx.puzzlerpg.main.entities.subtypes.Player;
import trx.puzzlerpg.main.gfx.SpriteSheet;
import trx.puzzlerpg.main.gfx.gui.Conversation;
import trx.puzzlerpg.main.gfx.gui.SpriteSet;
import trx.puzzlerpg.main.util.Vector2D;

public class LoadEntity {

	private String name;
	private int speed;
	private int movingDir = 1;
	private int entityScale;
	private int animationSpeed;
	private List<Vector2D> startingLocations = new ArrayList<>();
	private int spriteSize;
	private List<Vector2D> collisionCoords = new ArrayList<>();
	private List<Vector2D> movingUpSprites = new ArrayList<>();
	private List<Vector2D> movingDownSprites = new ArrayList<>();
	private List<Vector2D> movingLeftSprites = new ArrayList<>();
	private List<Vector2D> movingRightSprites = new ArrayList<>();
	private List<Vector2D> passiveSprites = new ArrayList<>();
	private Vector2D unLootedSprite = new Vector2D();
	private Vector2D lootedSprite = new Vector2D();
	private EntityType entityType;
	private List<AiProfiles> aiProfiles = new ArrayList<>();
	private boolean interactable;
	private List<LoadItem> loadItems = new ArrayList<>();
	private List<Conversation> conversations = new ArrayList<>();
	private int maxHealth, currHealth;
    private Equipment equipment = new Equipment();

	public List<Entity> getEntities(SpriteSheet sheet) {
		List<Entity> entities = new ArrayList<>();
		
		for (Vector2D location: startingLocations) {
			if (entityType.equals(EntityType.Hostile)) {
				entities.add(getHostileMob(location, sheet));	
			} else if (entityType.equals(EntityType.Player)) {
				entities.add(getPlayer(location, sheet));
			} else if (entityType.equals(EntityType.NPC)) {
				entities.add(getNPC(location, sheet));
			} else if (entityType.equals(EntityType.EntityLootable)) {
				entities.add(getEntityLootable(location, sheet));
			} else if (entityType.equals(EntityType.EntityDestructable)) {
				//TODO: entities.add(getEntityDestructable(location));
			}	
		}
		return entities;
	}

	private HostileMob getHostileMob(Vector2D location, SpriteSheet sheet) {
		HostileMob hostile = new HostileMob();
		hostile.setName(this.name);
		hostile.setSpeed(this.speed);
		hostile.setAnimationSpeed(this.animationSpeed);
		hostile.vector = location;
		hostile.setMovingDir(this.movingDir);
		hostile.setEntityScale(this.entityScale);
		hostile.setCollisionCoords(this.collisionCoords);
		hostile.setCollision(new Rectangle(this.collisionCoords.get(0).getIntX(), this.collisionCoords.get(0).getIntY(), this.collisionCoords.get(1).getIntX() - this.collisionCoords.get(0).getIntX(), this.collisionCoords.get(2).getIntY() - this.collisionCoords.get(0).getIntY()));
		hostile.setSpriteSize(this.spriteSize);
		hostile.setMaxHealth(maxHealth);
		hostile.setCurrHealth(currHealth);
		hostile.setInvincibleDuration(500);

		SpriteSet spriteSet = new SpriteSet();
		spriteSet.setUpSprites(setImage(sheet, movingUpSprites, spriteSize));
		spriteSet.setDownSprites(setImage(sheet, movingDownSprites, spriteSize));
		spriteSet.setLeftSprites(setImage(sheet, movingLeftSprites, spriteSize));
		spriteSet.setRightSprites(setImage(sheet, movingRightSprites, spriteSize));
		spriteSet.setPassiveSprites(setImage(sheet, passiveSprites, spriteSize));
		hostile.setSpriteSet(spriteSet);
		hostile.setProfiles((EnumSet.copyOf(aiProfiles)));	

		return hostile;
	}
	
	private Player getPlayer(Vector2D location, SpriteSheet sheet) {
		Player player = new Player();
		player.setName(this.name);
		player.setSpeed(this.speed);
		player.setAnimationSpeed(this.animationSpeed);
		player.vector = location;
		player.setMovingDir(this.movingDir);
		player.setEntityScale(this.entityScale);
		player.setCollisionCoords(this.collisionCoords);
		player.setCollision(new Rectangle(this.collisionCoords.get(0).getIntX(), this.collisionCoords.get(0).getIntY(), this.collisionCoords.get(1).getIntX() - this.collisionCoords.get(0).getIntX(), this.collisionCoords.get(2).getIntY() - this.collisionCoords.get(0).getIntY()));
		player.setSpriteSize(this.spriteSize);
		player.setInteractable(this.interactable);
		player.setMaxHealth(maxHealth);
		player.setCurrHealth(currHealth);
		player.setInvincibleDuration(500);
        player.setEquipment(equipment);

		SpriteSet spriteSet = new SpriteSet();
		spriteSet.setUpSprites(setImage(sheet, movingUpSprites, spriteSize));
		spriteSet.setDownSprites(setImage(sheet, movingDownSprites, spriteSize));
		spriteSet.setLeftSprites(setImage(sheet, movingLeftSprites, spriteSize));
		spriteSet.setRightSprites(setImage(sheet, movingRightSprites, spriteSize));
		spriteSet.setPassiveSprites(setImage(sheet, passiveSprites, spriteSize));
		player.setSpriteSet(spriteSet);

		return player;
	}
	
	private NPC getNPC(Vector2D location, SpriteSheet sheet) {
		NPC npc = new NPC();
		npc.setName(this.name);
		npc.setSpeed(this.speed);
		npc.setAnimationSpeed(this.animationSpeed);
		npc.vector = location;
		npc.setMovingDir(this.movingDir);
		npc.setEntityScale(this.entityScale);
		npc.setCollisionCoords(this.collisionCoords);
		npc.setCollision(new Rectangle(this.collisionCoords.get(0).getIntX(), this.collisionCoords.get(0).getIntY(), this.collisionCoords.get(1).getIntX() - this.collisionCoords.get(0).getIntX(), this.collisionCoords.get(2).getIntY() - this.collisionCoords.get(0).getIntY()));
		npc.setSpriteSize(this.spriteSize);
		npc.setConversations(conversations);
		npc.setProfiles((EnumSet.copyOf(aiProfiles)));	
		npc.setInteractable(this.interactable);

		SpriteSet spriteSet = new SpriteSet();
		spriteSet.setUpSprites(setImage(sheet, movingUpSprites, spriteSize));
		spriteSet.setDownSprites(setImage(sheet, movingDownSprites, spriteSize));
		spriteSet.setLeftSprites(setImage(sheet, movingLeftSprites, spriteSize));
		spriteSet.setRightSprites(setImage(sheet, movingRightSprites, spriteSize));
		spriteSet.setPassiveSprites(setImage(sheet, passiveSprites, spriteSize));
		npc.setSpriteSet(spriteSet);
		
		return npc;
	}
	
	private Entity getEntityLootable(Vector2D location, SpriteSheet sheet) {
		EntityLootable lootable = new EntityLootable();
		lootable.setName(this.name);
		lootable.vector = location;
		lootable.setFacingDir(this.movingDir);
		lootable.setEntityScale(this.entityScale);
		lootable.setCollisionCoords(this.collisionCoords);
		lootable.setCollision(new Rectangle(this.collisionCoords.get(0).getIntX(), this.collisionCoords.get(0).getIntY(), this.collisionCoords.get(1).getIntX() - this.collisionCoords.get(0).getIntX(), this.collisionCoords.get(2).getIntY() - this.collisionCoords.get(0).getIntY()));
		lootable.setSpriteSize(this.spriteSize);
		lootable.setLooted(false);
		lootable.setInteractable(this.interactable);
		
		lootable.setUnLootedSprite(setImage(sheet, unLootedSprite, spriteSize));
		lootable.setLootedSprite(setImage(sheet, lootedSprite, spriteSize));
		List<Item> items = new ArrayList<>();
		for (LoadItem lootItem : loadItems) {
			if (lootItem.getType() == null) {
                items.add(lootItem.getItem(sheet));
            } else if (lootItem.getType().equals(ItemType.Weapon)) {
                items.add(lootItem.getWeapon(sheet));
            }
		}
		lootable.setItems(items);
		
		return lootable;
	}
	
	private List<int[]> setImage(SpriteSheet sheet, List<Vector2D> spriteLocations, int spriteSize) {
		List<int[]> imageInt = new ArrayList<int[]>();
		int[] imagePixels = new int[sheet.size * sheet.size];
		for (Vector2D spriteLoc : spriteLocations) {
			imagePixels = sheet.crop(spriteLoc.getIntX(), spriteLoc.getIntY(), spriteSize).getRGB(0, 0, spriteSize, spriteSize, null,
					0, spriteSize);
			imageInt.add(imagePixels);
		}
		
		return imageInt;
	}	
	
	private int[] setImage(SpriteSheet sheet, Vector2D spriteLocation, int spriteSize) {
		return sheet.crop(spriteLocation.getIntX(), spriteLocation.getIntY(), spriteSize).getRGB(0, 0, spriteSize, spriteSize, null, 0, spriteSize);		
	}	
	
}
