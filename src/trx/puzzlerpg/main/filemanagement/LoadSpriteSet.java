package trx.puzzlerpg.main.filemanagement;

import trx.puzzlerpg.main.gfx.SpriteSheet;
import trx.puzzlerpg.main.gfx.gui.SpriteSet;
import trx.puzzlerpg.main.util.Vector2D;
import java.util.ArrayList;
import java.util.List;

public class LoadSpriteSet {

    private int spriteSetID;
    private List<Vector2D> upSprites = new ArrayList<>();
    private List<Vector2D> downSprites = new ArrayList<>();
    private List<Vector2D> leftSprites = new ArrayList<>();
    private List<Vector2D> rightSprites = new ArrayList<>();
    private List<Vector2D> passiveSprites = new ArrayList<>();

    public SpriteSet loadSpriteSet(SpriteSheet sheet) {
        SpriteSet spriteSet = new SpriteSet();
        spriteSet.setUpSprites(setImage(sheet, upSprites, sheet.size));
        spriteSet.setDownSprites(setImage(sheet, downSprites, sheet.size));
        spriteSet.setLeftSprites(setImage(sheet, leftSprites, sheet.size));
        spriteSet.setRightSprites(setImage(sheet, rightSprites, sheet.size));
        spriteSet.setPassiveSprites(setImage(sheet, passiveSprites, sheet.size));

        return spriteSet;
    }

    private List<int[]> setImage(SpriteSheet sheet, List<Vector2D> spriteLocations, int spriteSize) {
        List<int[]> imageInt = new ArrayList<int[]>();
        int[] imagePixels = new int[sheet.size * sheet.size];
        for (Vector2D spriteLoc : spriteLocations) {
            imagePixels = sheet.crop(spriteLoc.getIntX(), spriteLoc.getIntY(), spriteSize).getRGB(0, 0, spriteSize, spriteSize, null,
                    0, spriteSize);
            imageInt.add(imagePixels);
        }

        return imageInt;
    }
}
