package trx.puzzlerpg.main.filemanagement;

import java.net.URI;

import trx.puzzlerpg.main.gfx.ProjectileLibrary;
import trx.puzzlerpg.main.gfx.SpriteSheet;
import trx.puzzlerpg.main.gfx.gui.*;

public class GameInfo {

	private String name;
    private String displayName;
    private URI saveLocation;
    private URI newGameLocation;
	private int width;
    private int height; 
    private int scale;
    private int tickRate;
    private int renderRate;
    private LoadMainMenu loadMainMenu;
    private LoadPauseMenu loadPauseMenu;
    private LoadInfoBox loadInfoBox;
    private LoadHud loadHud;
    private LoadProjectiles loadProjectiles;
    
    public MainMenu getMainMenu() {
    	return loadMainMenu.loadMenu();
    }
    
    public PauseMenu getPauseMenu() {
    	return loadPauseMenu.loadPauseMenu();
    }
    
    public InfoBox getInfoBox() {
    	return loadInfoBox.loadInfoBox();
    }
    
    public Hud getHud() {
    	return loadHud.loadHud();
    }

	public void getProjectileLibrary() { loadProjectiles.loadProjectiles(); }
	
/*******************************************GETTERS AND SETTERS***********************************************/    
    public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public URI getSaveLocation() {
		return saveLocation;
	}
	public void setSaveLocation(URI saveLocation) {
		this.saveLocation = saveLocation;
	}
	public URI getNewGameLocation() {
		return newGameLocation;
	}
	public void setNewGameLocation(URI newGameLocation) {
		this.newGameLocation = newGameLocation;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public  int getHeight() {
		return height;
	}
	public  void setHeight(int height) {
		this.height = height;
	}
	public  int getScale() {
		return scale;
	}
	public  void setScale(int scale) {
		this.scale = scale;
	}
	public  double getTickRate() {
		return tickRate;
	}
	public  void setTickRate(int tickRate) {
		this.tickRate = tickRate;
	}
    public int getRenderRate() {
        return renderRate;
    }
    public void setRenderRate(int renderRate) {
        this.renderRate = renderRate;
    }
    public LoadMainMenu getLoadMenu() {
		return loadMainMenu;
	}
	public void setLoadMenu(LoadMainMenu loadMenu) {
		this.loadMainMenu = loadMenu;
	}
	public LoadHud getLoadHud() {
		return loadHud;
	}
	public void setLoadHud(LoadHud loadHud) {
		this.loadHud = loadHud;
	}
}
