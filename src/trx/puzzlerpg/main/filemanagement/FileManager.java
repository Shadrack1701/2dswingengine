package trx.puzzlerpg.main.filemanagement;

import java.io.FileNotFoundException;
import java.io.FileReader;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

public class FileManager {

	public static GameInfo getGameInfo() {
		Gson gson = new Gson();
		GameInfo gameInfo = null;
		try {
			gameInfo = gson.fromJson(new FileReader(System.getProperty("user.dir") + "//res//Json//GameInfo.json"), GameInfo.class);
		} catch (JsonSyntaxException | JsonIOException | FileNotFoundException e) {
			e.printStackTrace();
		}
		gson = null;
		return gameInfo;
	}
	
	public static LoadLevel getLevel(String levelName) {
		Gson gson = new Gson();
		LoadLevel level = null;
		try {
			level = gson.fromJson(new FileReader(System.getProperty("user.dir") + "//res//Json//" + levelName), LoadLevel.class);
		} catch (JsonSyntaxException | JsonIOException | FileNotFoundException e) {
			e.printStackTrace();
		}
		gson = null;
		return level;
	}
	
	
}
