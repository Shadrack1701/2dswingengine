package trx.puzzlerpg.main.filemanagement;

import java.util.ArrayList;
import java.util.List;
import trx.puzzlerpg.main.gfx.SpriteSheet;
import trx.puzzlerpg.main.gfx.gui.InfoBox;
import trx.puzzlerpg.main.util.Vector2D;

public class LoadInfoBox {

	private List<Vector2D> topLeft = new ArrayList<Vector2D>();
	private List<Vector2D> topMiddle = new ArrayList<Vector2D>();
	private List<Vector2D> topRight = new ArrayList<Vector2D>();
	private List<Vector2D> middleLeft = new ArrayList<Vector2D>();
	private List<Vector2D> middleMiddle = new ArrayList<Vector2D>();
	private List<Vector2D> middleRight = new ArrayList<Vector2D>();
	private List<Vector2D> bottomLeft = new ArrayList<Vector2D>();
	private List<Vector2D> bottomMiddle = new ArrayList<Vector2D>();
	private List<Vector2D> bottomRight = new ArrayList<Vector2D>();
	private int infoWidth = 32;
	private int infoHeight = 16;
	private String sheetPath;
	
	public InfoBox loadInfoBox() {
		InfoBox infoBox = new InfoBox();
		SpriteSheet sheet = new SpriteSheet(sheetPath);
		infoBox.setInfoWidth(this.infoWidth);
		infoBox.setInfoHeight(this.infoHeight);
		
		infoBox.setTopLeft(sheet.cropVector(topLeft.get(0), topLeft.get(1), infoBox.getInfoWidth(), infoBox.getInfoHeight()).getRGB(0, 0,  infoBox.getInfoWidth(),  infoBox.getInfoHeight(), null, 0, infoBox.getInfoWidth()));
		infoBox.setTopMiddle(sheet.cropVector(topMiddle.get(0), topMiddle.get(1), infoBox.getInfoWidth(), infoBox.getInfoHeight()).getRGB(0, 0,  infoBox.getInfoWidth(),  infoBox.getInfoHeight(), null, 0, infoBox.getInfoWidth()));
		infoBox.setTopRight(sheet.cropVector(topRight.get(0), topRight.get(1), infoBox.getInfoWidth(), infoBox.getInfoHeight()).getRGB(0, 0,  infoBox.getInfoWidth(),  infoBox.getInfoHeight(), null, 0, infoBox.getInfoWidth()));
		infoBox.setMiddleLeft(sheet.cropVector(middleLeft.get(0), middleLeft.get(1), infoBox.getInfoWidth(), infoBox.getInfoHeight()).getRGB(0, 0,  infoBox.getInfoWidth(),  infoBox.getInfoHeight(), null, 0, infoBox.getInfoWidth()));
		infoBox.setMiddleMiddle(sheet.cropVector(middleMiddle.get(0), middleMiddle.get(1), infoBox.getInfoWidth(), infoBox.getInfoHeight()).getRGB(0, 0,  infoBox.getInfoWidth(),  infoBox.getInfoHeight(), null, 0, infoBox.getInfoWidth()));
		infoBox.setMiddleRight(sheet.cropVector(middleRight.get(0), middleRight.get(1), infoBox.getInfoWidth(), infoBox.getInfoHeight()).getRGB(0, 0,  infoBox.getInfoWidth(),  infoBox.getInfoHeight(), null, 0, infoBox.getInfoWidth()));
		infoBox.setBottomLeft(sheet.cropVector(bottomLeft.get(0), bottomLeft.get(1), infoBox.getInfoWidth(), infoBox.getInfoHeight()).getRGB(0, 0,  infoBox.getInfoWidth(),  infoBox.getInfoHeight(), null, 0, infoBox.getInfoWidth()));
		infoBox.setBottomMiddle(sheet.cropVector(bottomMiddle.get(0), bottomMiddle.get(1), infoBox.getInfoWidth(), infoBox.getInfoHeight()).getRGB(0, 0,  infoBox.getInfoWidth(),  infoBox.getInfoHeight(), null, 0, infoBox.getInfoWidth()));
		infoBox.setBottomRight(sheet.cropVector(bottomRight.get(0), bottomRight.get(1), infoBox.getInfoWidth(), infoBox.getInfoHeight()).getRGB(0, 0,  infoBox.getInfoWidth(),  infoBox.getInfoHeight(), null, 0, infoBox.getInfoWidth()));
		
		return infoBox;
	}
	
}
