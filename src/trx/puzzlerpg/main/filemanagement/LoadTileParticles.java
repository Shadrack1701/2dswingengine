package trx.puzzlerpg.main.filemanagement;

import java.util.ArrayList;
import java.util.List;

import trx.puzzlerpg.main.gfx.SpriteSheet;
import trx.puzzlerpg.main.level.tiles.TileParticles;
import trx.puzzlerpg.main.util.Vector2D;

public class LoadTileParticles {
	
	private List<Vector2D> enterImages = new ArrayList<Vector2D>();
	private Vector2D enterOffset;
	private int enterRate;
	private boolean enterPreEntity;
	
	private List<Vector2D> movingUpImages = new ArrayList<Vector2D>();
	private Vector2D movingUpOffset;
	private List<Vector2D> movingDownImages = new ArrayList<Vector2D>();
	private Vector2D movingDownOffset;
	private List<Vector2D> movingLeftImages = new ArrayList<Vector2D>();
	private Vector2D movingLeftOffset;
	private List<Vector2D> movingRightImages = new ArrayList<Vector2D>();
	private Vector2D movingRightOffset;
	private int movingRate;
	private boolean movingPreEntity;
	
	private List<Vector2D> exitImages = new ArrayList<Vector2D>();
	private Vector2D exitOffset;
	private int exitRate;
	private boolean exitPreEntity;	
	
	private List<Vector2D> passiveImages = new ArrayList<Vector2D>();
	private Vector2D passiveOffset;
	private int passiveRate;
	private boolean passivePreEntity;

	public TileParticles getTileParticles(SpriteSheet sheet) {
		TileParticles tileParticles = new TileParticles();
		
		tileParticles.setEnterRate(this.enterRate);
		tileParticles.setEnterPreEntity(this.enterPreEntity);
		tileParticles.setEnterOffset(this.enterOffset);
		
		tileParticles.setMovingRate(this.movingRate);
		tileParticles.setMovingPreEntity(this.movingPreEntity);
		tileParticles.setMovingUpOffset(this.movingUpOffset);
		tileParticles.setMovingDownOffset(this.movingDownOffset);
		tileParticles.setMovingLeftOffset(this.movingLeftOffset);
		tileParticles.setMovingRightOffset(this.movingRightOffset);
		
		tileParticles.setExitRate(this.exitRate);
		tileParticles.setExitPreEntity(this.exitPreEntity);
		tileParticles.setExitOffset(this.exitOffset);
		
		tileParticles.setPassiveRate(this.passiveRate);
		tileParticles.setPassivePreEntity(this.passivePreEntity);
		tileParticles.setPassiveOffset(this.passiveOffset);
		
		if (!(enterImages == null)) {
			List<int[]> loadEnterImages = new ArrayList<int[]>();
			int[] enterPixels = new int[sheet.size * sheet.size];
			for (Vector2D tileLoc : enterImages) {
				enterPixels = sheet.crop(tileLoc.getIntX(), tileLoc.getIntY()).getRGB(0, 0, sheet.size, sheet.size, null, 0, sheet.size);
				loadEnterImages.add(enterPixels);
			}
			tileParticles.setEnterImages(loadEnterImages);
		} else {
			tileParticles.setEnterImages(null);
		}
		
		if (!(movingUpImages == null)) {
			List<int[]> loadMovingUpImages = new ArrayList<int[]>();
			int[] movingUpPixels = new int[sheet.size * sheet.size];
			for (Vector2D tileLoc : movingUpImages) {
				movingUpPixels = sheet.crop(tileLoc.getIntX(), tileLoc.getIntY()).getRGB(0, 0, sheet.size, sheet.size, null, 0, sheet.size);
				loadMovingUpImages.add(movingUpPixels);
			}
			tileParticles.setMovingUpImages(loadMovingUpImages);
		} else {
			tileParticles.setMovingUpImages(null);
		}
		
		if (!(movingDownImages == null)) {
			List<int[]> loadMovingDownImages = new ArrayList<int[]>();
			int[] movingDownPixels = new int[sheet.size * sheet.size];
			for (Vector2D tileLoc : movingDownImages) {
				movingDownPixels = sheet.crop(tileLoc.getIntX(), tileLoc.getIntY()).getRGB(0, 0, sheet.size, sheet.size, null, 0, sheet.size);
				loadMovingDownImages.add(movingDownPixels);
			}
			tileParticles.setMovingDownImages(loadMovingDownImages);
		} else {
			tileParticles.setMovingDownImages(null);
		}
	
		if (!(movingLeftImages == null)) {
			List<int[]> loadMovingLeftImages = new ArrayList<int[]>();
			int[] movingLeftPixels = new int[sheet.size * sheet.size];
			for (Vector2D tileLoc : movingLeftImages) {
				movingLeftPixels = sheet.crop(tileLoc.getIntX(), tileLoc.getIntY()).getRGB(0, 0, sheet.size, sheet.size, null, 0, sheet.size);
				loadMovingLeftImages.add(movingLeftPixels);
			}
			tileParticles.setMovingLeftImages(loadMovingLeftImages);
		} else {
			tileParticles.setMovingLeftImages(null);
		}
		
		if (!(movingRightImages == null)) {
			List<int[]> loadMovingRightImages = new ArrayList<int[]>();
			int[] movingRightPixels = new int[sheet.size * sheet.size];
			for (Vector2D tileLoc : movingRightImages) {
				movingRightPixels = sheet.crop(tileLoc.getIntX(), tileLoc.getIntY()).getRGB(0, 0, sheet.size, sheet.size, null, 0, sheet.size);
				loadMovingRightImages.add(movingRightPixels);
			}
			tileParticles.setMovingRightImages(loadMovingRightImages);
		} else {
			tileParticles.setMovingRightImages(null);
		}
		
		if (!(exitImages == null)) {
			List<int[]> loadExitImages = new ArrayList<int[]>();
			int[] exitPixels = new int[sheet.size * sheet.size];
			for (Vector2D tileLoc : exitImages) {
				exitPixels = sheet.crop(tileLoc.getIntX(), tileLoc.getIntY()).getRGB(0, 0, sheet.size, sheet.size, null, 0, sheet.size);
				loadExitImages.add(exitPixels);
			}
			tileParticles.setExitImages(loadExitImages);
		} else {
			tileParticles.setExitImages(null);
		}
		
		if (!(passiveImages == null)) {
			List<int[]> loadPassiveImages = new ArrayList<int[]>();
			int[] passivePixels = new int[sheet.size * sheet.size];
			for (Vector2D tileLoc : passiveImages) {
				passivePixels = sheet.crop(tileLoc.getIntX(), tileLoc.getIntY()).getRGB(0, 0, sheet.size, sheet.size, null, 0, sheet.size);
				loadPassiveImages.add(passivePixels);
			}
			tileParticles.setPassiveImages(loadPassiveImages);
		} else {
			tileParticles.setPassiveImages(null);
		}
		
		return tileParticles; 
	}
	
}
