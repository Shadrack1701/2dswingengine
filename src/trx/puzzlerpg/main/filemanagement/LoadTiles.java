package trx.puzzlerpg.main.filemanagement;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import trx.puzzlerpg.main.gfx.Screen;
import trx.puzzlerpg.main.gfx.SpriteSheet;
import trx.puzzlerpg.main.level.tiles.Collision;
import trx.puzzlerpg.main.level.tiles.Tile;
import trx.puzzlerpg.main.level.tiles.TileTypes;
import trx.puzzlerpg.main.util.Vector2D;

public class LoadTiles {

	private byte id;
	private List<TileTypes> loadProperties = new ArrayList<TileTypes>();
	private String name;
	private String levelColor;
	private int tileAnimationDelay;
	private boolean animationSwitch;
	private boolean animationRandomize;
	private List<List<Vector2D>> tileLocations = new ArrayList<List<Vector2D>>();
	private LoadTileParticles loadTileParticles;
	private Collision collision;
	private int subTiles[];

	public Tile LoadGameTiles(SpriteSheet sheet) {
		Tile tile = new Tile();
		tile.setId(this.id);
		tile.setName(this.name);
		tile.setLevelColor(this.levelColor);
		tile.setTileAnimationDelay(this.tileAnimationDelay);
		tile.setAnimationSwitch(this.animationSwitch);
		tile.setAnimationRandomize(this.animationRandomize);
		tile.setCollision(collision);
		tile.setSubTiles(subTiles);
		if (this.loadTileParticles == null) {
			tile.setTileParticles(null);
		} else {
			tile.setTileParticles(this.loadTileParticles.getTileParticles(sheet));
		}
		tile.setTileProperties(EnumSet.copyOf(loadProperties));
		List<int[]> tileImages = new ArrayList<int[]>();
		if (tileLocations.size() == 2) {
			for(int x = 0; x < tileLocations.get(0).size(); x++) {
				int[] pixels1 = new int[sheet.size * sheet.size];
				int[] pixels2 = new int[sheet.size * sheet.size];
				int[] combinedPixels = new int[sheet.size * sheet.size];
				pixels1 = sheet.crop(tileLocations.get(0).get(x).getIntX(), tileLocations.get(0).get(x).getIntY()).getRGB(0, 0, sheet.size, sheet.size, null, 0, sheet.size);
				pixels2 = sheet.crop(tileLocations.get(1).get(x).getIntX(), tileLocations.get(1).get(x).getIntY()).getRGB(0, 0, sheet.size, sheet.size, null, 0, sheet.size);
				for(int y = 0; y < pixels1.length; y++) {
					if (pixels2[y] == Screen.TRANSPARENT_PIXEL || pixels2[y] == 0) {
						combinedPixels[y] = pixels1[y];
					} else combinedPixels[y] = pixels2[y];
				}
				tileImages.add(combinedPixels);	
			}
		} else {
			int[] pixels = new int[sheet.size * sheet.size];
			for (Vector2D tileLoc : tileLocations.get(0)) {
				pixels = sheet.crop(tileLoc.getIntX(), tileLoc.getIntY()).getRGB(0, 0, sheet.size, sheet.size, null, 0, sheet.size);
				tileImages.add(pixels);
			}
		}
		tile.setTileImages(tileImages);
		return tile;
	}
	
}
