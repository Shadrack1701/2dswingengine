package trx.puzzlerpg.main.filemanagement;

import java.util.ArrayList;
import java.util.List;
import trx.puzzlerpg.main.entities.Entity;
import trx.puzzlerpg.main.gfx.SpriteSheet;
import trx.puzzlerpg.main.level.Level;
import trx.puzzlerpg.main.level.tiles.Tile;

public class LoadLevel {
	
	private int levelId;
	private String name;
	private String displayName;
	private int currentArea;
	private ArrayList<Tile> levelTiles = new ArrayList<>();
	private List<LoadEntity> levelEntities = new ArrayList<>();
	private List<LoadTiles> loadingTiles = new ArrayList<>();
	private List<LoadArea> loadAreas = new ArrayList<>();
	private String levelSpriteSheet;
	private int levelSpriteSheetSize;
	private int levelSpriteSheetBinary;
	private String levelEntitiesSheet;
	private int levelEntitiesSheetSize;
	private int levelEntitiesSheetBinary;
	private SpriteSheet levelSheet;
	private SpriteSheet entitiesSheet;
	
	public Level getGameLevel() {
		
		Level level = new Level();
		level.setLevelId(this.levelId);
		level.setName(this.name);
		level.setDisplayName(this.displayName);
		level.setCurrentArea(this.currentArea);
		level.levelTiles = this.levelTiles;
		level.levelSpriteSheetBinary = this.levelSpriteSheetBinary;
		level.levelSpriteSheetSize = this.levelSpriteSheetSize;
		levelSheet = new SpriteSheet(this.levelSpriteSheet, this.levelSpriteSheetSize, this.levelSpriteSheetBinary);

		entitiesSheet = new SpriteSheet(this.levelEntitiesSheet, this.levelEntitiesSheetSize, this.levelEntitiesSheetBinary);
		
		for (LoadArea loadArea : loadAreas) {
			level.areas.add(loadArea.getArea(entitiesSheet));
		}
		
		for (LoadTiles loadingTiles : this.loadingTiles) {
			levelTiles.add(loadingTiles.LoadGameTiles(levelSheet));
		}
		level.loadLevelFromFile();
		
		List<Entity> entities = new ArrayList<>();
		for (LoadEntity loadEntity : levelEntities) {
			entities.addAll(loadEntity.getEntities(entitiesSheet));
		}
		level.setLevelEntities(entities);
		
		return level;
	}
/*******************************************GETTERS AND SETTERS***********************************************/	
	public int getLevelId() {
		return levelId;
	}
	public void setLevelId(int levelId) {
		this.levelId = levelId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public int getCurrentArea() {
		return currentArea;
	}
	public void setCurrentArea(int currentArea) {
		this.currentArea = currentArea;
	}
	public List<LoadEntity> getLevelEntities() {
		return levelEntities;
	}
	public void setLevelEntities(List<LoadEntity> entities) {
		this.levelEntities = entities;
	}
	public List<LoadTiles> getLoadTiles() {
		return loadingTiles;
	}
	public void setLoadTiles(List<LoadTiles> loadTiles) {
		this.loadingTiles = loadTiles;
	}
	public List<LoadArea> getLoadAreas() {
		return loadAreas;
	}
	public void setLoadAreas(List<LoadArea> loadAreas) {
		this.loadAreas = loadAreas;
	}
	public String getLevelSpriteSheet() {
		return levelSpriteSheet;
	}
	public void setLevelSpriteSheet(String levelSpriteSheet) {
		this.levelSpriteSheet = levelSpriteSheet;
	}
	public int getLevelSpriteSheetSize() {
		return levelSpriteSheetSize;
	}
	public void setLevelSpriteSheetSize(int levelSpriteSheetSize) {
		this.levelSpriteSheetSize = levelSpriteSheetSize;
	}
	public int getLevelSpriteSheetBinary() {
		return levelSpriteSheetBinary;
	}
	public void setLevelSpriteSheetBinary(int levelSpriteSheetBinary) {
		this.levelSpriteSheetBinary = levelSpriteSheetBinary;
	}
}
