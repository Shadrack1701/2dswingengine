package trx.puzzlerpg.main.filemanagement;

import java.util.ArrayList;
import java.util.List;
import trx.puzzlerpg.main.gfx.SpriteSheet;
import trx.puzzlerpg.main.gfx.gui.Hud;
import trx.puzzlerpg.main.util.Vector2D;

public class LoadHud {

	private List<Vector2D> healthBarImage = new ArrayList<Vector2D>();
	private List<Vector2D> healthImage = new ArrayList<Vector2D>();
	private String sheetPath;
	
	public Hud loadHud() {
		Hud hud = new Hud();
		SpriteSheet sheet = new SpriteSheet(sheetPath);
		
		hud.setHealthBarWidth(healthBarImage.get(1).getIntX() - healthBarImage.get(0).getIntX());
		hud.setHealthBarHeight(healthBarImage.get(1).getIntY() - healthBarImage.get(0).getIntY());
		hud.setHealthBarImage(sheet.cropVector(healthBarImage.get(0), healthBarImage.get(1), hud.getHealthBarWidth(), hud.getHealthBarHeight()).getRGB(0, 0,  hud.getHealthBarWidth(),  hud.getHealthBarHeight(), null, 0, hud.getHealthBarWidth()));
				
		hud.setHealthWidth(healthImage.get(1).getIntX() - healthImage.get(0).getIntX());
		hud.setHealthHeight(healthImage.get(1).getIntY() - healthImage.get(0).getIntY());
		hud.setHealthImage(sheet.cropVector(healthImage.get(0), healthImage.get(1), hud.getHealthWidth(), hud.getHealthHeight()).getRGB(0, 0,  hud.getHealthWidth(),  hud.getHealthHeight(), null, 0, hud.getHealthWidth()));		
		
		return hud;
	}
}
