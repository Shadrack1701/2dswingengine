package trx.puzzlerpg.main.filemanagement;

import java.util.ArrayList;
import java.util.List;

import trx.puzzlerpg.main.gfx.SpriteSheet;
import trx.puzzlerpg.main.gfx.gui.MainMenu;
import trx.puzzlerpg.main.util.Vector2D;

public class LoadMainMenu {

    private List<Vector2D> testRender = new ArrayList<Vector2D>();
	private Vector2D titleLoc;
	private List<Vector2D> titleImage = new ArrayList<Vector2D>();
	private Vector2D newGameLoc;
	private List<Vector2D> newGameImage = new ArrayList<Vector2D>();
	private Vector2D continueLoc;
	private List<Vector2D> continueImage = new ArrayList<Vector2D>();
	private Vector2D settingsLoc;
	private List<Vector2D> settingsImage = new ArrayList<Vector2D>();
	private Vector2D quitDesktopLoc;
	private List<Vector2D> quitDesktopImage = new ArrayList<Vector2D>();
	private List<Vector2D> newGameSelImage = new ArrayList<Vector2D>();
	private List<Vector2D> continueSelImage = new ArrayList<Vector2D>();
	private List<Vector2D> settingsSelImage = new ArrayList<Vector2D>();
	private List<Vector2D> quitDesktopSelImage = new ArrayList<Vector2D>();
	private Vector2D cursorLoc;
	private List<Vector2D> cursorImage = new ArrayList<Vector2D>();
	private int selectedOption;
	private String sheetPath;
	
	public MainMenu loadMenu() {
		MainMenu menu = new MainMenu();
		SpriteSheet sheet = new SpriteSheet(sheetPath);
		menu.setTitleLoc(this.titleLoc);
		menu.setNewGameLoc(this.newGameLoc);
		menu.setContinueLoc(this.continueLoc);
		menu.setSettingsLoc(this.settingsLoc);
		menu.setQuitDesktopLoc(this.quitDesktopLoc);
		menu.setCursorLoc(this.cursorLoc);
		menu.setSelectedOption(selectedOption);

		menu.setTitleWidth(titleImage.get(1).getIntX() - titleImage.get(0).getIntX());
		menu.setTitleHeight(titleImage.get(1).getIntY() - titleImage.get(0).getIntY());
		menu.setTitleImage(sheet.cropVector(titleImage.get(0), titleImage.get(1), menu.getTitleWidth(), menu.getTitleHeight()).getRGB(0, 0,  menu.getTitleWidth(),  menu.getTitleHeight(), null, 0, menu.getTitleWidth()));
		
		menu.setNewGameWidth(newGameImage.get(1).getIntX() - newGameImage.get(0).getIntX());
		menu.setNewGameHeight(newGameImage.get(1).getIntY() - newGameImage.get(0).getIntY());
		menu.setNewGameImage(sheet.cropVector(newGameImage.get(0), newGameImage.get(1), menu.getNewGameWidth(), menu.getNewGameHeight()).getRGB(0, 0,  newGameImage.get(1).getIntX() - newGameImage.get(0).getIntX(),  newGameImage.get(1).getIntY() - newGameImage.get(0).getIntY(), null, 0, newGameImage.get(1).getIntX() - newGameImage.get(0).getIntX()));
		
		menu.setContinueWidth(continueImage.get(1).getIntX() - continueImage.get(0).getIntX());
		menu.setContinueHeight(continueImage.get(1).getIntY() - continueImage.get(0).getIntY());
		menu.setContinueImage(sheet.cropVector(continueImage.get(0), continueImage.get(1), menu.getContinueWidth(), menu.getContinueHeight()).getRGB(0, 0,  continueImage.get(1).getIntX() - continueImage.get(0).getIntX(),  continueImage.get(1).getIntY() - continueImage.get(0).getIntY(), null, 0, continueImage.get(1).getIntX() - continueImage.get(0).getIntX()));
		
		menu.setSettingsWidth(settingsImage.get(1).getIntX() - settingsImage.get(0).getIntX());
		menu.setSettingsHeight(settingsImage.get(1).getIntY() - settingsImage.get(0).getIntY());
		menu.setSettingsImage(sheet.cropVector(settingsImage.get(0), settingsImage.get(1), menu.getSettingsWidth(), menu.getSettingsHeight()).getRGB(0, 0,  settingsImage.get(1).getIntX() - settingsImage.get(0).getIntX(),  settingsImage.get(1).getIntY() - settingsImage.get(0).getIntY(), null, 0, settingsImage.get(1).getIntX() - settingsImage.get(0).getIntX()));
		
		menu.setQuitDesktopWidth(quitDesktopImage.get(1).getIntX() - quitDesktopImage.get(0).getIntX());
		menu.setQuitDesktopHeight(quitDesktopImage.get(1).getIntY() - quitDesktopImage.get(0).getIntY());
		menu.setQuitDesktopImage(sheet.cropVector(quitDesktopImage.get(0), quitDesktopImage.get(1), menu.getQuitDesktopWidth(), menu.getQuitDesktopHeight()).getRGB(0, 0,  quitDesktopImage.get(1).getIntX() - quitDesktopImage.get(0).getIntX(),  quitDesktopImage.get(1).getIntY() - quitDesktopImage.get(0).getIntY(), null, 0, quitDesktopImage.get(1).getIntX() - quitDesktopImage.get(0).getIntX()));
		
		menu.setNewGameSelWidth(newGameSelImage.get(1).getIntX() - newGameSelImage.get(0).getIntX());
		menu.setNewGameSelHeight(newGameSelImage.get(1).getIntY() - newGameSelImage.get(0).getIntY());
		menu.setNewGameSelImage(sheet.cropVector(newGameSelImage.get(0), newGameSelImage.get(1), menu.getNewGameSelWidth(), menu.getNewGameSelHeight()).getRGB(0, 0,  newGameSelImage.get(1).getIntX() - newGameSelImage.get(0).getIntX(),  newGameSelImage.get(1).getIntY() - newGameSelImage.get(0).getIntY(), null, 0, newGameSelImage.get(1).getIntX() - newGameSelImage.get(0).getIntX()));
		
		menu.setContinueSelWidth(continueSelImage.get(1).getIntX() - continueSelImage.get(0).getIntX());
		menu.setContinueSelHeight(continueSelImage.get(1).getIntY() - continueSelImage.get(0).getIntY());
		menu.setContinueSelImage(sheet.cropVector(continueSelImage.get(0), continueSelImage.get(1), menu.getContinueSelWidth(), menu.getContinueSelHeight()).getRGB(0, 0,  continueSelImage.get(1).getIntX() - continueSelImage.get(0).getIntX(),  continueSelImage.get(1).getIntY() - continueSelImage.get(0).getIntY(), null, 0, continueSelImage.get(1).getIntX() - continueSelImage.get(0).getIntX()));
		
		menu.setSettingsSelWidth(settingsSelImage.get(1).getIntX() - settingsSelImage.get(0).getIntX());
		menu.setSettingsSelHeight(settingsSelImage.get(1).getIntY() - settingsSelImage.get(0).getIntY());
		menu.setSettingsSelImage(sheet.cropVector(settingsSelImage.get(0), settingsSelImage.get(1), menu.getSettingsSelWidth(), menu.getSettingsSelHeight()).getRGB(0, 0,  settingsSelImage.get(1).getIntX() - settingsSelImage.get(0).getIntX(),  settingsSelImage.get(1).getIntY() - settingsSelImage.get(0).getIntY(), null, 0, settingsSelImage.get(1).getIntX() - settingsSelImage.get(0).getIntX()));
		
		menu.setQuitDesktopSelWidth(quitDesktopSelImage.get(1).getIntX() - quitDesktopSelImage.get(0).getIntX());
		menu.setQuitDesktopSelHeight(quitDesktopSelImage.get(1).getIntY() - quitDesktopSelImage.get(0).getIntY());
		menu.setQuitDesktopSelImage(sheet.cropVector(quitDesktopSelImage.get(0), quitDesktopSelImage.get(1), menu.getQuitDesktopSelWidth(), menu.getQuitDesktopSelHeight()).getRGB(0, 0,  quitDesktopSelImage.get(1).getIntX() - quitDesktopSelImage.get(0).getIntX(),  quitDesktopSelImage.get(1).getIntY() - quitDesktopSelImage.get(0).getIntY(), null, 0, quitDesktopSelImage.get(1).getIntX() - quitDesktopSelImage.get(0).getIntX()));
		
		menu.setCursorWidth(cursorImage.get(1).getIntX() - cursorImage.get(0).getIntX());
		menu.setCursorHeight(cursorImage.get(1).getIntY() - cursorImage.get(0).getIntY());
		menu.setCursorImage(sheet.cropVector(cursorImage.get(0), cursorImage.get(1), menu.getCursorWidth(), menu.getCursorHeight()).getRGB(0, 0,  cursorImage.get(1).getIntX() - cursorImage.get(0).getIntX(),  cursorImage.get(1).getIntY() - cursorImage.get(0).getIntY(), null, 0, cursorImage.get(1).getIntX() - cursorImage.get(0).getIntX()));
		
		return menu;
	}
}
