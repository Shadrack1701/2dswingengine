package trx.puzzlerpg.main.filemanagement;

import trx.puzzlerpg.main.gfx.SpriteSheet;
import trx.puzzlerpg.main.gfx.ProjectileLibrary;
import trx.puzzlerpg.main.gfx.gui.SpriteSet;
import trx.puzzlerpg.main.util.Vector2D;
import java.util.ArrayList;
import java.util.List;

public class LoadProjectiles {

    private List<LoadSpriteSet> projectileSpriteList = new ArrayList<>();
    private String projectilesSheet;
    private int projectilesSheetSize;
    private int projectilesSheetBinary;

    public void loadProjectiles() {
        SpriteSheet projectilesSheet = new SpriteSheet(this.projectilesSheet, this.projectilesSheetSize, this.projectilesSheetBinary);
        List<SpriteSet> projectileList = new ArrayList<>();
        for (LoadSpriteSet loadSpriteSet : this.projectileSpriteList) {
            projectileList.add(loadSpriteSet.loadSpriteSet(projectilesSheet));
        }

        ProjectileLibrary.setProjectileList(projectileList);
    }

        private List<int[]> setImage(SpriteSheet sheet, List<Vector2D> spriteLocations, int spriteSize) {
            List<int[]> imageInt = new ArrayList<int[]>();
            int[] imagePixels = new int[sheet.size * sheet.size];
            for (Vector2D spriteLoc : spriteLocations) {
                imagePixels = sheet.crop(spriteLoc.getIntX(), spriteLoc.getIntY(), spriteSize).getRGB(0, 0, spriteSize, spriteSize, null,
                        0, spriteSize);
                imageInt.add(imagePixels);
            }

            return imageInt;
        }


    }
