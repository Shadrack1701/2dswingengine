package trx.puzzlerpg.main.filemanagement;

import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import trx.puzzlerpg.main.entities.Entity;
import trx.puzzlerpg.main.gfx.SpriteSheet;
import trx.puzzlerpg.main.level.Area;
import trx.puzzlerpg.main.level.tiles.LoadTile;

public class LoadArea {
	
	private byte[] tiles;
	private BufferedImage image;
	private int areaId;
	private String areaName;
	private String areaTilePath;
	private String heightMapPath;
	private List<LoadTile> loadTiles = new ArrayList<LoadTile>();
	private List<LoadEntity> areaEntities = new ArrayList<LoadEntity>();

	public Area getArea(SpriteSheet entitiesSheet) {
		Area area = new Area();
		area.setImage(this.image);
		area.setAreaId(this.areaId);
		area.setAreaName(this.areaName);
		area.setAreaPath(this.areaTilePath);
		area.setLoadTiles(this.loadTiles);
		List<Entity> entities = new ArrayList<Entity>();
		for (LoadEntity entity : areaEntities) {
			entities.addAll(entity.getEntities(entitiesSheet));
		}
		area.setAreaEntities(entities);
		
		if (heightMapPath != null) { 
			Gson gson = new Gson();
			Type collectionType = new TypeToken<ArrayList<int[]>>(){}.getType();
			try {
				ArrayList<int[]> heightMap = gson.fromJson(new FileReader(System.getProperty("user.dir") + "//res//" + heightMapPath), collectionType);
				area.setHeightMap(heightMap);
			} catch (JsonSyntaxException | JsonIOException | FileNotFoundException e) {
				e.printStackTrace();
			}
		}
		
		return area;
	}

/******************************************** GETTERS AND SETTERS***********************************************/
	public int getAreaId() {
		return areaId;
	}
	public String getAreaName() {
		return areaName;
	}
	public byte[] getTiles() {
		return tiles;
	}
	public void setTiles(byte[] tiles) {
		this.tiles = tiles;
	}
	public void setTile(byte tile, int location) {
		this.tiles[location] = tile;
	}
	public BufferedImage getImage() {
		return image;
	}
	public void setImage(BufferedImage image) {
		this.image = image;
	}
	public String getAreaPath() {
		return areaTilePath;
	}
	public void setAreaPath(String areaPath) {
		this.areaTilePath = areaPath;
	}
	public List<LoadTile> getLoadTiles() {
		return loadTiles;
	}
	public void setLoadTiles(List<LoadTile> loadTiles) {
		this.loadTiles = loadTiles;
	}
	public List<LoadEntity> getAreaEntities() {
		return areaEntities;
	}
	public void setAreaEntities(List<LoadEntity> entities) {
		this.areaEntities = entities;
	}
}
