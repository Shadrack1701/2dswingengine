package trx.puzzlerpg.main.entities;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import trx.puzzlerpg.main.entities.item.Equipment;
import trx.puzzlerpg.main.entities.item.subtypes.Weapon;
import trx.puzzlerpg.main.entities.subtypes.Projectile;
import trx.puzzlerpg.main.gfx.DamageText;
import trx.puzzlerpg.main.gfx.RenderList;
import trx.puzzlerpg.main.gfx.Screen;
import trx.puzzlerpg.main.gfx.ProjectileLibrary;
import trx.puzzlerpg.main.gfx.gui.SpriteSet;
import trx.puzzlerpg.main.level.tiles.Collision;
import trx.puzzlerpg.main.level.tiles.Tile;
import trx.puzzlerpg.main.level.tiles.TileParticles;
import trx.puzzlerpg.main.level.tiles.TileTypes;
import trx.puzzlerpg.main.util.SubTileCollisionProcessor;
import trx.puzzlerpg.main.util.Vector2D;

public abstract class Mob extends Entity {
	protected int speed;
	protected boolean isMoving;
	protected int animationIndex;
	protected long animationTime;
	protected int animationSpeed;
	private boolean enterAnimation = false;
	private boolean exitAnimation = false;
    protected SpriteSet spriteSet;
	protected int enterIndex = 0;
	protected int movingIndex = 0;
	protected int passiveIndex = 0;
	protected int exitIndex = 0;
	protected long enterTime = 0;
	protected long movingTime = 0;
	protected long passiveTime = 0;
	protected long exitTime = 0;
	protected boolean swimming = false;
	protected int invincibleDuration;	
	protected List<DamageText> damageTexts = new ArrayList<>();
	protected Equipment equipment;
	protected int xa = 0;
	protected int ya = 0;
    protected int attackTime = 1000;
	
	private boolean tileSwitch;
	private Tile previousTile;
	private Tile currentTile;
	private Tile currentTile1;
	private Tile currentTile2;
	private Tile currentTile3;
	private Tile currentTile4;
	private long previousBobTime;
	private List<RenderList> renderList;
	private long invincibleTimer;

	public Mob() {
		super();
	}

	public void init() {
		setCurrentTile();
		renderList = new ArrayList<>();
	}

	@Override
	public void tick() {}

	@Override
	public void render(Screen screen) {
		renderList.clear();

		int modifier = spriteSize / 2;
		int xOffset = vector.getIntX() - spriteSize * entityScale / 2;
		int yOffset = vector.getIntY() - spriteSize * entityScale / 2;
		
		if (swimming) {
			yOffset = setBobAmount(yOffset);
		}
		
		checkTileParticles(screen, xOffset, yOffset, modifier);
			
		drawMob(screen, xOffset, yOffset);

		if (!renderList.isEmpty()) {
			for (RenderList render : renderList) {
				screen.render(render.xOffset, render.yOffset, render.image, render.mirrorImage, render.scale, render.spriteWidth, render.spriteHeight);
			}
		}
	}

	public void renderText(Screen screen, Graphics g) {
		for (DamageText damageText : damageTexts) {
			damageText.render(screen, g, this.vector);
		}
	}

    protected void checkEnvironmentDamage() {
        if (currentTile1.properties.contains(TileTypes.Hot) || currentTile2.properties.contains(TileTypes.Hot) || currentTile3.properties.contains(TileTypes.Hot) || currentTile4.properties.contains(TileTypes.Hot)) {
            damageEntity(10);
        }
    }

    public void move(int xa, int ya, boolean bothMove) {
		if (xa != 0 && ya != 0) {
			move(xa, 0, true);
			move(0, ya, true);
			return;
		}
		
		this.xa = xa;
		this.ya = ya;
		int tempMovingDir = 0;
		if (ya < 0) tempMovingDir = 0;
		if (ya > 0) tempMovingDir = 1;
		if (xa < 0) tempMovingDir = 2;
		if (xa > 0) tempMovingDir = 3;
		
		if (!hasCollided(xa * speed, ya * speed, tempMovingDir) && !checkEntityCollision(xa * speed , ya * speed, tempMovingDir)) {
			xa = this.xa;
			ya = this.ya;
			isMoving = true;
			movingDir = tempMovingDir;
			if (!bothMove) {
				vector.x += xa * speed;
				vector.y += ya * speed;
			} else {
				vector.x += xa * speed * .70;
				vector.y += ya * speed * .70;
			}
			
			UpdateAnimationIndex();
		} else if (!bothMove) movingDir = tempMovingDir;
	}

    public void attack(int xa, int ya, Weapon weapon) {
        Projectile projectile = new Projectile();

        projectile.setName(weapon.name);
        projectile.setSpeed(weapon.getProjectileSpeed());
        projectile.setAnimationSpeed(weapon.getProjectileAnimationSpeed());
        projectile.vector = new Vector2D(xa, ya); //plus some logic for facing direction getting it right outside the entity collisionbox
        projectile.setMovingDir(this.movingDir);
        projectile.setEntityScale(weapon.entityScale);
        projectile.setCollisionCoords(weapon.collisionCoords);
        projectile.setCollision(new Rectangle(weapon.getProjectileCollision().get(0).getIntX(), weapon.getProjectileCollision().get(0).getIntY(), weapon.getProjectileCollision().get(1).getIntX() - weapon.getProjectileCollision().get(0).getIntX(), weapon.getProjectileCollision().get(2).getIntY() - weapon.getProjectileCollision().get(0).getIntY()));
        projectile.setSpriteSize(weapon.spriteSize);
        projectile.setMaxHealth(0);
        projectile.setCurrHealth(0);
        projectile.setSpriteSet(ProjectileLibrary.getProjectileSet(weapon.getProjectileSetId()));
        projectile.setProjectileBirth(System.currentTimeMillis());
        projectile.setVector(this.vector);

        level.getAreas().get(level.getCurrentArea()).getAreaEntities().add(projectile);
    }

	private void UpdateAnimationIndex() {
		long currentTime = System.currentTimeMillis();
		long animationGap = currentTime - animationTime;
		if (movingDir == 0 ) {
			if (animationGap >= animationSpeed) {
				animationTime = currentTime;
				if (animationIndex >= spriteSet.getUpSprites().size() - 1) {
					animationIndex = 0;
				} else {
					animationIndex++;
				}
			}
		} else if (movingDir == 1) {
			if (animationGap >= animationSpeed) {
				animationTime = currentTime;
				if (animationIndex >= spriteSet.getDownSprites().size() - 1) {
					animationIndex = 0;
				} else {
					animationIndex++;
				}
			}
		} else if (movingDir == 2) {
			if (animationGap >= animationSpeed) {
				animationTime = currentTime;
				if (animationIndex >= spriteSet.getLeftSprites().size() - 1) {
					animationIndex = 0;
				} else {
					animationIndex++;
				}
			}
		} else {
			if (animationGap >= animationSpeed) {
				animationTime = currentTime;
				if (animationIndex >= spriteSet.getRightSprites().size() - 1) {
					animationIndex = 0;
				} else {
					animationIndex++;
				}
			}
		}
	}

	public boolean hasCollided(int xa, int ya, int tempMovingDir) {
		Rectangle mobRectangle = new Rectangle(this.vector.getIntX() + xa + this.getCollisionCoords().get(0).getIntX(), this.vector.getIntY() + ya + this.getCollisionCoords().get(0).getIntY(), this.getCollision().width, this.getCollision().height);
		Vector2D currTile1V = new Vector2D((vector.getIntX() + collisionCoords.get(0).getIntX()) >> level.levelSpriteSheetBinary, (vector.getIntY() + collisionCoords.get(0).getIntY()) >> level.levelSpriteSheetBinary);
		Vector2D currTile2V = new Vector2D((vector.getIntX() + collisionCoords.get(1).getIntX()) >> level.levelSpriteSheetBinary, (vector.getIntY() + collisionCoords.get(1).getIntY()) >> level.levelSpriteSheetBinary);
		Vector2D currTile3V = new Vector2D((vector.getIntX() + collisionCoords.get(2).getIntX()) >> level.levelSpriteSheetBinary, (vector.getIntY() + collisionCoords.get(2).getIntY()) >> level.levelSpriteSheetBinary);
		Vector2D currTile4V = new Vector2D((vector.getIntX() + collisionCoords.get(3).getIntX()) >> level.levelSpriteSheetBinary, (vector.getIntY() + collisionCoords.get(3).getIntY()) >> level.levelSpriteSheetBinary);
		
		Tile currTile1 = level.getTile(currTile1V.getIntX(), currTile1V.getIntY());
		Tile currTile2 = level.getTile(currTile2V.getIntX(), currTile2V.getIntY());
		Tile currTile3 = level.getTile(currTile3V.getIntX(), currTile3V.getIntY());
		Tile currTile4 = level.getTile(currTile4V.getIntX(), currTile4V.getIntY());
		
		if (currTile1.properties.contains(TileTypes.Solid) || currTile2.properties.contains(TileTypes.Solid) || currTile3.properties.contains(TileTypes.Solid) || currTile4.properties.contains(TileTypes.Solid)) {
			if (!currTile1.getCollision().equals(Collision.Full) || !currTile2.getCollision().equals(Collision.Full) ||  !currTile3.getCollision().equals(Collision.Full) || !currTile4.getCollision().equals(Collision.Full)) {
				if (subTileCollision(currTile1, currTile1V, currTile2, currTile2V, currTile3, currTile3V, currTile4, currTile4V, tempMovingDir, mobRectangle)) return true;
			}
		} 
		
		Vector2D newTile1V = new Vector2D((vector.getIntX() + collisionCoords.get(0).getIntX() + xa) >> level.levelSpriteSheetBinary, (vector.getIntY() + collisionCoords.get(0).getIntY() + ya) >> level.levelSpriteSheetBinary);
		Vector2D newTile2V = new Vector2D((vector.getIntX() + collisionCoords.get(1).getIntX() + xa) >> level.levelSpriteSheetBinary, (vector.getIntY() + collisionCoords.get(1).getIntY() + ya) >> level.levelSpriteSheetBinary);
		Vector2D newTile3V = new Vector2D((vector.getIntX() + collisionCoords.get(2).getIntX() + xa) >> level.levelSpriteSheetBinary, (vector.getIntY() + collisionCoords.get(2).getIntY() + ya) >> level.levelSpriteSheetBinary);
		Vector2D newTile4V = new Vector2D((vector.getIntX() + collisionCoords.get(3).getIntX() + xa) >> level.levelSpriteSheetBinary, (vector.getIntY() + collisionCoords.get(3).getIntY() + ya) >> level.levelSpriteSheetBinary);		
		Tile newTile1 = level.getTile(newTile1V.getIntX(), newTile1V.getIntY());
		Tile newTile2 = level.getTile(newTile2V.getIntX(), newTile2V.getIntY());
		Tile newTile3 = level.getTile(newTile3V.getIntX(), newTile3V.getIntY());
		Tile newTile4 = level.getTile(newTile4V.getIntX(), newTile4V.getIntY());
		
		if (newTile1.properties.contains(TileTypes.Solid) || newTile2.properties.contains(TileTypes.Solid) || newTile3.properties.contains(TileTypes.Solid) || newTile4.properties.contains(TileTypes.Solid)) {
			if (!newTile1.getCollision().equals(Collision.Full) || !newTile2.getCollision().equals(Collision.Full) ||  !newTile3.getCollision().equals(Collision.Full) || !newTile4.getCollision().equals(Collision.Full)) {
				if (subTileCollision(newTile1, newTile1V, newTile2, newTile2V, newTile3, newTile3V, newTile4, newTile4V, tempMovingDir, mobRectangle)) return true;
			} else return true; 
		} 
		
		if (level.getAreas().get(level.getCurrentArea()).getHeightMap() != null) {
			ArrayList<int[]> heightMap = level.areas.get(level.getCurrentArea()).getHeightMap();
			Rectangle currMobRectangle = new Rectangle(this.vector.getIntX() + this.getCollisionCoords().get(0).getIntX(), this.vector.getIntY() + this.getCollisionCoords().get(0).getIntY(), this.getCollision().width, this.getCollision().height);
			int currHeight = 0, newHeight = 0;
			
			currHeight = getMaxTileHeight(heightMap, currTile1, currTile1V, currHeight, currMobRectangle);
			currHeight = getMaxTileHeight(heightMap, currTile2, currTile2V, currHeight, currMobRectangle);
			currHeight = getMaxTileHeight(heightMap, currTile3, currTile3V, currHeight, currMobRectangle);
			currHeight = getMaxTileHeight(heightMap, currTile4, currTile4V, currHeight, currMobRectangle);
			
			if (tempMovingDir == 0) {
				newHeight = getMaxTileHeight(heightMap, newTile1, newTile1V, newHeight, mobRectangle);
				newHeight = getMaxTileHeight(heightMap, newTile2, newTile2V, newHeight, mobRectangle);
			} else if (tempMovingDir == 1) {
				newHeight = getMaxTileHeight(heightMap, newTile3, newTile3V, newHeight, mobRectangle);
				newHeight = getMaxTileHeight(heightMap, newTile4, newTile4V, newHeight, mobRectangle);
			} else if (tempMovingDir == 2) {
				newHeight = getMaxTileHeight(heightMap, newTile1, newTile1V, newHeight, mobRectangle);
				newHeight = getMaxTileHeight(heightMap, newTile3, newTile3V, newHeight, mobRectangle);
			} else if (tempMovingDir == 3) {
				newHeight = getMaxTileHeight(heightMap, newTile2, newTile2V, newHeight, mobRectangle);
				newHeight = getMaxTileHeight(heightMap, newTile4, newTile4V, newHeight, mobRectangle);
			}
			
			if (newHeight - currHeight > 1)  return true;
		}
		
		setCurrentTile(newTile1V, newTile2V, newTile3V, newTile4V, newTile1, newTile2, newTile3, newTile4);
		return false;
	}

	private int getMaxTileHeight(ArrayList<int[]> heightMap, Tile currTile, Vector2D currTileV, int height, Rectangle mobRectangle) {
		int[] tempHeight = heightMap.get(currTileV.getIntX() + currTileV.getIntY() * level.areas.get(level.getCurrentArea()).getWidth());
		if (tempHeight.length > 1) {
			if (SubTileCollisionProcessor.processSubTileCollision(currTile.getCollision(), new Rectangle(currTileV.getIntX() * level.levelSpriteSheetSize, currTileV.getIntY() * level.levelSpriteSheetSize, level.levelSpriteSheetSize, level.levelSpriteSheetSize), mobRectangle)) {
				if (tempHeight[0] > height) return tempHeight[0];
			} else if (tempHeight[0] > height) return tempHeight[1];
		} else if (tempHeight[0] > height) return tempHeight[0];
		return height;	
	}

	protected boolean subTileCollision(Tile tile1, Vector2D tile1V, Tile tile2, Vector2D tile2V, Tile tile3, Vector2D tile3V, Tile tile4, Vector2D tile4V, int tempMovingDir, Rectangle mobRectangle) {
		switch(tempMovingDir) {
			case 0: {
				if (!(tile1.getCollision().equals(Collision.Full)) && tile1.properties.contains(TileTypes.Solid)) {
					if (SubTileCollisionProcessor.processSubTileCollision(tile1.getCollision(), new Rectangle(tile1V.getIntX() * level.levelSpriteSheetSize, tile1V.getIntY() * level.levelSpriteSheetSize, level.levelSpriteSheetSize, level.levelSpriteSheetSize), mobRectangle)) return true;
				}
				if (!(tile2.getCollision().equals(Collision.Full)) && tile2.properties.contains(TileTypes.Solid)) {
					if (SubTileCollisionProcessor.processSubTileCollision(tile2.getCollision(), new Rectangle(tile2V.getIntX() * level.levelSpriteSheetSize, tile2V.getIntY() * level.levelSpriteSheetSize, level.levelSpriteSheetSize, level.levelSpriteSheetSize), mobRectangle)) return true;
				}
			}
			case 1: {
				if (!(tile3.getCollision().equals(Collision.Full)) && tile3.properties.contains(TileTypes.Solid)) {
					if (SubTileCollisionProcessor.processSubTileCollision(tile3.getCollision(), new Rectangle(tile3V.getIntX() * level.levelSpriteSheetSize, tile3V.getIntY() * level.levelSpriteSheetSize, level.levelSpriteSheetSize, level.levelSpriteSheetSize), mobRectangle)) return true;
				}
				if (!(tile4.getCollision().equals(Collision.Full)) && tile4.properties.contains(TileTypes.Solid)) {
					if (SubTileCollisionProcessor.processSubTileCollision(tile4.getCollision(), new Rectangle(tile4V.getIntX() * level.levelSpriteSheetSize, tile4V.getIntY() * level.levelSpriteSheetSize, level.levelSpriteSheetSize, level.levelSpriteSheetSize), mobRectangle)) return true;
				}
			}
			case 2: {
				if (!(tile1.getCollision().equals(Collision.Full)) && tile1.properties.contains(TileTypes.Solid)) {
					if (SubTileCollisionProcessor.processSubTileCollision(tile1.getCollision(), new Rectangle(tile1V.getIntX() * level.levelSpriteSheetSize, tile1V.getIntY() * level.levelSpriteSheetSize, level.levelSpriteSheetSize, level.levelSpriteSheetSize), mobRectangle)) return true;
				}
				if (!(tile3.getCollision().equals(Collision.Full)) && tile3.properties.contains(TileTypes.Solid)) {
					if (SubTileCollisionProcessor.processSubTileCollision(tile3.getCollision(), new Rectangle(tile3V.getIntX() * level.levelSpriteSheetSize, tile3V.getIntY() * level.levelSpriteSheetSize, level.levelSpriteSheetSize, level.levelSpriteSheetSize), mobRectangle)) return true;
				}
			}
			case 3: {
				if (!(tile2.getCollision().equals(Collision.Full)) && tile2.properties.contains(TileTypes.Solid)) {
					if (SubTileCollisionProcessor.processSubTileCollision(tile2.getCollision(), new Rectangle(tile2V.getIntX() * level.levelSpriteSheetSize, tile2V.getIntY() * level.levelSpriteSheetSize, level.levelSpriteSheetSize, level.levelSpriteSheetSize), mobRectangle)) return true;
				}
				if (!(tile4.getCollision().equals(Collision.Full)) && tile4.properties.contains(TileTypes.Solid)) {
					if (SubTileCollisionProcessor.processSubTileCollision(tile4.getCollision(), new Rectangle(tile4V.getIntX() * level.levelSpriteSheetSize, tile4V.getIntY() * level.levelSpriteSheetSize, level.levelSpriteSheetSize, level.levelSpriteSheetSize), mobRectangle)) return true;
				}
			}
		}		
		return false;
	}

	private boolean checkEntityCollision(int xa, int ya, int movingDir) {
		for (Entity entity: Stream.concat(level.getAreas().get(level.getCurrentArea()).getAreaEntities().stream(), level.getLevelEntities().stream()).collect(Collectors.toList())) {
			if (this == entity) continue;
			int r2left = (entity.vector.getIntX() - entity.spriteSize * entity.entityScale / 2) + entity.collisionCoords.get(0).getIntX();
			int r2right = (entity.vector.getIntX() - entity.spriteSize * entity.entityScale / 2) + entity.collisionCoords.get(3).getIntX();
			int r2top = (entity.vector.getIntY() - entity.spriteSize * entity.entityScale / 2) + entity.collisionCoords.get(0).getIntY();
			int r2bottom = (entity.vector.getIntY() - entity.spriteSize * entity.entityScale / 2) + entity.collisionCoords.get(3).getIntY();
			int r1left = (vector.getIntX() - spriteSize * entityScale / 2) + collisionCoords.get(0).getIntX() + xa;
			int r1right = (vector.getIntX() - spriteSize * entityScale / 2) + collisionCoords.get(3).getIntX() + xa;
			int r1top = (vector.getIntY() - spriteSize * entityScale / 2) + collisionCoords.get(0).getIntY() + ya;
			int r1bottom = (vector.getIntY() - spriteSize * entityScale / 2) + collisionCoords.get(3).getIntY() + ya;
			
			if (movingDir == 0) {
				if (r1top <= r2bottom && r1top >= r2top && ((r1left > r2left && r1left < r2right) || (r1right < r2right && r1right > r2left) || (r1left < r2left && r1right > r2right))) return true;
			} else if (movingDir == 1) {
				if (r1bottom >= r2top && r1bottom <= r2bottom && ((r1left > r2left && r1left < r2right) || (r1right > r2left && r1right < r2right) || (r1left < r2left && r1right > r2right))) return true;
			} else if (movingDir == 2) {
				if (r1left <= r2right && r1left >= r2left && ((r1top >= r2top && r1top <= r2bottom) || (r1bottom > r2top && r1bottom < r2bottom) || (r1top < r2top && r1bottom > r2bottom))) return true;
			} else if (movingDir == 3) {
				if (r1right >= r2left && r1right <= r2right && ((r1top >= r2top && r1top <= r2bottom) || (r1bottom > r2top && r1bottom < r2bottom) || (r1top < r2top && r1bottom > r2bottom))) return true;
			}
		}
		return false;
	}
	
	private int setBobAmount(int yOffset) {
		yOffset += level.levelSpriteSheetSize / 2;
		if (previousBobTime == 0) previousBobTime = System.currentTimeMillis();
		long bobDiff = System.currentTimeMillis() - previousBobTime;
		if (bobDiff < currentTile.getTileAnimationDelay()) { 
			yOffset += 1;
		} else if (bobDiff >= currentTile.getTileAnimationDelay() && bobDiff < currentTile.getTileAnimationDelay() * 2) {
			yOffset += 0;
		} else if (bobDiff >= currentTile.getTileAnimationDelay() * 2 && bobDiff < currentTile.getTileAnimationDelay() * 3) {
			yOffset -= 1;
		} else if (bobDiff >= currentTile.getTileAnimationDelay() * 3 && bobDiff < currentTile.getTileAnimationDelay() * 4) {
			yOffset += 0;
		} else {
			previousBobTime = 0;
		}	
		return yOffset;
	}

	private void drawMob(Screen screen, int xOffset, int yOffset) {
		if (swimming) {
			if (movingDir == 0) {
				screen.render(xOffset, yOffset, spriteSet.getUpSprites().get(animationIndex),0 ,0 ,spriteSize, 20, 0x00, entityScale, spriteSize);
			} else if (movingDir == 1) {
				screen.render(xOffset, yOffset, spriteSet.getDownSprites().get(animationIndex),0 ,0 ,spriteSize, 20, 0x00, entityScale, spriteSize);
			} else if (movingDir == 2) {
				screen.render(xOffset, yOffset, spriteSet.getLeftSprites().get(animationIndex),0 ,0 ,spriteSize, 20, 0x00, entityScale, spriteSize);
			} else {
				screen.render(xOffset, yOffset, spriteSet.getRightSprites().get(animationIndex),0 ,0 ,spriteSize, 20, 0x00, entityScale, spriteSize);
			}
		} else{
			if (isMoving) {
				if (movingDir == 0) {
					screen.render(xOffset, yOffset, spriteSet.getUpSprites().get(animationIndex), 0x00, entityScale, spriteSize, spriteSize);
				} else if (movingDir == 1) {
					screen.render(xOffset, yOffset, spriteSet.getDownSprites().get(animationIndex), 0x00, entityScale, spriteSize, spriteSize);
				} else if (movingDir == 2) {
					screen.render(xOffset, yOffset, spriteSet.getLeftSprites().get(animationIndex), 0x00, entityScale, spriteSize, spriteSize);
				} else {
					screen.render(xOffset, yOffset, spriteSet.getRightSprites().get(animationIndex), 0x00, entityScale, spriteSize, spriteSize);
				}
			} else {
				if (movingDir == 0) {
					screen.render(xOffset, yOffset, spriteSet.getPassiveSprites().get(movingDir), 0x00, entityScale, spriteSize, spriteSize);
				} else if (movingDir == 1) {
					screen.render(xOffset, yOffset, spriteSet.getPassiveSprites().get(movingDir), 0x00, entityScale, spriteSize, spriteSize);
				} else if (movingDir == 2) {
					screen.render(xOffset, yOffset, spriteSet.getPassiveSprites().get(movingDir), 0x00, entityScale, spriteSize, spriteSize);
				} else {
					screen.render(xOffset, yOffset, spriteSet.getPassiveSprites().get(movingDir), 0x00, entityScale, spriteSize, spriteSize);
				}
			}
		}
	}

	private void checkTileParticles(Screen screen, int xOffset, int yOffset, int modifier) {
		TileParticles tileParticlesNew = currentTile.getTileParticles();
		TileParticles tileParticlesOld = previousTile.getTileParticles();
		if (tileParticlesNew == null && tileParticlesOld == null) return;

		if (!(tileParticlesNew == null)) {
			if (!(tileParticlesNew.getPassiveImages() == null) && !enterAnimation) {
				if (System.currentTimeMillis() - passiveTime > tileParticlesNew.getPassiveRate()) {
					passiveIndex++;
					passiveTime = System.currentTimeMillis();
				}				
				if (passiveIndex + 1 > tileParticlesNew.getPassiveImages().size()) {
					passiveIndex = 0;
				} 
				int[] image = tileParticlesNew.getPassiveImages().get(passiveIndex);
				if (tileParticlesNew.isPassivePreEntity()) {
					screen.render(xOffset + tileParticlesNew.getPassiveOffset().getIntX(), yOffset + tileParticlesNew.getPassiveOffset().getIntY(), image ,0x00, entityScale, level.levelSpriteSheetSize, level.levelSpriteSheetSize);
					screen.render(xOffset + modifier + tileParticlesNew.getPassiveOffset().getIntX(), yOffset + tileParticlesNew.getPassiveOffset().getIntY(), image ,0x01, entityScale, level.levelSpriteSheetSize, level.levelSpriteSheetSize);
				} else {
					renderList.add(new RenderList(xOffset + tileParticlesNew.getPassiveOffset().getIntX(), yOffset + tileParticlesNew.getPassiveOffset().getIntY(), image, (byte) 0x00, entityScale, level.levelSpriteSheetSize, level.levelSpriteSheetSize));
					renderList.add(new RenderList(xOffset + modifier + tileParticlesNew.getPassiveOffset().getIntX(), yOffset + tileParticlesNew.getPassiveOffset().getIntY(), image, (byte) 0x01, entityScale, level.levelSpriteSheetSize, level.levelSpriteSheetSize));
				}	
			}			
			
			if ((tileSwitch || enterAnimation) && !(tileParticlesNew.getEnterImages() == null)) {
				if (enterAnimation) {
					if (System.currentTimeMillis() - enterTime > tileParticlesNew.getEnterRate()) {
						enterIndex++;
						enterTime = System.currentTimeMillis();
					}
				} else {
					enterAnimation = true;
					enterTime = System.currentTimeMillis();
				}
				if (enterIndex + 1 > tileParticlesNew.getEnterImages().size()) {
					enterAnimation = false;
					enterIndex = 0;
				} else {
					int[] image = tileParticlesNew.getEnterImages().get(enterIndex);
					if (tileParticlesNew.isEnterPreEntity()) {
						screen.render(xOffset + tileParticlesNew.getEnterOffset().getIntX(), yOffset + tileParticlesNew.getEnterOffset().getIntY(), image ,0x00, entityScale, level.levelSpriteSheetSize, level.levelSpriteSheetSize);
						screen.render(xOffset + modifier + tileParticlesNew.getEnterOffset().getIntX(), yOffset + tileParticlesNew.getEnterOffset().getIntY(), image ,0x01, entityScale, level.levelSpriteSheetSize, level.levelSpriteSheetSize);
					} else {
						renderList.add(new RenderList(xOffset + tileParticlesNew.getEnterOffset().getIntX(), yOffset + tileParticlesNew.getEnterOffset().getIntY(), image, (byte) 0x00, entityScale, level.levelSpriteSheetSize, level.levelSpriteSheetSize));
						renderList.add(new RenderList(xOffset + modifier + tileParticlesNew.getEnterOffset().getIntX(), yOffset + tileParticlesNew.getEnterOffset().getIntY(), image, (byte) 0x01, entityScale, level.levelSpriteSheetSize, level.levelSpriteSheetSize));
					}
				}
			}

			if (isMoving && !(tileParticlesNew.getMovingUpImages() == null)) {
				if (movingDir == 0) {
					if (System.currentTimeMillis() - movingTime > tileParticlesNew.getMovingRate()) {
						movingIndex++;
						movingTime = System.currentTimeMillis();
					}					
					if (movingIndex + 1 > tileParticlesNew.getMovingUpImages().size()) {
						movingIndex = 0;
					} 
					int[] image = tileParticlesNew.getMovingUpImages().get(movingIndex);
					if (tileParticlesNew.isMovingPreEntity()) {
						screen.render(xOffset - tileParticlesNew.getMovingUpOffset().getIntX(), yOffset + tileParticlesNew.getMovingUpOffset().getIntY(), image ,0x00, entityScale, level.levelSpriteSheetSize, level.levelSpriteSheetSize);
						screen.render(xOffset + tileParticlesNew.getMovingUpOffset().getIntX() + modifier, yOffset + tileParticlesNew.getMovingUpOffset().getIntY(), image ,0x01, entityScale, level.levelSpriteSheetSize, level.levelSpriteSheetSize);
					} else {
						renderList.add(new RenderList(xOffset - tileParticlesNew.getMovingUpOffset().getIntX() , yOffset + tileParticlesNew.getMovingUpOffset().getIntY(), image, (byte) 0x00, entityScale, level.levelSpriteSheetSize, level.levelSpriteSheetSize));
						renderList.add(new RenderList(xOffset + tileParticlesNew.getMovingUpOffset().getIntX() + modifier, yOffset + tileParticlesNew.getMovingUpOffset().getIntY(), image, (byte) 0x01, entityScale, level.levelSpriteSheetSize, level.levelSpriteSheetSize));
					}
					
				} else if (movingDir == 1) {
					if (System.currentTimeMillis() - movingTime > tileParticlesNew.getMovingRate()) {
						movingIndex++;
						movingTime = System.currentTimeMillis();
					}
					
					if (movingIndex + 1 > tileParticlesNew.getMovingDownImages().size()) {
						movingIndex = 0;
					} 
					int[] image = tileParticlesNew.getMovingDownImages().get(movingIndex);
					if (tileParticlesNew.isMovingPreEntity()) {
						screen.render(xOffset - tileParticlesNew.getMovingDownOffset().getIntX(), yOffset + tileParticlesNew.getMovingDownOffset().getIntY(), image ,0x02, entityScale, level.levelSpriteSheetSize, level.levelSpriteSheetSize);
						screen.render(xOffset + tileParticlesNew.getMovingDownOffset().getIntX() + modifier, yOffset + tileParticlesNew.getMovingDownOffset().getIntY(), image ,0x03, entityScale, level.levelSpriteSheetSize, level.levelSpriteSheetSize);
					} else {
						renderList.add(new RenderList(xOffset - tileParticlesNew.getMovingDownOffset().getIntX(), yOffset + tileParticlesNew.getMovingDownOffset().getIntY(), image, (byte) 0x02, entityScale, level.levelSpriteSheetSize, level.levelSpriteSheetSize));
						renderList.add(new RenderList(xOffset + tileParticlesNew.getMovingDownOffset().getIntX() + modifier, yOffset + tileParticlesNew.getMovingDownOffset().getIntY(), image, (byte) 0x03, entityScale, level.levelSpriteSheetSize, level.levelSpriteSheetSize));
					}
					
				} else if (movingDir == 2) {
					if (System.currentTimeMillis() - movingTime > tileParticlesNew.getMovingRate()) {
						movingIndex++;
						movingTime = System.currentTimeMillis();
					}					
					if (movingIndex + 1 > tileParticlesNew.getMovingLeftImages().size()) {
						movingIndex = 0;
					} 
					int[] image = tileParticlesNew.getMovingLeftImages().get(movingIndex);
					if (tileParticlesNew.isMovingPreEntity()) {
						screen.render(xOffset + tileParticlesNew.getMovingLeftOffset().getIntX(), yOffset + tileParticlesNew.getMovingLeftOffset().getIntY(), image ,0x00, entityScale, level.levelSpriteSheetSize, level.levelSpriteSheetSize);
						screen.render(xOffset + tileParticlesNew.getMovingLeftOffset().getIntX(), yOffset + tileParticlesNew.getMovingLeftOffset().getIntY() - modifier, image ,0x00, entityScale, level.levelSpriteSheetSize, level.levelSpriteSheetSize);
					} else {
						renderList.add(new RenderList(xOffset + tileParticlesNew.getMovingLeftOffset().getIntX(), yOffset + tileParticlesNew.getMovingLeftOffset().getIntY(), image, (byte) 0x00, entityScale, level.levelSpriteSheetSize, level.levelSpriteSheetSize));
						renderList.add(new RenderList(xOffset + tileParticlesNew.getMovingLeftOffset().getIntX(), yOffset + tileParticlesNew.getMovingLeftOffset().getIntY() - modifier, image, (byte) 0x00, entityScale, level.levelSpriteSheetSize, level.levelSpriteSheetSize));
					}
				} else if (movingDir == 3){
					if (System.currentTimeMillis() - movingTime > tileParticlesNew.getMovingRate()) {
						movingIndex++;
						movingTime = System.currentTimeMillis();
					}
					if (movingIndex + 1 > tileParticlesNew.getMovingRightImages().size()) {
						movingIndex = 0;
					} 
					int[] image = tileParticlesNew.getMovingRightImages().get(movingIndex);
					if (tileParticlesNew.isMovingPreEntity()) {
						screen.render(xOffset + tileParticlesNew.getMovingRightOffset().getIntX(), yOffset + tileParticlesNew.getMovingRightOffset().getIntY(), image ,0x01, entityScale, level.levelSpriteSheetSize, level.levelSpriteSheetSize);
						screen.render(xOffset + tileParticlesNew.getMovingRightOffset().getIntX(), yOffset + tileParticlesNew.getMovingRightOffset().getIntY() - modifier, image ,0x01, entityScale, level.levelSpriteSheetSize, level.levelSpriteSheetSize);
					} else {
						renderList.add(new RenderList(xOffset + tileParticlesNew.getMovingRightOffset().getIntX(), yOffset + tileParticlesNew.getMovingRightOffset().getIntY(), image, (byte) 0x01, entityScale, level.levelSpriteSheetSize, level.levelSpriteSheetSize));
						renderList.add(new RenderList(xOffset + tileParticlesNew.getMovingRightOffset().getIntX(), yOffset + tileParticlesNew.getMovingRightOffset().getIntY() - modifier, image, (byte) 0x01, entityScale, level.levelSpriteSheetSize, level.levelSpriteSheetSize));
					}
				}
			}
		}

		if (!(tileParticlesOld == null)) {
			if (!(tileParticlesOld.getExitImages() == null)) {
				if (tileSwitch || exitAnimation) {
					if (exitAnimation) {
						if (System.currentTimeMillis() - exitTime > tileParticlesOld.getExitRate()) {
							exitIndex++;
							exitTime = System.currentTimeMillis();
						}
					} else {
						exitAnimation = true;
						exitTime = System.currentTimeMillis();
					}

					if (exitIndex + 1 > tileParticlesOld.getExitImages().size()) {
						exitAnimation = false;
						exitIndex = 0;
					} else {
						int[] image = tileParticlesOld.getExitImages().get(exitIndex);
						if (tileParticlesOld.isExitPreEntity()) {
							screen.render(xOffset + tileParticlesOld.getExitOffset().getIntX(), yOffset + tileParticlesOld.getExitOffset().getIntY(), image ,0x00, entityScale, level.levelSpriteSheetSize, level.levelSpriteSheetSize);
							screen.render(xOffset + modifier + tileParticlesOld.getExitOffset().getIntX(), yOffset + tileParticlesOld.getExitOffset().getIntY(), image ,0x01, entityScale, level.levelSpriteSheetSize, level.levelSpriteSheetSize);
						} else {
							renderList.add(new RenderList(xOffset + tileParticlesOld.getExitOffset().getIntX(), yOffset + tileParticlesOld.getExitOffset().getIntY(), image, (byte) 0x00, entityScale, level.levelSpriteSheetSize, level.levelSpriteSheetSize));
							renderList.add(new RenderList(xOffset + modifier + tileParticlesOld.getExitOffset().getIntX(), yOffset + tileParticlesOld.getExitOffset().getIntY(), image, (byte) 0x01, entityScale, level.levelSpriteSheetSize, level.levelSpriteSheetSize));
						}	
					}
				}
			}
		}
		tileSwitch = false;
	}

	private void setCurrentTile() {
		Vector2D tile1V = new Vector2D((vector.getIntX() + collisionCoords.get(0).getIntX()) >> level.levelSpriteSheetBinary, (vector.getIntY() + collisionCoords.get(0).getIntY()) >> level.levelSpriteSheetBinary);
		Vector2D tile2V = new Vector2D((vector.getIntX() + collisionCoords.get(1).getIntX()) >> level.levelSpriteSheetBinary, (vector.getIntY() + collisionCoords.get(1).getIntY()) >> level.levelSpriteSheetBinary);
		Vector2D tile3V = new Vector2D((vector.getIntX() + collisionCoords.get(2).getIntX()) >> level.levelSpriteSheetBinary, (vector.getIntY() + collisionCoords.get(2).getIntY()) >> level.levelSpriteSheetBinary);
		Vector2D tile4V = new Vector2D((vector.getIntX() + collisionCoords.get(3).getIntX()) >> level.levelSpriteSheetBinary, (vector.getIntY() + collisionCoords.get(3).getIntY()) >> level.levelSpriteSheetBinary);
		Tile tile1 = level.getTile(tile1V.getIntX(), tile1V.getIntY());
		Tile tile2 = level.getTile(tile2V.getIntX(), tile2V.getIntY());
		Tile tile3 = level.getTile(tile3V.getIntX(), tile3V.getIntY());
		Tile tile4 = level.getTile(tile4V.getIntX(), tile4V.getIntY());	
		
		setCurrentTile(tile1V, tile2V, tile3V, tile4V, tile1, tile2, tile3, tile4);
	}

	private void setCurrentTile(Vector2D tile1V, Vector2D tile2V, Vector2D tile3V, Vector2D tile4V, Tile tile1, Tile tile2, Tile tile3, Tile tile4) {	
		if (previousTile == null && currentTile == null) {
			previousTile = tile1;
			currentTile = tile1;
			currentTile1 = tile1;
			currentTile2 = tile1;
			currentTile3 = tile1;
			currentTile4 = tile1;
			return;
		}		
		
		currentTile1 = tile1;
		currentTile2 = tile2;
		currentTile3 = tile3;
		currentTile4 = tile4;
		
		if (!tile1.getCollision().equals(Collision.Full) && tile1.properties.contains(TileTypes.Liquid)) {
			tile1 = getTileSubTile(tile1, tile1V);
		}
		if (!tile2.getCollision().equals(Collision.Full) && tile1.properties.contains(TileTypes.Liquid)) {
			tile2 = getTileSubTile(tile2, tile2V);
		}
		if (!tile3.getCollision().equals(Collision.Full) && tile1.properties.contains(TileTypes.Liquid)) {
			tile3 = getTileSubTile(tile3, tile3V);
		}
		if (!tile4.getCollision().equals(Collision.Full) && tile1.properties.contains(TileTypes.Liquid)) {
			tile4 = getTileSubTile(tile4, tile4V);
		}
		
		if (tile1.getId() == tile2.getId() && tile2.getId() == tile3.getId() && tile3.getId() == tile4.getId()) {
			if (!(currentTile == tile1)) {
				setPreviousTile(tile1);
				return;
			} else {
                swimming = currentTile.properties.contains(TileTypes.Liquid);
			}
		} 
		
		if (currentTile.properties.contains(TileTypes.Liquid)) {
			if (!(tile1.properties.contains(TileTypes.Liquid))) setPreviousTile(tile1);
			else if (!(tile2.properties.contains(TileTypes.Liquid))) setPreviousTile(tile2);  
			else if (!(tile3.properties.contains(TileTypes.Liquid))) setPreviousTile(tile3); 
			else if (!(tile4.properties.contains(TileTypes.Liquid))) setPreviousTile(tile4);
		}
	}
	
	public void damageEntity(int damage) {
		long tempTime = System.currentTimeMillis();
		if (tempTime - invincibleTimer > invincibleDuration) {
			DamageText newDamageText = new DamageText(damage);
			damageTexts.add(newDamageText);
			currHealth = currHealth - damage;
			invincibleTimer = System.currentTimeMillis();
		}
		if (currHealth <= 0) entityDies();
	}

	private void entityDies() {
		// TODO Auto-generated method stub
		
	}

	private Tile getTileSubTile(Tile tile, Vector2D tileV) {
		boolean collided = SubTileCollisionProcessor.processSubTileCollision(this, tile.getCollision(), 0, 0, movingDir, new Rectangle(tileV.getIntX() * level.levelSpriteSheetSize, tileV.getIntY() * level.levelSpriteSheetSize, level.levelSpriteSheetSize, level.levelSpriteSheetSize));
		if (collided) {
			if (!(currentTile == level.getTile(tile.getSubTiles()[1]))) setPreviousTile(level.getTile(tile.getSubTiles()[1]));
			return level.getTile(tile.getSubTiles()[1]);
		} else { 
			return level.getTile(tile.getSubTiles()[0]);
		}
	}

	private void setPreviousTile(Tile tile) {
		previousTile = currentTile;
		currentTile = tile;	
		enterAnimation = false;
		exitAnimation = false;
		exitIndex = 0;
		enterIndex = 0;
		enterIndex = 0;
		enterTime = 0;
		tileSwitch = true;
        swimming = currentTile.properties.contains(TileTypes.Liquid);
	}
	
/*******************************************GETTERS AND SETTERS***********************************************/
	public int getSpeed() {
		return speed;
	}
	public void setSpeed(int speed) {
		this.speed = speed;
	}
	public boolean isMoving() {
		return isMoving;
	}
	public void setMoving(boolean isMoving) {
		this.isMoving = isMoving;
	}
	public int getMovingDir() {
		return movingDir;
	}
	public void setMovingDir(int movingDir) {
		this.movingDir = movingDir;
	}
	public Tile getPreviousTile() {
		return previousTile;
	}
	public Tile getCurrentTile() {
		return currentTile;
	}
	public void setCurrentTile(Tile currentTile) {
		this.currentTile = currentTile;
	}
	public List<RenderList> getRenderList() {
		return renderList;
	}
	public void setRenderList(List<RenderList> renderList) {
		this.renderList = renderList;
	}
	public long getPreviousBobTime() {
		return previousBobTime;
	}
	public void setPreviousBobTime(long previousBobTime) {
		this.previousBobTime = previousBobTime;
	}
	public boolean isTileSwitch() {
		return tileSwitch;
	}
	public void setTileSwitch(boolean tileSwitch) {
		this.tileSwitch = tileSwitch;
	}
	public int getAnimationIndex() {
		return animationIndex;
	}
	public void setAnimationIndex(int animationIndex) {
		this.animationIndex = animationIndex;
	}
	public int getAnimationSpeed() {
		return animationSpeed;
	}
	public void setAnimationSpeed(int animationSpeed) {
		this.animationSpeed = animationSpeed;
	}
	public List<Vector2D> getCollisionBox() {
		return collisionCoords;
	}
	public void setCollisionBox(List<Vector2D> collisionCoords) {
		this.collisionCoords = collisionCoords;
	}
	public long getAnimationTime() {
		return animationTime;
	}
	public void setAnimationTime(long animationTime) {
		this.animationTime = animationTime;
	}
	public boolean isEnterAnimation() {
		return enterAnimation;
	}
	public void setEnterAnimation(boolean enterAnimation) {
		this.enterAnimation = enterAnimation;
	}
	public boolean isExitAnimation() {
		return exitAnimation;
	}
	public void setExitAnimation(boolean exitAnimation) {
		this.exitAnimation = exitAnimation;
	}
	public int getEnterIndex() {
		return enterIndex;
	}
	public void setEnterIndex(int enterIndex) {
		this.enterIndex = enterIndex;
	}
	public int getMovingIndex() {
		return movingIndex;
	}
	public void setMovingIndex(int movingIndex) {
		this.movingIndex = movingIndex;
	}
	public int getPassiveIndex() {
		return passiveIndex;
	}
	public void setPassiveIndex(int passiveIndex) {
		this.passiveIndex = passiveIndex;
	}
	public int getExitIndex() {
		return exitIndex;
	}
	public void setExitIndex(int exitIndex) {
		this.exitIndex = exitIndex;
	}
	public long getEnterTime() {
		return enterTime;
	}
	public void setEnterTime(long enterTime) {
		this.enterTime = enterTime;
	}
	public long getMovingTime() {
		return movingTime;
	}
	public void setMovingTime(long movingTime) {
		this.movingTime = movingTime;
	}
	public long getPassiveTime() {
		return passiveTime;
	}
	public void setPassiveTime(long passiveTime) {
		this.passiveTime = passiveTime;
	}
	public long getExitTime() {
		return exitTime;
	}
	public void setExitTime(long exitTime) {
		this.exitTime = exitTime;
	}
	public int getXa() {
		return xa;
	}
	public void setXa(int xa) {
		this.xa = xa;
	}
	public int getYa() {
		return ya;
	}
	public void setYa(int ya) {
		this.ya = ya;
	}
	public int getInvincibleDuration() {
		return invincibleDuration;
	}
	public void setInvincibleDuration(int invincibleDuration) {
		this.invincibleDuration = invincibleDuration;
	}
    public SpriteSet getSpriteSet() {
        return spriteSet;
    }
    public void setSpriteSet(SpriteSet spriteSet) {
        this.spriteSet = spriteSet;
    }
    public Equipment getEquipment() {
        return equipment;
    }
    public void setEquipment(Equipment equipment) {
        this.equipment = equipment;
    }
}
