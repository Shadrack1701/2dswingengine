package trx.puzzlerpg.main.entities;

public enum EntityType {
	Player,
	Hostile,
	NPC,
	EntityLootable,
	EntityDestructable;
	
}