package trx.puzzlerpg.main.entities.subtypes;

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

import trx.puzzlerpg.main.PuzzleRPG;
import trx.puzzlerpg.main.entities.Entity;
import trx.puzzlerpg.main.entities.item.Item;
import trx.puzzlerpg.main.gfx.Screen;
import trx.puzzlerpg.main.state.InfoState;

public class EntityLootable extends Entity {

	private List<Item> items = new ArrayList<Item>();
	private int[] unLootedSprite;
	private int[] lootedSprite;
	private boolean looted;
	private int facingDir;
	
	
	@Override
	public void init() {
		// TODO Auto-generated method stub	
	}
	
	@Override
	public void tick() {
		
	}

	@Override
	public void render(Screen screen) {
		int xOffset = vector.getIntX() + spriteSize * entityScale / 2;
		int yOffset = vector.getIntY() + spriteSize * entityScale / 2;
		
		if (looted) {
			screen.render(xOffset, yOffset, lootedSprite, 0x00, entityScale, spriteSize, spriteSize);
		} else {
			screen.render(xOffset, yOffset, unLootedSprite, 0x00, entityScale, spriteSize, spriteSize);
		}
	}
	
	@Override
	public void renderText(Screen screen, Graphics g) {}
	
	@Override
	public List<Item> interact() {
		if (!looted) {
			looted = true;		
			PuzzleRPG.stateManager.render(PuzzleRPG.handler);
			InfoState.setLastEnterInput(System.currentTimeMillis());
			PuzzleRPG.stateManager.states.add(new InfoState(items));
			return items;
		} else return null;
	}

/*******************************************GETTERS AND SETTERS***********************************************/	
	public List<Item> getItems() {
		return items;
	}
	public void setItems(List<Item> items) {
		this.items = items;
	}
	public int[] getUnLootedSprite() {
		return unLootedSprite;
	}
	public void setUnLootedSprite(int[] unLootedSprite) {
		this.unLootedSprite = unLootedSprite;
	}
	public int[] getLootedSprite() {
		return lootedSprite;
	}
	public void setLootedSprite(int[] lootedSprite) {
		this.lootedSprite = lootedSprite;
	}
	public boolean isLooted() {
		return looted;
	}
	public void setLooted(boolean looted) {
		this.looted = looted;
	}
	public int getFacingDir() {
		return facingDir;
	}
	public void setFacingDir(int facingDir) {
		this.facingDir = facingDir;
	}
}
