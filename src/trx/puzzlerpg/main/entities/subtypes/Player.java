package trx.puzzlerpg.main.entities.subtypes;

import java.util.ArrayList;
import java.util.List;
import trx.puzzlerpg.main.InputHandler;
import trx.puzzlerpg.main.entities.Entity;
import trx.puzzlerpg.main.entities.Mob;
import trx.puzzlerpg.main.entities.item.Item;
import trx.puzzlerpg.main.entities.item.ItemProperties;
import trx.puzzlerpg.main.entities.item.subtypes.Weapon;
import trx.puzzlerpg.main.gfx.DamageText;
import trx.puzzlerpg.main.util.Vector2D;

public class Player extends Mob {

	private InputHandler input;
	private List<Item> inventory = new ArrayList<Item>();
	private long lastInteract;
	private long lastAttack = 0;
    private boolean attacking = false;
	private boolean autoEquip = true;

	public Player() {
		super();
	}

	public void tick() {
		int xa = 0;
		int ya = 0;
		if (input.up.isPressed()) ya--;
		if (input.down.isPressed()) ya++;
		if (input.left.isPressed()) xa--;
		if (input.right.isPressed()) xa++;
		if (input.e.isPressed()) {
			long temp = System.currentTimeMillis() - lastInteract;	
			if (lastInteract == 0 || temp > 1000) {
				interact(xa, ya);
				lastInteract = System.currentTimeMillis();
			}
		} else lastInteract = 0;

		if (input.space.isPressed()) {
			long temp = System.currentTimeMillis() - lastAttack;
			if (lastAttack == 0 || temp > attackTime) {
				attack(xa, ya, equipment.getMainHand());
				lastAttack = System.currentTimeMillis();
			}
		}

		isMoving = false;
		if (xa != 0 || ya != 0) {
			move(xa, ya, false);
		}
		if (!isMoving) {
			animationIndex = 0;
			animationTime = 0;
			movingIndex = 0;
			movingTime = 0;
		} else {
			Vector2D newLoc = level.getNewAreaOnLoadTile((vector.getIntX() + spriteSize / 2) >> level.levelSpriteSheetBinary, (vector.getIntY() + spriteSize / 2) >> level.levelSpriteSheetBinary, movingDir);
			if (!(newLoc == null)) {
				vector.x = newLoc.getIntX() * level.levelSpriteSheetSize - level.levelSpriteSheetSize / 2;
				vector.y = newLoc.getIntY() * level.levelSpriteSheetSize;
				level.setWipeScreen(true);
			}
		}
		
		checkEnvironmentDamage();
		
		List<DamageText> removalList = new ArrayList<DamageText>();
		for (DamageText damageText : damageTexts) {
			damageText.tick();
			if (damageText.isStopDisplaying()) removalList.add(damageText);
		}
		
		damageTexts.removeAll(removalList);
		
		tickCount++;
	}

	private void interact(int xa, int ya) {
		for (Entity entity: level.getAllEntities()) {
			if (!entity.isInteractable() || this == entity) continue;
			int r2left = (entity.vector.getIntX() - entity.getSpriteSize() * entity.getEntityScale() / 2) + entity.getCollisionCoords().get(0).getIntX();
			int r2right = (entity.vector.getIntX() - entity.getSpriteSize() * entity.getEntityScale() / 2) + entity.getCollisionCoords().get(3).getIntX();
			int r2top = (entity.vector.getIntY() - entity.getSpriteSize() * entity.getEntityScale() / 2) + entity.getCollisionCoords().get(0).getIntY();
			int r2bottom = (entity.vector.getIntY() - entity.getSpriteSize() * entity.getEntityScale() / 2) + entity.getCollisionCoords().get(3).getIntY();
			int r1left = (vector.getIntX() - spriteSize * entityScale / 2) + collisionCoords.get(0).getIntX() + xa;
			int r1right = (vector.getIntX() - spriteSize * entityScale / 2) + collisionCoords.get(3).getIntX() + xa;
			int r1top = (vector.getIntY() - spriteSize * entityScale / 2) + collisionCoords.get(0).getIntY() + ya;
			int r1bottom = (vector.getIntY() - spriteSize * entityScale / 2) + collisionCoords.get(3).getIntY() + ya;
			
			if (movingDir == 0) {
				r1bottom = r1top;
				r1top = r1top - collision.height;
				if (r1top <= r2bottom && r1top >= r2top && ((r1left > r2left && r1left < r2right) || (r1right < r2right && r1right > r2left) || (r1left < r2left && r1right > r2right)))  {
					List<Item> loot = entity.interact();
					if (!(loot == null)) {
						if (autoEquip) {
							autoEquip(loot);
						} else inventory.addAll(loot);
					}
				}
			} else if (movingDir == 1) {
				r1top = r1bottom;
				r1bottom = r1bottom + collision.height;
				if (r1bottom >= r2top && r1bottom <= r2bottom && ((r1left > r2left && r1left < r2right) || (r1right > r2left && r1right < r2right) || (r1left < r2left && r1right > r2right))) {
					List<Item> loot = entity.interact();
                    if (!(loot == null)) {
                        if (autoEquip) {
                            autoEquip(loot);
                        } else inventory.addAll(loot);
                    }
				}					
			} else if (movingDir == 2) {
				r1right = r1left;
				r1left = r1right - collision.width;
				if (r1left <= r2right && r1left >= r2left && ((r1top >= r2top && r1top <= r2bottom) || (r1bottom > r2top && r1bottom < r2bottom) || (r1top < r2top && r1bottom > r2bottom))) {
					List<Item> loot = entity.interact();
                    if (!(loot == null)) {
                        if (autoEquip) {
                            autoEquip(loot);
                        } else inventory.addAll(loot);
                    }
				}					
			} else if (movingDir == 3) {
				r1left = r1right;
				r1right = r1right + collision.width;		
				if (r1right >= r2left && r1right <= r2right && ((r1top >= r2top && r1top <= r2bottom) || (r1bottom > r2top && r1bottom < r2bottom) || (r1top < r2top && r1bottom > r2bottom))) {
					List<Item> loot = entity.interact();
                    if (!(loot == null)) {
                        if (autoEquip) {
                            autoEquip(loot);
                        } else inventory.addAll(loot);
                    }
				}					
			}
		}	
	}

    public void setInputHandler(InputHandler input) {
		this.input = input;
	}

	@Override
	public List<Item> interact() {return null;}

    private void autoEquip(List<Item> loot) {
        for(Item item : loot) {
            if (item instanceof Weapon) {
                if (equipment.getMainHand() == null) {
                    equipment.setMainHand((Weapon) item);
                    loot.remove(item);
                } else {
                    Weapon weapon = (Weapon) item;
                    Weapon currentWeapon = equipment.getMainHand();
                    if (equipment.getMainHand() == null || equipment.getMainHand().getDamage() > weapon.getDamage()) {
                        equipment.setMainHand(weapon);
                        loot.remove(item);
                    }
                }
            }
        }
        inventory.addAll(loot);
    }

	public long getLastInteract() {
		return lastInteract;
	}

	public void setLastInteract(long lastInteract) {
		this.lastInteract = lastInteract;
	}

}
