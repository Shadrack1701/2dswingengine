package trx.puzzlerpg.main.entities.subtypes;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import trx.puzzlerpg.main.entities.AiProfiles;
import trx.puzzlerpg.main.entities.Mob;
import trx.puzzlerpg.main.entities.item.Item;
import trx.puzzlerpg.main.gfx.DamageText;

public class HostileMob extends Mob {
	private int xa = 0;
	private int ya = 0;
	private EnumSet<AiProfiles> profiles;

	public HostileMob() {
		super();
	}

	public void tick() {
		if (profiles.contains(AiProfiles.Melee)) {
			if (!isMoving && (xa == 0 || xa == 1)) {
				xa = -1;
			} else if (!isMoving && xa == -1) {
				xa = 1;
			}
		} else if (profiles.contains(AiProfiles.Ranged)) {
			if (!isMoving && (ya == 0 || ya == 1)) {
				ya = -1;
			} else if (!isMoving && ya == -1) {
				ya = 1;
			}
		}
			
		isMoving = false;
		if (xa != 0 || ya != 0) {
			move(xa, ya, false);
		}
		if (isMoving) {
			if(level.collideWithLoadTile((vector.getIntX() + level.levelSpriteSheetSize / 2) >> level.levelSpriteSheetBinary, (vector.getIntY() + level.levelSpriteSheetSize / 2) >> level.levelSpriteSheetBinary, movingDir)) {
				isMoving = false;
			}
		}
		
		if (!isMoving) {
			animationIndex = 0;
			animationTime = 0;
			movingIndex = 0;
			movingTime = 0;
		}

		List<DamageText> removalList = new ArrayList<DamageText>();
		for (DamageText damageText : damageTexts) {
			damageText.tick();
			if (damageText.isStopDisplaying()) removalList.add(damageText);
		}

		damageTexts.removeAll(removalList);

		tickCount++;
	}

	
	public EnumSet<AiProfiles> getProfiles() {
		return profiles;
	}
	public void setProfiles(EnumSet<AiProfiles> profiles) {
		this.profiles = profiles;
	}

	@Override
	public List<Item> interact() {return null;}

}
