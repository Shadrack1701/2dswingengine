package trx.puzzlerpg.main.entities.subtypes;

import trx.puzzlerpg.main.entities.Entity;
import trx.puzzlerpg.main.entities.Mob;
import trx.puzzlerpg.main.entities.item.Item;
import trx.puzzlerpg.main.gfx.Screen;
import trx.puzzlerpg.main.util.Vector2D;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Projectile extends Mob {
    private List<Vector2D> projectileCollision = new ArrayList<>();
    private int projectileLife;
    private int projectileSpeed;
    private int projectileAnimationSpeed;
    private long projectileBirth;

    @Override
    public void init() {

    }

    @Override
    public void tick() {
        if (projectileSpeed > 0) {
            if (movingDir == 0) {
                move(0,projectileSpeed * -1, false);
            } else if (movingDir == 1) {
                move(0,projectileSpeed, false);
            } else if (movingDir == 2) {
                move(projectileSpeed * -1, 0, false);
            } else if (movingDir == 3) {
                move(projectileSpeed, 0, false);
            }
        }

        if (System.currentTimeMillis() - projectileBirth > projectileLife) {
            level.getAreas().get(level.getCurrentArea()).getAreaRemoveEntities().add(this);
        }

    }

    @Override
    public void render(Screen screen) {
        if (movingDir == 0) {
            screen.render(vector.getIntX(), vector.getIntY(), spriteSet.getUpSprites().get(animationIndex), 0x00, entityScale, spriteSize, spriteSize);
        } else if (movingDir == 1) {
            screen.render(vector.getIntX(), vector.getIntY(), spriteSet.getDownSprites().get(animationIndex), 0x00, entityScale, spriteSize, spriteSize);
        } else if (movingDir == 2) {
            screen.render(vector.getIntX(), vector.getIntY(), spriteSet.getLeftSprites().get(animationIndex), 0x00, entityScale, spriteSize, spriteSize);
        } else if (movingDir == 3) {
            screen.render(vector.getIntX(), vector.getIntY(), spriteSet.getRightSprites().get(animationIndex), 0x00, entityScale, spriteSize, spriteSize);
        }
    }

    @Override
    public void renderText(Screen screen, Graphics g) {

    }

    @Override
    public List<Item> interact() {
        return null;
    }

    public List<Vector2D> getProjectileCollision() {
        return projectileCollision;
    }
    public void setProjectileCollision(List<Vector2D> projectileCollision) {
        this.projectileCollision = projectileCollision;
    }
    public int getProjectileLife() {
        return projectileLife;
    }
    public void setProjectileLife(int projectileLife) {
        this.projectileLife = projectileLife;
    }
    public int getProjectileSpeed() {
        return projectileSpeed;
    }
    public void setProjectileSpeed(int projectileSpeed) {
        this.projectileSpeed = projectileSpeed;
    }
    public int getProjectileAnimationSpeed() {
        return projectileAnimationSpeed;
    }
    public void setProjectileAnimationSpeed(int projectileAnimationSpeed) {
        this.projectileAnimationSpeed = projectileAnimationSpeed;
    }
    public long getProjectileBirth() {
        return projectileBirth;
    }
    public void setProjectileBirth(long projectileBirth) {
        this.projectileBirth = projectileBirth;
    }
}
