package trx.puzzlerpg.main.entities.subtypes;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import trx.puzzlerpg.main.PuzzleRPG;
import trx.puzzlerpg.main.entities.AiProfiles;
import trx.puzzlerpg.main.entities.Mob;
import trx.puzzlerpg.main.entities.item.Item;
import trx.puzzlerpg.main.gfx.gui.Conversation;
import trx.puzzlerpg.main.state.DialogueState;

public class NPC extends Mob {
	private int xa = 0;
	private int ya = 0;
	private EnumSet<AiProfiles> profiles;
	private List<Conversation> conversations = new ArrayList<Conversation>();
	private int currentConversation;
	private boolean allRead;

	public NPC() {
		super();
	}

	public void tick() {
		if (profiles.contains(AiProfiles.Stationary)) return;	
		
		isMoving = false;
		if (xa != 0 || ya != 0) {
			move(xa, ya, false);
		}
		if (isMoving) {
			if(level.collideWithLoadTile((vector.getIntX() + level.levelSpriteSheetSize / 2) >> level.levelSpriteSheetBinary, (vector.getIntY() + level.levelSpriteSheetSize / 2) >> level.levelSpriteSheetBinary, movingDir)) {
				isMoving = false;
			}
		}
		
		if (!isMoving) {
			animationIndex = 0;
			animationTime = 0;
			movingIndex = 0;
			movingTime = 0;
		}
		
		tickCount++;
	}

	@Override
	public List<Item> interact() {
		if (currentConversation < conversations.size()) {
			if (allRead) {
				return perpetualConverse();
			}
			DialogueState.setLastEnterInput(System.currentTimeMillis());
			PuzzleRPG.stateManager.states.add(new DialogueState(conversations.get(currentConversation)));
			conversations.get(currentConversation).setRead(true);
			currentConversation++;
		} else {
			allRead = true;
			currentConversation = 0;
			repeatOnlyConversation();
			return perpetualConverse();
		}
		return null;
	}
	
	private void repeatOnlyConversation() {
		for (int x = 0; x < conversations.size(); x++) {
			if(!conversations.get(x).isRepeatable()) conversations.remove(x);
		}
		
	}

	public List<Item> perpetualConverse() {
		if (currentConversation >= conversations.size()) {
			currentConversation = 0;
		} else {
			DialogueState.setLastEnterInput(System.currentTimeMillis());
			PuzzleRPG.stateManager.states.add(new DialogueState(conversations.get(currentConversation)));
			currentConversation++;
		}
		return null;
	}
	
	/*******************************************GETTERS AND SETTERS***********************************************/		
	public EnumSet<AiProfiles> getProfiles() {
		return profiles;
	}
	public void setProfiles(EnumSet<AiProfiles> profiles) {
		this.profiles = profiles;
	}
	public List<Conversation> getConversations() {
		return conversations;
	}
	public void setConversations(List<Conversation> conversations) {
		this.conversations = conversations;
	}

	public int getCurrentConversation() {
		return currentConversation;
	}

	public void setCurrentConversation(int currentConversation) {
		this.currentConversation = currentConversation;
	}

}
