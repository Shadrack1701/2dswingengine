package trx.puzzlerpg.main.entities.item;

import trx.puzzlerpg.main.entities.item.subtypes.Weapon;

public class Equipment {

    private Item head;
    private Item chest;
    private Item legs;
    private Item feet;
    private Item hands;
    private Weapon mainHand;
    private Item offHand;
    private Item rings[] = new Item[4];
    private Item necklace;

/*******************************************GETTERS AND SETTERS***********************************************/
    public Item getHead() {
        return head;
    }
    public void setHead(Item head) {
        this.head = head;
    }
    public Item getChest() {
        return chest;
    }
    public void setChest(Item chest) {
        this.chest = chest;
    }
    public Item getLegs() {
        return legs;
    }
    public void setLegs(Item legs) {
        this.legs = legs;
    }
    public Item getFeet() {
        return feet;
    }
    public void setFeet(Item feet) {
        this.feet = feet;
    }
    public Item getHands() {
        return hands;
    }
    public void setHands(Item hands) {
        this.hands = hands;
    }
    public Weapon getMainHand() {
        return mainHand;
    }
    public void setMainHand(Weapon mainHand) {
        this.mainHand = mainHand;
    }
    public Item getOffHand() {
        return offHand;
    }
    public void setOffHand(Item offHand) {
        this.offHand = offHand;
    }
    public Item[] getRings() {
        return rings;
    }
    public void setRings(Item[] rings) {
        this.rings = rings;
    }
    public Item getNecklace() {
        return necklace;
    }
    public void setNecklace(Item necklace) {
        this.necklace = necklace;
    }
}
