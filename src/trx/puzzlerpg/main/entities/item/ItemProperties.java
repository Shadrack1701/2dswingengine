package trx.puzzlerpg.main.entities.item;

public enum ItemProperties {
	Sellable,
	Quest,
	Usable,
	Currency;
}
