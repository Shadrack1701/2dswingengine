package trx.puzzlerpg.main.entities.item;

public enum ItemType {
    Weapon,
    Armor,
    Accessory;
}
