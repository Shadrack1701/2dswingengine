package trx.puzzlerpg.main.entities.item.subtypes;

import trx.puzzlerpg.main.entities.item.Item;
import trx.puzzlerpg.main.gfx.Screen;
import trx.puzzlerpg.main.gfx.gui.SpriteSet;
import trx.puzzlerpg.main.util.Vector2D;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Weapon extends Item {

    private int projectileSetId;
    private int damage;
    private int attackSpeed;
    private boolean melee;
    private SpriteSet weaponSprite;
    private List<Vector2D> projectileCollision = new ArrayList<>();
    private int projectileLife;
    private int projectileSpeed;
    private int projectileAnimationSpeed;

    @Override
    public void init() {
        super.init();
    }

    @Override
    public void tick() {
        super.tick();
    }

    @Override
    public void render(Screen screen) {
        super.render(screen);
    }

    @Override
    public void renderText(Screen screen, Graphics g) {
        super.renderText(screen, g);
    }

    @Override
    public List<Item> interact() {
        return super.interact();
    }

    public int getProjectileSetId() {
        return projectileSetId;
    }
    public void setProjectileSetId(int projectileSetId) {
        this.projectileSetId = projectileSetId;
    }
    public int getDamage() {
        return damage;
    }
    public void setDamage(int damage) {
        this.damage = damage;
    }
    public int getAttackSpeed() {
        return attackSpeed;
    }
    public void setAttackSpeed(int attackSpeed) {
        this.attackSpeed = attackSpeed;
    }
    public boolean isMelee() {
        return melee;
    }
    public void setMelee(boolean melee) {
        this.melee = melee;
    }
    public SpriteSet getWeaponSprite() {
        return weaponSprite;
    }
    public void setWeaponSprite(SpriteSet weaponSprite) {
        this.weaponSprite = weaponSprite;
    }
    public List<Vector2D> getProjectileCollision() {
        return projectileCollision;
    }
    public void setProjectileCollision(List<Vector2D> projectileCollision) {
        this.projectileCollision = projectileCollision;
    }
    public int getProjectileLife() {
        return projectileLife;
    }
    public void setProjectileLife(int projectileLife) {
        this.projectileLife = projectileLife;
    }
    public int getProjectileSpeed() {
        return projectileSpeed;
    }
    public void setProjectileSpeed(int projectileSpeed) {
        this.projectileSpeed = projectileSpeed;
    }
    public int getProjectileAnimationSpeed() {
        return projectileAnimationSpeed;
    }
    public void setProjectileAnimationSpeed(int projectileAnimationSpeed) {
        this.projectileAnimationSpeed = projectileAnimationSpeed;
    }
}
