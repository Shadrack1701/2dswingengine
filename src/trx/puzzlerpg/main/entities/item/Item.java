package trx.puzzlerpg.main.entities.item;

import java.awt.Graphics;
import java.util.EnumSet;
import java.util.List;

import trx.puzzlerpg.main.entities.Entity;
import trx.puzzlerpg.main.gfx.Screen;

public class Item extends Entity {

	private boolean pluralName;
	private int[] icon;
	private int iconWidth, iconHeight;
	private EnumSet<ItemProperties> itemProperties;
	private int value;
	private String infoText;
	
	@Override
	public void init() {}
	
	@Override
	public void tick() {}

	@Override
	public void render(Screen screen) {}
	
	@Override
	public void renderText(Screen screen, Graphics g) {}

	@Override
	public List<Item> interact() {return null;}


	public boolean isPluralName() {
		return pluralName;
	}

	public void setPluralName(boolean pluralName) {
		this.pluralName = pluralName;
	}

	public int[] getIcon() {
		return icon;
	}

	public void setIcon(int[] icon) {
		this.icon = icon;
	}

	public EnumSet<ItemProperties> getItemProperties() {
		return itemProperties;
	}

	public void setItemProperties(EnumSet<ItemProperties> itemProperties) {
		this.itemProperties = itemProperties;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public String getInfoText() {
		return infoText;
	}

	public void setInfoText(String infoText) {
		this.infoText = infoText;
	}

	public int getIconWidth() {
		return iconWidth;
	}

	public void setIconWidth(int iconWidth) {
		this.iconWidth = iconWidth;
	}

	public int getIconHeight() {
		return iconHeight;
	}

	public void setIconHeight(int iconHeight) {
		this.iconHeight = iconHeight;
	}

}
