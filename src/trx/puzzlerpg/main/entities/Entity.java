package trx.puzzlerpg.main.entities;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

import trx.puzzlerpg.main.entities.item.Item;
import trx.puzzlerpg.main.gfx.Screen;
import trx.puzzlerpg.main.level.Level;
import trx.puzzlerpg.main.util.Vector2D;

public abstract class Entity{

	public Vector2D vector;
	protected Level level;
	protected int entityScale;
	protected int tickCount;
	protected String name;
	protected int spriteSize;
	protected List<Vector2D> collisionCoords = new ArrayList<>();
	protected Rectangle collision;
	protected boolean collides;
	protected boolean interactable;
	protected int movingDir = 1;
	protected int maxHealth, currHealth;
	
	public Entity(Level level, int entityScale, String name) {
		this.entityScale = entityScale;
		this.name = name;
		this.level = level;
	}
	
	public Entity() {
	}
	
	public abstract List<Item> interact();
	
	public abstract void init();

	public abstract void tick();
	
	public abstract void render(Screen screen);
	
	public abstract void renderText(Screen screen, Graphics g);

	public Vector2D getVector() {
		return vector;
	}
	public void setVector(Vector2D vector) {
		this.vector = vector;
	}
	public Level getLevel() {
		return level;
	}
	public void setLevel(Level level) {
		this.level = level;
	}
	public int getEntityScale() {
		return entityScale;
	}
	public void setEntityScale(int entityScale) {
		this.entityScale = entityScale;
	}
	public int getTickCount() {
		return tickCount;
	}
	public void setTickCount(int tickCount) {
		this.tickCount = tickCount;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getSpriteSize() {
		return spriteSize;
	}
	public void setSpriteSize(int spriteSize) {
		this.spriteSize = spriteSize;
	}
	public List<Vector2D> getCollisionCoords() {
		return collisionCoords;
	}
	public void setCollisionCoords(List<Vector2D> collisionCoords) {
		this.collisionCoords = collisionCoords;
	}
	public Rectangle getCollision() {
		return collision;
	}
	public void setCollision(Rectangle collision) {
		this.collision = collision;
	}
	public boolean isCollides() {
		return collides;
	}
	public void setCollides(boolean collides) {
		this.collides = collides;
	}
	public boolean isInteractable() {
		return interactable;
	}
	public void setInteractable(boolean interactable) {
		this.interactable = interactable;
	}
	public int getMovingDir() {
		return movingDir;
	}
	public void setMovingDir(int movingDir) {
		this.movingDir = movingDir;
	}
	public int getMaxHealth() {
		return maxHealth;
	}
	public void setMaxHealth(int maxHealth) {
		this.maxHealth = maxHealth;
	}
	public int getCurrHealth() {
		return currHealth;
	}
	public void setCurrHealth(int currHealth) {
		this.currHealth = currHealth;
	}
}
