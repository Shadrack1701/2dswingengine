package trx.puzzlerpg.main.entities;

public enum AiProfiles {
	Melee,
	Ranged,
	Aggressive,
	Retreating,
	Melee50Ranged,
	Ranged50Melee,
	Shield, 
	Stationary;
}
